@extends('public.student.template')

@section('head')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/ol.css') }}">
  <style type="text/css">
    .map, #map {
      height: 200px;
      width: 100%;
    }
    #map_marker
    {
      background-color: rgba(230, 10, 10, 0.5);
      color: black;
      font-weight: bold;
      text-align: center;
      width: 20px;
      height: 20px;
      border-radius: 15px;
    }
    img.olTileImage {
      max-width: none;
    }
  </style>
@endsection

@section('content')
  <div class="content">
    <div class="container-fluid">
      <!-- div class="row">
        <div class="col-md-12">
          <div id="map_marker"></div>
          <div id="map" class="map"></div>
        </div>
      </div -->

      <div class="row">
        <div class="col-md-8">

          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">{{ $student->nama }}</h4>
              <p class="card-category">Student of {{ $student->school->nama }}, Grade {{ $student->kelas }}</p>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card card-chart">
                <div class="card-header card-header-success">
                  <!-- div class="ct-chart" id="websiteViewsChart"></div -->
                  <canvas id="chart_line_gloryhistory"></canvas>
                </div>
                <div class="card-body">
                  <h4 class="card-title">Glory History</h4>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> last log at
                    {{ App\StudentHistoryGlory::where('student_id', $student->id)->max('tanggal') }}
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="card card-chart">
                <div class="card-header card-header-danger">
                  <!-- div class="ct-chart" id="websiteViewsChart"></div -->
                  <canvas id="chart_radar_stats"></canvas>
                </div>
                <div class="card-body">
                  <h4 class="card-title">Student Status</h4>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">games</i> stats determined by test scores
                  </div>
                </div>
              </div>
            </div>
          </div>



          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">Detail</h4>
              <p class="card-category">{{ $student->nama }} of {{ $student->school->nama }}</p>
            </div>
            <div class="card-body">

              <div class="alert alert-info">
                <span>Glory: {{ round($student->glory) }}</span>
              </div>
              <div class="alert alert-info">
                <span>Apprentices: {{ count($student->apprentices) }} people</span>
              </div>
              <div class="alert alert-success">
                <span>Apprentice Skill-Up: {{ $student->apprentice_skill_up }}x</span>
              </div>
              <!--div class="alert alert-danger">
                <span>Battles: STUBBED!! win, STUBBED!! lose (STUBBED!!%)</span>
              </div-->

              <!-- div>
                <h6>Location</h6>
                <div id="map_marker"></div>
                <div id="map" class="map"></div>
              </div -->

            </div>
          </div>

          @if( count($student->apprentices) > 0 )
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">Apprentices</h4>
                <p class="card-category">{{ $student->nama }} is mentoring:</p>
              </div>
                <div class="card-body table-responsive">
                  <table class="table table-hover">
                    <thead class="text-warning">
                      <th>Global Rank</th>
                      <th>Name</th>
                      <th>Glory</th>
                      <th>Apprentice's School</th>
                      <th>When</th>
                      <th>Mentoring In</th>
                      <th>Action</th>
                    </thead>
                    <tbody>
                      @foreach($student->apprentices as $row)
                        <tr>
                          <td>{{ $row->apprentice->getRank() }}</td>
                          <td>{{ $row->apprentice->nama }}</td>
                          <td>{{ round($row->apprentice->glory) }}</td>
                          <td>{{ $row->apprentice->school->nama }}</td>
                          @if( $row->isActive() )
                            <td>Now</td>
                          @else
                            <td>Past</td>
                          @endif
                          <td>{{ $row->subject->nama }}</td>
                          <td><a href="{{ route('publicStudentProfile', ['id' => $row->apprentice->id]) }}"><button class="btn-primary btn-small">Profile</button></a></td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
          @endif

          @if( count($student->masters) > 0 )
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">Masters</h4>
                <p class="card-category">{{ $student->nama }} is being mentored by:</p>
              </div>
                <div class="card-body table-responsive">
                  <table class="table table-hover">
                    <thead class="text-warning">
                      <th>Global Rank</th>
                      <th>Name</th>
                      <th>Glory</th>
                      <th>Master's School</th>
                      <th>When</th>
                      <th>Mentored In</th>
                      <th>Action</th>
                    </thead>
                    <tbody>
                      @foreach($student->masters as $row)
                        <tr>
                          <td>{{ $row->master->getRank() }}</td>
                          <td>{{ $row->master->nama }}</td>
                          <td>{{ round($row->master->glory) }}</td>
                          <td>{{ $row->master->school->nama }}</td>
                          @if( $row->isActive() )
                            <td>Now</td>
                          @else
                            <td>Past</td>
                          @endif
                          <td>{{ $row->subject->nama }}</td>
                          <td><a href="{{ route('publicStudentProfile', ['id' => $row->master->id]) }}"><button class="btn-primary btn-small">Profile</button></a></td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
          @endif



        </div>
        <div class="col-md-4">
          <div class="card card-profile">
            <div class="card-avatar">
              <a href="{{ $student->getImageAsset() }}">
                <img class="img" src="{{ $student->getImageAsset() }}" />
              </a>
            </div>
            <div class="card-body">
              <h6 class="card-category">Glory: {{ round($student->glory) }}, {{ $student->specialization }} </h6>
              <h4 class="card-title">{{ $student->nama }}</h4>
              <h6 class="card-category">Specialization: {{ $student->recountSpecialization() }}</h6>
              <p class="card-description">{{ $student->getGloryDescription() }}</p>
              <h4 class="card-title"><img style="height: 20px;width: auto;" src="{{ $student->school->getImageAsset() }}"> {{ $student->school->nama }}</h4>
              <a href="{{ route('publicSchoolProfile', ['id' => $student->school->id]) }}" class="btn btn-primary btn-round">View School</a>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">Location</h4>
            <p class="card-category">Lat: {{ $student->y }}, Lon: {{ $student->x }}</p>
          </div>
          <div class="card-body">
            <div class="col-md-12">
              <div id="map_marker"></div>
              <div id="map" class="map"></div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
@endsection

@section('scripts')
  <!-- script src="{{ asset('js/plugins/chartist.min.js') }}"></script -->
  <script src="{{ asset('js/plugins/chartist.min.js') }}"></script>
  <script src="{{ asset('js/plugins/Chart.js') }}"></script>
  <script type="text/javascript">
    Chart.defaults.global.defaultFontColor = "#000000";
  </script>
  <script type="text/javascript">
    var ctx = document.getElementById('chart_radar_stats').getContext('2d');

    var radarChart = new Chart(ctx, {
      type: 'radar',
      data: @json( $student->getChartRadarData() ),
      options: {
        scale: {
          display: true,
          ticks: {
            //min: 0,
            max: 100
          },
          responsive:true,
          maintainAspectRatio: false
        }
      }
    });
  </script>
  <script type="text/javascript">
    var ctx1 = document.getElementById('chart_line_gloryhistory').getContext('2d');

    var radarChart1 = new Chart(ctx1, {
      type: 'line',
      data: @json( $student->getChartLineGloryHistoryData() ),
      options: {
        scales: {
          yAxes: [{
            ticks: {
              //beginAtZero: true
            }
          }]
        },
        bezierCurve : false
      }
    });
  </script>

  <script type="text/javascript" src="{{ asset('js/plugins/ol.js') }}"></script>
  <script type="text/javascript">
    var lat = {{ $student->y }};
    var lng = {{ $student->x }};
    var center = ol.proj.transform([lng, lat], 'EPSG:4326', 'EPSG:3857'); //set coordinate

    //set overlay (map marker)
    var overlay = new ol.Overlay({
      position: center,
      element: document.getElementById('map_marker'),
      positioning: 'center-center',
    });


    //set map
    var map = new ol.Map({
      target: 'map',
      layers: [new ol.layer.Tile({
        source: new ol.source.OSM()
      })],
      overlays: [overlay],
      view: new ol.View({
        center: center,
        zoom: 16 // 9 kelihatan pulau jawa, 16 kelihatan perkampungan sekitar, 20 kelihatan rumah2 tetangga
      })
    });
  </script>
@endsection