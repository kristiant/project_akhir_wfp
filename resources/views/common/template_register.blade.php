@extends('common.template_app')
@section('content')
  <div class="content">
    <div class="container-fluid d-flex justify-content-center align-items-center">

      <div class="card card-nav-tabs col-lg-6 col-md-12">
        <h4 class="card-header card-header-info">@yield('title')</h4>
        <div class="card-body">
          <p class="card-text">@yield('text')</p>
          <form @yield('formActionMethod')>
            @csrf
            @foreach($fields as $key => $value)
              <div class="form-group{{ $errors->has($value['name']) ? ' has-error' : '' }}">
                <label for="form_register_{{ $value['name'] }}" class="bmd-label-floating">{{ $value['readable'] }}</label>
                <input type="{{ $value['type'] }}" class="form-control" id="form_register_{{ $value['name'] }}" name="{{ $value['name'] }}" value="{{ old($value['name']) }}">
                @if(isset($value['hint']) && !$errors->has($value['name']) )
                  <span class="bmd-help">{{ $value['hint'] }}</span>
                @elseif ($errors->has($value['name']))
                  <span class="label-error">{{ $errors->first($value['name']) }}</span>
                @endif
              </div>
            @endforeach

            <div class="form-group">
              <button class="btn btn-primary">@yield('buttonText')</button>
              @yield('loginLink')
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>


@endsection