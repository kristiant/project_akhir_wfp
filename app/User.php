<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Exception;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'nama', 'is_admin', 'is_student'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table = 'users';
    public $timestamps = true;
    protected $primaryKey = 'id';
    protected $keyType = 'string';

    // relationships
    public function student()
    {
        if($this->is_student == true)
        {
            return $this->belongsTo('App\Student', 'id');
        }
        else
        {
            return null;
        }
    }
}
