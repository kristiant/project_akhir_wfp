@extends('admin.template')

@section('searchBarAction')
  {{ route('adminSubjectSearchRedirect') }}
@endsection

@section('searchBarVisibility')
  inline-block
@endsection

@section('scriptNav')
  <script type="text/javascript">
    $("#nav_item_subjects").addClass("active");
  </script>
@endsection