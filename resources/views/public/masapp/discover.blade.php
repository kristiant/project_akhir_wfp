@extends('public.masapp.template')

@section('head')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/ol.css') }}">
  <style type="text/css">
    .map_masapp
    {
      height: 400px;
      width: 100%;
    }
    img.olTileImage {
      max-width: none;
    }
    .map_masapp_marker
    {
      background-color: rgba(230, 10, 10, 0.5);
      width: 20px;
      height: 20px;
      border-radius: 15px;
      /* overflow: hidden; */
      font-size: x-small;
      text-align: center;
    }
    .map_masapp_label
    {
      background-color: rgba(10, 230, 10, 0.5);
      color: black;
      font-weight: bold;
    }
  </style>
@endsection

@section('content')
  <div class="content">
    <div class="container-fluid">

      <form action="{{ route('memberMasappDiscoverSearchRedirect') }}" method="POST">
      @csrf
        <div class="row">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">Search {{ $masterOrApprentice }}s on subject {{ $subject->nama }}</h4>
              <p class="card-category">Search on another subject</p>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Subject</label>
                    <select name="subject_id" class="form-control">
                      @foreach($subjects as $item)
                        <option value="{{ $item->id }}"
                          @if($item->id == $subject->id)
                            selected
                          @endif
                        >
                          {{ $item->nama }}
                        </option>
                      @endforeach
                    </select>
                    <input type="hidden" name="master_or_apprentice" value="{{ $masterOrApprentice }}">
                  </div>
                </div>
                <div class="col-md-6">
                  <input type="submit" value="Search" class="btn btn-primary">
                </div>
              </div>

            </div>

          </div>
        </div>
      </form>

      <div class="row">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">Recommended {{ $masterOrApprentice }}s Map</h4>
            <p class="card-category">See potential {{ $masterOrApprentice }}s' location on map</p>
          </div>
          <div class="card-body">
            <div class="col-md-12">

              <div id="map_masapp_marker_ME" class="map_masapp_marker"></div>
              <div id="map_masapp_label_ME" class="map_masapp_label">ME</div>

              @foreach($recommendations as $item)
                <div id="map_masapp_marker_{{ $item->id }}" class="map_masapp_marker"></div>
                <div id="map_masapp_label_{{ $item->id }}" class="map_masapp_label">{{ $item->nama }}</div>
              @endforeach

              <div id="map_masapp" class="map_masapp">
              </div>
            </div>
          </div>
        </div>
      </div>


      <div class="row">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">Recommended {{ $masterOrApprentice }}s</h4>
            <p class="card-category">List potential {{ $masterOrApprentice }}s on subject {{ $subject->nama }}</p>
          </div>
            <div class="card-body table-responsive">
              <table class="table table-hover">
                <thead class="text-warning">
                  <th>Global #</th>
                  <th>Name</th>
                  <th>Glory</th>
                  <th>Specialization</th>
                  <th>Action</th>
                </thead>
                <tbody>
                  @foreach($recommendations as $row)
                    <tr>
                      <td>{{ $row->getRank() }}</td>
                      <td>{{ $row->nama }}</td>
                      <td>{{ round($row->glory) }}</td>
                      <td>{{ $row->specialization }}</td>
                      <td>
                        <a href="{{ route('publicStudentProfile', ['id' => $row->id]) }}"><button class="btn-secondary btn-small">Profile</button></a>
                        <a href="{{ route('memberMasappApplyForm',
                        [
                          'arg_master_or_apprentice' => $masterOrApprentice,
                          'target_student_id' => $row->id,
                          'subject_id' => $subject->id
                        ]) }}"><button class="btn-primary btn-small">Apply</button></a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
        </div>
      </div>


    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript" src="{{ asset('js/plugins/ol.js') }}"></script>
  <script type="text/javascript">
    var lat = {{ $student->y }};
    var lng = {{ $student->x }};
    var center = ol.proj.transform([lng, lat], 'EPSG:4326', 'EPSG:3857'); //set coordinate

    //set overlay (marker)
    var overlayMe = new ol.Overlay({
      position: center,
      element: document.getElementById('map_masapp_marker_ME'),
      positioning: 'center-center',
    });

    var overlayArr = [overlayMe];

    var overlayMeLabel = new ol.Overlay({
      position: center,
      element: document.getElementById('map_masapp_label_ME'),
      positioning: 'bottom-left',
    });
    overlayArr.push(overlayMeLabel);

    var positionBaru = null;
    var overlayBaru = null;
    @foreach($recommendations as $item)
      // circle marker
      positionBaru = ol.proj.transform([{{ $item->x }}, {{ $item->y }}], 'EPSG:4326', 'EPSG:3857');
      overlayBaru = new ol.Overlay({
        position: positionBaru,
        element: document.getElementById('map_masapp_marker_{{ $item->id }}'),
        positioning: 'center-center',
      });
      overlayArr.push(overlayBaru);

      // name label
      positionBaru = ol.proj.transform([{{ $item->x }}, {{ $item->y }}], 'EPSG:4326', 'EPSG:3857');
      overlayBaru = new ol.Overlay({
        position: positionBaru,
        element: document.getElementById('map_masapp_label_{{ $item->id }}'),
        positioning: 'bottom-left',
      });
      overlayArr.push(overlayBaru);
    @endforeach



    //set map_masapp
    var map_masapp = new ol.Map({
      target: 'map_masapp',
      layers: [new ol.layer.Tile({
        source: new ol.source.OSM()
      })],
      overlays: overlayArr,
      view: new ol.View({
        center: center,
        zoom: 14 // 9 kelihatan pulau jawa, 16 kelihatan perkampungan sekitar, 20 kelihatan rumah2 tetangga
      })
    });
  </script>
@endsection