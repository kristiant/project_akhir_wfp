@extends('parent_template')
@section('content')
  <div class="content">
    <div class="container-fluid d-flex justify-content-center align-items-center">

      <div class="card card-nav-tabs col-lg-6 col-md-12">
        <h4 class="card-header card-header-info">Login</h4>
        <div class="card-body">
          <!-- h4 class="card-title">Login to Hall of Academic Fame</h4>
          <p class="card-text">Login to Hall of Academic Fame</p -->
          <!-- a href="#" class="btn btn-primary">Go somewhere</a -->

          <p class="card-text">Login to Hall of Academic Fame</p>
          <form action="/student/1">
            <div class="form-group">
             <label for="form_login_username" class="bmd-label-floating">Username</label>
             <input type="text" class="form-control" id="form_login_username">
             <span class="bmd-help">Enter your KTP NIK or your registered username</span>
            </div>
            <div>
              <input type="password" class="form-control" id="form_login_password" placeholder="Password">
            </div>
            <div class="form-group">
              <button class="btn btn-primary">Login</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>


@endsection