<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\School;
use Config;

class StudentRankPagesController extends Controller
{
    public function rankGlobalIndex()
    {
        return redirect(route('publicStudentIndexPaginated', ['page_number' => 1]));
    }

    public function rankGlobal($page_number)
    {
        $query = $query = Student::orderBy('glory', 'desc')->take(Config::get('kristian.max_rank_displayed'))->get();
        return $this->displayRank($query, null, $page_number);
    }

    /* public function rankLocal($page_number) { // return $this->displayRank($query); } */

    public function rankLevel($level_jenjang, $page_number)
    {
        // return $this->displayRank($query);
    }

    public function search($keyword, $page_number)
    {
        // return $this->displayRank($query);
        $query = Student::where('nama', 'like', '%' . $keyword . '%')
            ->orWhere('id', 'like', $keyword . '%')
            ->get();
        return $this->displayRank($query, $keyword, $page_number);
    }

    private function displayRank($query, $keyword, $page_number)
    {
        $students = $query->forPage($page_number, Config::get('kristian.pagination_max_item'));
        $viewArgs["students"] = $students;

        $viewArgs["page"]["current"] = $page_number;
        $viewArgs["page"]["max"] = ceil($query->count() / Config::get('kristian.pagination_max_item'));
        //dd($viewArgs); exit;
        if($viewArgs["page"]["max"] == 0) $viewArgs["page"]["max"] = 1;

        if($keyword != null) $viewArgs["keyword"] = $keyword;
        
        return view('public.student.list', $viewArgs);
    }

    public function profile($id)
    {
        $student = Student::find($id);
        
        $viewArgs["student"] = $student;
        
        $viewArgs["pageTitle"] = $student->nama . " - Hall of Academic Fame";

        return view('public.student.profile', $viewArgs);

    }
}
