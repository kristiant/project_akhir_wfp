@extends('common.template_app')
@section('content')
  <div class="content">
    <div class="container-fluid d-flex justify-content-center align-items-center">

      <div class="card card-nav-tabs col-lg-6 col-md-12">
        <h4 class="card-header card-header-info">@yield('title')</h4>
        <div class="card-body">
          <p class="card-text">@yield('text')</p>
          <form @yield('formActionMethod')>
            @csrf
            <div class="form-group{{ ( $errors->has('username') || $errors->has('auth.failed') ) ? ' has-error' : '' }}">
              <label for="form_login_username" class="bmd-label-floating">Username</label>
              <input type="text" class="form-control" id="form_login_username" name="username" value="{{ old('username') }}">
              <span class="bmd-help">@yield('usernameHint')</span>
              @if ($errors->has('username'))
                <span class="help-block">
                  <strong>{{ $errors->first('username') }}</strong>
                </span>
              @elseif ($errors->has('auth.failed'))
                <span class="help-block">
                  <strong>{{ $errors->first('auth.failed') }}</strong>
                </span>
              @endif
            </div>
            <div class="form-group{{ ( $errors->has('password') || $errors->has('auth.failed') ) ? ' has-error' : '' }}">
              <input type="password" class="form-control" id="form_login_password" placeholder="Password" name="password">
              @if ($errors->has('password'))
                <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
              @endif
            </div>
            <div class="form-group">
              <button class="btn btn-primary">@yield('buttonText')</button>
              @yield('registerLink')
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>


@endsection