<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasCompositeKey;
use Exception;

class StudentHistoryGlory extends Model
{
    use HasCompositeKey;

    protected $table = 'student_gloryhistories';
    public $timestamps = false;
    public $incrementing = false;
    protected $primaryKey = ['student_id', 'tanggal'];
    protected $keyType = 'string';

    // relationships
    public function student()
    {
        return $this->belongsTo('App\Student');
    }
}
