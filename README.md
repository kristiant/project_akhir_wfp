# Project Akhir WFP
- Nama Project: Hall of Academic Fame
- Kelompok Beta
- Finalis Ubaya Hackathon 50th

## Gambaran Sistem
Sistem ranking seluruh siswa dan seluruh sekolah yang ada di Surabaya

### User Role
- Student: siswa yang di-ranking
- Admin: administrator yang mengatur sistem (CRUD, buat ujian, validasi nilai ujian)
Seseorang boleh menjadi student dan menjadi admin sekaligus

### Gambaran
- Student mendapat ranking berdasarkan point "glory" yang dimilikinya
- Glory secara gambaran kasar adalah nilai rata-rata ulangan / test siswa tersebut dikali 1000 (misal glory 86000 berarti kurang lebih rata-rata ulangannya adalah 86)
- Student mendapat ranking berdasarkan jumlah glory yang dimilikinya (lebih tepatnya: ```ranking = jumlah siswa yang memiliki glory lebih tinggi dari dia + 1```)
- Student bisa mendapat tambahan glory dari luar ujian dengan cara mentoring siswa lain
- Admin bisa melakukan manajemen CRUD untuk resource school, student, subject, test, master-apprentice relationship, dan validasi nilai ujian

### Perhitungan Glory dari Ulangan
- Admin bisa melakukan validasi nilai ujian dengan membuka dari CRUD test (ujian) tersebut dan mengupload file csv (bisa dari excel save as)
- Perhitungan glory baru adalah (pseudocode): 
```
gloryLama := ( (gloryLama * jumlahBerapaKaliSudahUlangan) + nilaiUlanganBaru ) / (jumlahBerapaKaliSudahUlangan + 1)
```
- Jika seseorang yang nilainya di-validasi memiliki master yang mengajari dia dalam mata pelajaran yang diulangankan, jika nilai ulangannya tersebut meningkat dari rata-rata nilai ulangannya yang lama, maka masternya akan mendapat tambahan glory (ini disebut apprentice skill-up, selisih peningkatan nilai dari rata-rata ujian sebelumnya disebut skill-up point)
- Perhitungan tambahan glory dari apprentice skill-up adalah sebagai berikut (pseudocode):
```
skillUpPoint := nilaiUlanganBaru - rataRataUlanganLama
if(skillUpPoint > 5)
    masterGlory := masterGlory + ( (skillUpPoint-5) ^ 3 )
```
- Master akan mendapat tambahan glory dari apprentice-nya yang melakukan skill-up sebesar skillUpPoint^3 (pangkat 3) 
- Contoh: A mengajari B matematika, nilai ulangan matematika rata-rata si B adalah 70, pada ulangan selanjutnya si B mendapat nilai 95, maka skillUpPoint=95-70-5=20, si A mendapat tambahan glory sebesar 20^3=8000 (seperti si A mendapat peningkatan rata-rata nilai ulangan sebesar 8 poin)
- Jika skillUpPoint lebih kecil atau sama dengan 5, maka skill-up tersebut dianggap tidak signifikan dan master tidak mendapat poin apapun (5 disini dianggap sebagai batas error rata-rata nilai)
- Mengapa -5? karena dianggap kalau nilai rata-rata seseorang = 74, maka dapat 79 itu belum tentu karena dia dimentori, tapi kalo dari 74 bisa jadi 82 berarti sudah cukup maju sehingga dapat 3 skill up point

### Master-Apprentice System
Merupakan sistem mentoring yang digunakan untuk mendapat tambahan glory bagi pengajar/mentornya (disebut master), dan untuk mendapat diajari secara gratis bagi yang diajari (disebut apprentice)

Alasan penggunaan sistem mentoring:
- Seorang siswa yang sudah terlalu pintar mungkin akan merasa bosan karena tidak ada tantangan sama sekali (setiap ulangan selalu dapat 100)
- Siswa yang terlalu pintar tersebut bisa saja lebih dari satu, sedangkan mereka ingin bersaing, maka sistem mentoring ini dapat membuat nilai glory mereka lebih tinggi dari 100,000
- Sedangkan siswa yang kurang mungkin kesulitan mencari guru les (yang harganya semakin tinggi) atau mungkin malas
- Dengan sistem mentoring ini, diharapkan master akan mengajari apprentice-nya secara akademis dan berusaha memotivasi apprentice-nya untuk belajar dengan rajin (sebab jika apprentice-nya melakukan skill-up, masternya akan mendapat tambahan glory secara eksponensial)

Mengapa eksponensial?
- Tujuannya adalah untuk memotivasi master agar menjalankan sistem mentoring ini
- Memotivasi master agar memotivasi apprentice-nya agar mendapat nilai setinggi mungkin
- Serta agar master berusaha mencari dan mengajari mereka yang nilainya memang jelek
- Misal awalnya rata-rata nilai apprentice-nya 50 lalu di ulangan selanjutnya dapat 90, masternya dapat (90-50-5)^3=35^3=42,875poin, 
- Dibandingkan dengan master yang mengajari apprentice-nya yang nilai rata-rata awalnya 75, lalu di ulangan selanjutnya dapat 100, masternya dapat (100-70-5)^3=25^3=15,625poin saja (3x lipat lebih sedikit)

Fitur:
- Student bisa mencari master atau apprentice dari aplikasi
- Student bisa mendapat rekomendasi master dan apprentice dari aplikasi berdasarkan mata pelajaran yang paling dikuasainya, yang paling tidak dikuasainya, dan lokasi calon master atau apprentice yang direkomendasikan tersebut

### Perhitungan Prestige Sekolah
- Prestige sekolah didapat dengan cara merata-rata glory setiap student yang ada di sekolah tersebut
- Setiap bulan (dengan cronjobs) akan dilakukan perhitungan ulang prestige sekolah, lalu angka tersebut dicatat (untuk nantinya ditampilkan di grafik perubahan prestige sekolah)
