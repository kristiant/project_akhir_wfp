perhitungan glory siswa:

glory awal = 0

ketika ada nilai ulangan baru diinput:
glory baru = ((glory lama x count ulangan sebelumnya) + (nilai ulangan baru x 1000)) / (count ulangan sebelumnya + 1)
jadi: glory 95 000 artinya rata2 nilai ulangan anak tersebut sekitar 95

cth:
si X punya glory 92 000, dia sudah pernah ulangan 3x sebelumnya, lalu dia ulangan lagi (ini ulangannya yang ke 4) dan dapat 96
glory baru = ((92 000 x 3) + 96 000) / (3+1) 
= (276 000 + 96 000) / 4 
= 372 / 4 
= 93 000
cth: 
si Y baru masuk sekolah, dia belum pernah ulangan sebelumnya, lalu ulangan pertamanya dia dapat 84
glory baru = ((0 x 0) + 84 000) / (0+1)
= (0 + 84 000) / 1
= 84 000



battle (might want to drop this feature): 
total glory awal setiap peserta battle di suatu room = total glory akhir mereka
sistem winner take all

cth:
room 1001 bet nya 100pt, peserta ada 3 yaitu si A, B, C
yang menang si B,
maka glory si B dikurangi -100pt ditambah +300pt, si A dan si C dikurangi -100pt



mas-app apprentice level up
apa itu apprentice level up: seorang apprentice mendapat nilai yang lebih tinggi di ulangan selanjutnya (dibandingkan dengan rata-rata nilai ulangan milik dia di masa lampau pada mata pelajaran yang sama) setelah dia dimentori oleh masternya pada mata pelajaran tertentu

glory baru (milik master) = glory lama (milik master) + (skill up point milik apprentice)^3
dimana: skill up point = (nilai ulangan baru - nilai rata-rata ulangan sebelumnya) - 5, jika skill up point negatif maka dianggap nol

mengapa -5? karena dianggap kalau nilai rata-rata seseorang = 74, maka dapat 79 itu belum tentu karena dia dimentori, tapi kalo dari 74 bisa jadi 82 berarti sudah cukup maju sehingga dapat 3 skill up point



ujian yang sudah di-validasi tidak boleh dibatalkan






prestige sekolah:

prestige awal = 100 000

setiap kali ujian di-validasi, dan pada setiap akhir minggu akan dihitung ulang dengan rumus:
prestige baru = rata-rata glory milik siswanya
prestige baru = total ( glory siswa-siswa ) / jumlah siswa









battle untuk glory student:

suatu battle (bisa juga disebut room) punya id, nama, password (optional), bet, datetime mulai

battle room dibuat oleh student, pada saat membuat akan diminta input nama, password, bet, dan datetime mulai
datetime mulai harus minimal 7 hari kedepan dari created_at (agar punya waktu persiapan)

student pembuat battle room kemudian bisa menyebarkan id dan password room tersebut agar student lain bisa join battle
setelah datetime mulai, tidak boleh ada yang join/keluar battle room lagi

setelah datetime mulai, maka jika ada ujian yang di-validasi, saat memvalidasi sistem akan mengecek apakah student yang nilainya tervalidasi tadi sedang mengikuti battle room atau tidak, jika sedang mengikuti, maka nilai milik dia untuk ujian tersebut akan dicatat di battleroom_student_details
jika semua anggota battle room tersebut nilainya sudah masuk di battleroom_student_details, maka battle room tersebut ditutup dan akan dihitung siapa pemenangnya (dicari nilai tertinggi diantara battleroom_student_details)
pemenang dapat glory=glory+(jumlah student di battle room tersebut * bet)-bet
yang tidak menang dapat glory=glory-bet
















contoh testing kasus apprentice level up:
master: kristian 160416024
apprentice: sundoro 160416022
subject: matematika 1

master glory awal: 85416.7
apprentice glory awal: 83416.7

master rata-rata nilai matematika: 75
apprentice rata-rata nilai matematika: 73
apprentice rata-rata nilai matematika setelah dapat nilai baru: 81.333
master rata-rata nilai matematika setelah dapat nilai baru: 83.333

nilai baru master: 100
nilai baru apprentice: 98

skill up point = (nilai ulangan baru - nilai rata-rata ulangan sebelumnya) - 5, jika skill up point negatif maka dianggap nol
skill up point = (98 - 73) - 5 = 20

glory baru milik master setelah ditambah apprentice skill up bonus
glory baru (milik master) = glory lama (milik master) + (skill up point milik apprentice)^3
glory baru (milik master) = 85416.7 + (20)^3
glory baru (milik master) = 93416.7

glory baru milik master setelah ditambahkan nilai ulangannya sendiri:
glory baru = ((glory lama x count ulangan sebelumnya) + (nilai ulangan baru x 1000)) / (count ulangan sebelumnya + 1)
glory baru = ((93416.7 x 2) + (100 x 1000)) / (2 + 1)
glory baru = 95611












perhitungan master/apprentice yang direkomendasikan

recommended master:
1. student yang lagi login sekarang (disebut aku) dicari subject yang mana dia paling lemah (rata-rata nilai test paling rendah, disebut subject kelemahan)
2. buat query untuk meng-query semua student, sort by yang paling dekat lokasinya dengan aku
3. foreach hasil query tersebut, ambil yang nilai rata-rata pada subject kelemahan aku [90+, 85+, 80+, 70+, 50+, 30+, 0+] (dan tidak boleh sudah jadi master aku atau merupakan apprentice aku)
4. foreach berhenti ketika sudah dapat 5 calon master
5. tampilkan ke-5 calon master tersebut di tabel dan di peta

recommended apprentice:
1. student yang lagi login sekarang (disebut aku) dicari subject yang mana dia paling kuat (rata-rata nilai test paling tinggi, disebut subject terkuat)
2. buat query untuk meng-query semua student, sort by yang paling dekat lokasinya dengan aku
3. foreach hasil query tersebut, ambil yang nilai rata-rata pada subject terkuat aku [30-, 50-, 70-, 80-, 90-, 95-, 100-] (dan tidak boleh sudah jadi apprentice aku atau merupakan master aku)
4. foreach berhenti ketika sudah dapat 5 calon apprentice
5. tampilkan ke-5 calon apprentice tersebut di tabel dan di peta

arti [90+, 85+, 80+, 70+, 50+, 30+, 0+] dan [30-, 50-, 70-, 80-, 90-, 95-, 100-] :
artinya pertama ambili yang 90+ (kedua yang 95+ ketiga yang 80+ dst...), 
jika sudah selesai (habis array yang di foreach tadi) ternyata belum penuh (max 5), maka foreach lagi pakai 85+, dst...

tujuan kenapa dari 90+ ke 0+ dan dari 30- ke 100- :
jika ada subject yang sulit sekali sehingga tidak ada anak yang bisa mencapai nilai 60 misalnya (misal dapat 50 aja sudah jenius sekali), supaya tetap bisa mencari master pada subject-subject seperti itu

syarat pada foreach:
- tidak boleh sudah jadi master aku 
- tidak boleh merupakan apprentice aku
- tidak boleh rata-rata nilainya pada subject tersebut lebih rendah daripada nilai rata-rata aku pada subject tersebut
- tidak boleh sudah ada di dalam array hasil