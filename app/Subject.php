<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Exception;

class Subject extends Model
{
    protected $table = 'subjects';
    public $timestamps = true;
    protected $primaryKey = 'id';
    //protected $keyType = 'integer';

    // relationships
    public function tests()
    {
        return $this->hasMany('App\Test');
    }

    public function masterApprentices()
    {
        return $this->hasMany('App\MasterApprentice');
    }

    // other methods
    public function isCanBeDeleted()
    {
        $jumlahTest = Test::where('subject_id', $this->id)->count();
        if($jumlahTest == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
