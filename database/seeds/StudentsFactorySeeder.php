<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;


class StudentsFactorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Student::class, 1)->create()->each(function ($item) {
            $item->posts()->save(factory(App\Post::class)->make());
        });
    }
}
