<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sender_student_id');
            $table->string('receiver_student_id');
            $table->integer('subject_id')->unsigned();
            $table->string('proposal_type'); // berisi 'master', 'apprentice' (jika 'master' berarti sender ingin receiver jadi masternya)
            $table->string('status'); // berisi 'new', 'accepted', 'declined'
            $table->integer('length_in_month')->unsigned();
            $table->timestamps();

            $table->foreign('sender_student_id')->references('id')->on('students');
            $table->foreign('receiver_student_id')->references('id')->on('students');
            $table->foreign('subject_id')->references('id')->on('subjects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposals');
    }
}
