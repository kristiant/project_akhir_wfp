<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Subject;
use App\Proposal;
use App\MasterApprentice;
use Config;
use Auth;
use Exception;
use DateTime;
use DateInterval;

class MemberProposalController extends Controller
{
    public function landing()
    {
        $viewArgs["menus"] = [];
        $viewArgs["menus"]["discover-master"] =
        [
            "isActive" => false,
            "href" => "memberMasappDiscover",
            "hrefArgs" =>
            [
                "arg_master_or_apprentice" => "master"
            ],
            "text" => "Discover Masters",
            "icon" => "public",
            "color" => "primary",
            "description" => "",
        ];

        $viewArgs["menus"]["discover-apprentice"] =
        [
            "isActive" => false,
            "href" => "memberMasappDiscover",
            "hrefArgs" =>
            [
                "arg_master_or_apprentice" => "apprentice"
            ],
            "text" => "Discover Apprentices",
            "icon" => "public",
            "color" => "warning",
            "description" => "",
        ];

        $viewArgs["menus"]["notifications"] =
        [
            "isActive" => false,
            "href" => "memberMasappProposals",
            "text" => "Notifications",
            "icon" => "notifications",
            "color" => "success",
            "description" => "",
        ];

        return view('common.home_menu', $viewArgs);
    }

    public function discover($arg_master_or_apprentice = 'master', $subject_id = null)
    {
        // view args: recommendations (array of Student, nullable), mas_or_app (string enum), subject (Subject)
        $student = Auth::user()->student;



        $subject = null;
        if($subject_id == null)
        {
            if($arg_master_or_apprentice == 'master')
            {
                $subject = $student->getExtremeSubject('MIN');
            }
            else if($arg_master_or_apprentice == 'apprentice')
            {
                $subject = $student->getExtremeSubject('MAX');
            }

            if($subject == null)
            {
                $subject = Subject::all()[0];
            }
            //dd($subject); exit();
        }
        else
        {
            $subject = Subject::find($subject_id);
        }

        $recommendations = null;
        if($arg_master_or_apprentice == 'master')
        {
            $recommendations = $student->getArrRecommendedMasApp($arg_master_or_apprentice, $subject);
        }
        else if($arg_master_or_apprentice == 'apprentice')
        {
            $recommendations = $student->getArrRecommendedMasApp($arg_master_or_apprentice, $subject);
        }

        $viewArgs["student"] = $student;
        $viewArgs["recommendations"] = $recommendations;
        $viewArgs["masterOrApprentice"] = $arg_master_or_apprentice;
        $viewArgs["subject"] = $subject;
        $viewArgs["subjects"] = Subject::all();
        return view('public.masapp.discover', $viewArgs);
    }

    public function applyForm($arg_master_or_apprentice, $target_student_id, $subject_id) // display apply form
    {
        // view args: mas_or_app, target (Student),subject (Subject)
        $viewArgs["mas_or_app"] = $arg_master_or_apprentice;
        $viewArgs["targetStudent"] = Student::find($target_student_id);
        $viewArgs["subject"] = Subject::find($subject_id);

        $studentsIdName = [];
        $students = Student::all();
        foreach ($students as $key => $value)
        {
            $arr = [];
            $arr["value"] = $value->id;
            $arr["label"] = $value->nama;

            $studentsIdName[] = $arr;
        }
        $viewArgs["studentsIdName"] = $studentsIdName;
        $viewArgs["subjects"] = Subject::all();

        if( $arg_master_or_apprentice != "master" && $arg_master_or_apprentice != "apprentice" )
        {
            return redirect(route('memberMasappLanding'));
        }

        return view('public.masapp.apply', $viewArgs);
    }

    public function applyProcess(Request $request)
    {
        if($request->target_student_id == null || $request->target_student_id == -1 || !isset($request->target_student_id))
        {
            return redirect()->back()->withErrors(['target_student_id' => 'The target student must be specified']);
        }
        if($request->subject_id == null || $request->subject_id == -1 || !isset($request->subject_id))
        {
            return redirect()->back()->withErrors(['subject_id' => 'The subject must be specified']);
        }
        if($request->proposal_type != "master" && $request->proposal_type != "apprentice")
        {
            return redirect()->back()->withErrors(['proposal_type' => 'Proposal must be either "master" or "apprentice"']);
        }
        if($request->length_in_month == null || !is_numeric($request->length_in_month) || $request->length_in_month < 1)
        {
            return redirect()->back()->withErrors(['length_in_month' => 'The length must be a valid number of month']);
        }

        // cek apakah dia sudah jadi master disana
        try
        {
            $proposal = new Proposal();
            $proposal->sender_student_id = Auth::user()->student->id;
            $proposal->receiver_student_id = $request->target_student_id;
            $proposal->subject_id = $request->subject_id;
            $proposal->proposal_type = $request->proposal_type;
            $proposal->status = 'new';
            $proposal->length_in_month = $request->length_in_month;

            //$proposal->isValidForApplication();

            try
            {
                $proposal->isValidForApplication();
            }
            catch(Exception $ex)
            {
                return redirect()->back()->withErrors(['error' => $ex->getMessage()]);
            }



            $proposal->save();

            return redirect(route('memberMasappProposals'));
        }
        catch (Exception $e)
        {
            return json_encode(['status' => 'failed', 'message' => $e->getMessage()]);
        }
    }

    public function proposals()
    {
        // view args: student (yang sekarang login), lalu bisa ambil array proposals nya dari $student->proposalsReceived dan $student->proposalsSent
        $viewArgs["student"] = Auth::user()->student;

        return view('public.masapp.proposals', $viewArgs); // UNFINISHED, VIEW member_masapp_proposals BELUM DIBUAT
    }

    public function respond($proposal_id, $arg_accept_or_decline)
    {
        try
        {
            $proposal = Proposal::find($proposal_id);
            $proposal->isValidForApplication();

            if($arg_accept_or_decline == 'accept')
            {
                $proposal->status = 'accepted';
                // add masapp relationship

                $newMasterId = -1;
                $newApprenticeId = -1;

                if($proposal->proposal_type == 'master')
                {
                    $newApprenticeId = $proposal->sender->id;
                    $newMasterId = $proposal->receiver->id;
                }
                else if($proposal->proposal_type == 'apprentice')
                {
                    $newApprenticeId = $proposal->receiver->id;
                    $newMasterId = $proposal->sender->id;
                }
                else
                {
                    throw new Exception("wrong action type", 1);
                }

                $masapp = new MasterApprentice();
                $masapp->master_student_id = $newMasterId;
                $masapp->apprentice_student_id = $newApprenticeId;
                $masapp->subject_id = $proposal->subject->id;
                $masapp->started_at = date('Y-m-d H:i:s'); //$proposal->started_at; //date('Y-m-d H:i:s');

                $dateTimeNow = new DateTime("now");
                $intervalNMonth = new DateInterval("P" . $proposal->length_in_month . "M");
                $masapp->ended_at = $dateTimeNow->add($intervalNMonth)->format('Y-m-d H:i:s'); //$proposal->length_in_month;
                $masapp->save();
            }
            else
            {
                $proposal->status = 'declined';
            }

            $proposal->save();

            return json_encode(['status' => 'success']);
        }
        catch (Exception $e)
        {
            return json_encode(['status' => 'failed', 'message' => $e->getMessage()]);
        }
    }

    public function searchRedirect(Request $request)
    {
        return redirect(route('memberMasappSubject', [
            'arg_master_or_apprentice' => $request->master_or_apprentice,
            'subject_id' => $request->subject_id,
        ]));
    }
}
