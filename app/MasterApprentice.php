<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasCompositeKey;
use Exception;
use DateTime;

class MasterApprentice extends Model
{
    use HasCompositeKey;

    protected $table = 'master_apprentice';
    public $timestamps = true;
    public $incrementing = false;
    protected $primaryKey = ['master_student_id', 'apprentice_student_id', 'subject_id'];

    // relationships
    public function master()
    {
        return $this->belongsTo('App\Student', 'master_student_id');
    }

    public function apprentice()
    {
        return $this->belongsTo('App\Student', 'apprentice_student_id');
    }

    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }

    // other methods
    public function isCanBeDeleted()
    {
        return true;
    }

    public function isActive()
    {
        if($this->ended_at == null) return true;

        $dateNow = new DateTime("now");
        $dateStart = new DateTime($this->started_at);
        $dateEnd = new DateTime($this->ended_at);

        //dd($dateEnd);
        return ( $dateStart->getTimestamp() < $dateNow->getTimestamp() && $dateNow->getTimestamp() < $dateEnd->getTimestamp() );
    }
}
