<div>
  Page:
  <!-- pagination: page["current"], page["max"] -->
  @if( isset($page) )
    @if( $page["current"] != 1 )
      @if( isset($keyword) )
        <a class="" href="{{ route(\Request::route()->getName(), ['id' => 1, 'keyword' => $keyword]) }}">
      @else
        <a class="" href="{{ route(\Request::route()->getName(), ['id' => 1]) }}">
      @endif
        First
      </a>
    @endif
    @for( $i = ($page["current"]-4); $i < $page["current"]; $i++ )
      @if($i < 1)
        @php($i = 0)
        @continue
      @endif
      @if( isset($keyword) )
        <a class="" href="{{ route(\Request::route()->getName(), ['id' => $i, 'keyword' => $keyword]) }}">
      @else
        <a class="" href="{{ route(\Request::route()->getName(), ['id' => $i]) }}">
      @endif
        {{ $i }}
      </a>
    @endfor
    {{ $page["current"] }}
    @for( $i = ($page["current"]+1); $i <= $page["max"] && $i < ($page["current"]+5); $i++ )
      @if( isset($keyword) )
        <a class="" href="{{ route(\Request::route()->getName(), ['id' => $i, 'keyword' => $keyword]) }}">
      @else
        <a class="" href="{{ route(\Request::route()->getName(), ['id' => $i]) }}">
      @endif
        {{ $i }}
      </a>
    @endfor
    @if( $page["current"] != $page["max"] )
      @if( isset($keyword) )
        <a class="" href="{{ route(\Request::route()->getName(), ['id' => $page['max'], 'keyword' => $keyword]) }}">
      @else
        <a class="" href="{{ route(\Request::route()->getName(), ['id' => $page['max']]) }}">
      @endif
        Last
      </a>
    @endif
  @endif
</div>