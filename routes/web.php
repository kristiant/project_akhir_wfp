<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





// ADMIN

// ADMIN LOGIN-LOGOUT-DASHBOARD

Route::get('/admin/manage', 'HomeController@indexAdmin')->name('adminIndex'); // dashboard


// ADMIN SCHOOL

//handle search routing (chaging search keyword from POST into url)
Route::post('/admin/manage/schools/search', 'SupportController@searchSchoolAdmin')
    ->name('adminSchoolSearchRedirect')
    ->middleware('auth.is_admin');
//handle search pagination
Route::get('/admin/manage/schools/search/{keyword}/page/{page_number}', 'Admin\\SchoolsController@search')
    ->name('adminSchoolSearch')
    ->middleware('auth.is_admin');
Route::get('/admin/manage/schools/logprestige', 'Admin\\SchoolsController@logPrestige')
    ->name('adminSchoolLogPrestige')
    ->middleware('auth.is_admin');
Route::get('/admin/manage/schools/page/{page_number}', 'Admin\\SchoolsController@indexPaginated')
    ->name('adminSchoolIndexPaginated')
    ->middleware('auth.is_admin');
Route::resource('/admin/manage/schools', 'Admin\\SchoolsController')
    ->middleware('auth.is_admin'); // CRUD resource controller
//->middleware('auth') // artinya hanya yang login yang bisa akses, tapi belum di cek apakah dia admin atau bukan

// ADMIN STUDENT
Route::post('/admin/manage/students/search', 'SupportController@searchStudentAdmin')
    ->name('adminStudentSearchRedirect')
    ->middleware('auth.is_admin');
Route::get('/admin/manage/students/search/{keyword}/page/{page_number}', 'Admin\\StudentsController@search')
    ->name('adminStudentSearch')
    ->middleware('auth.is_admin');
Route::get('/admin/manage/students/page/{page_number}', 'Admin\\StudentsController@indexPaginated')
    ->name('adminStudentIndexPaginated')
    ->middleware('auth.is_admin');
Route::resource('/admin/manage/students', 'Admin\\StudentsController')
    ->middleware('auth.is_admin'); // CRUD resource controller
//->middleware('auth.is_admin') // artinya hanya yang login sebagai admin yang bisa akses,
//cara buat middleware:
// php artisan make:middleware CheckUserIsAdmin
// contoh middleware seperti di 'App\Http\Middleware\CheckUserIsAdmin.php' dan
// registerkan middleware tersebut di 'App\Http\Kernel.php' pada bagian $routeMiddleware

// ADMIN SUBJECTS
Route::post('/admin/manage/subjects/search', 'SupportController@searchSubjectAdmin')
    ->name('adminSubjectSearchRedirect')
    ->middleware('auth.is_admin');
Route::get('/admin/manage/subjects/search/{keyword}/page/{page_number}', 'Admin\\SubjectsController@search')
    ->name('adminSubjectSearch')
    ->middleware('auth.is_admin');
Route::get('/admin/manage/subjects/page/{page_number}', 'Admin\\SubjectsController@indexPaginated')
    ->name('adminSubjectIndexPaginated')
    ->middleware('auth.is_admin');
Route::resource('/admin/manage/subjects', 'Admin\\SubjectsController')
    ->middleware('auth.is_admin'); // CRUD resource controller

// ADMIN TESTS
Route::post('/admin/manage/tests/search', 'SupportController@searchTestAdmin')
    ->name('adminTestSearchRedirect')
    ->middleware('auth.is_admin');
Route::get('/admin/manage/tests/search/{keyword}/page/{page_number}', 'Admin\\TestsController@search')
    ->name('adminTestSearch')
    ->middleware('auth.is_admin');
Route::post('/admin/manage/tests/{id}/validasinilai', 'Admin\\TestsController@validasiNilai')
    ->name('adminTestValidasiNilai')
    ->middleware('auth.is_admin');
Route::get('/admin/manage/tests/page/{page_number}', 'Admin\\TestsController@indexPaginated')
    ->name('adminTestIndexPaginated')
    ->middleware('auth.is_admin');
Route::resource('/admin/manage/tests', 'Admin\\TestsController')
    ->middleware('auth.is_admin'); // CRUD resource controller

// ADMIN MASTER APPRENTICE
Route::get('/admin/manage/master_apprentice', 'Admin\\MasterApprenticeController@index')
    ->name('masapp.index')
    ->middleware('auth.is_admin'); // CRUD index manual
Route::post('/admin/manage/master_apprentice/search', 'SupportController@searchMasterApprenticeAdmin')
    ->name('adminMasAppSearchRedirect')
    ->middleware('auth.is_admin');
Route::get('/admin/manage/master_apprentice/search/{keyword}/page/{page_number}', 'Admin\\MasterApprenticeController@search')
    ->name('adminMasAppSearch')
    ->middleware('auth.is_admin');
Route::get('/admin/manage/master_apprentice/page/{page_number}', 'Admin\\MasterApprenticeController@indexPaginated')
    ->name('adminMasAppIndexPaginated')
    ->middleware('auth.is_admin');
Route::get('/admin/manage/master_apprentice/{master_student_id}/{apprentice_student_id}/{subject_id}/edit', 'Admin\\MasterApprenticeController@edit')
    ->name('masapp.edit')
    ->middleware('auth.is_admin'); // CRUD edit manual
Route::put('/admin/manage/master_apprentice/{master_student_id}/{apprentice_student_id}/{subject_id}', 'Admin\\MasterApprenticeController@update')
    ->name('masapp.update')
    ->middleware('auth.is_admin'); // CRUD update manual
Route::get('/admin/manage/master_apprentice/create', 'Admin\\MasterApprenticeController@create')
    ->name('masapp.create')
    ->middleware('auth.is_admin'); // CRUD create manual
Route::post('/admin/manage/master_apprentice', 'Admin\\MasterApprenticeController@store')
    ->name('masapp.store')
    ->middleware('auth.is_admin'); // CRUD store manual
Route::delete('/admin/manage/master_apprentice/{master_student_id}/{apprentice_student_id}/{subject_id}', 'Admin\\MasterApprenticeController@destroy')
    ->name('masapp.destroy')
    ->middleware('auth.is_admin'); // CRUD delete manual
// Route::resource('/admin/manage/master_apprentice', 'Admin\\MasterApprenticeController'); // CRUD resource controller



// PUBLIC RANK

// RANK STUDENTS
Route::get('/rank', 'StudentRankPagesController@rankGlobalIndex')
    ->name('publicStudentIndex'); // suruh redirect ke /rank/page/1 !!
Route::get('/rank/page/{page_number}', 'StudentRankPagesController@rankGlobal')
    ->name('publicStudentIndexPaginated'); // keseluruhan / global
Route::get('/rank/level/{level_jenjang}/page/{page_number}', 'StudentRankPagesController@rankLevel'); // di jenjang yang sama (SD saja, atau SMP saja, atau SMA saja)

Route::post('student/search', 'SupportController@searchStudentPublic')
    ->name('publicStudentSearchProcess');
Route::get('/student/search/{keyword}/page/{page_number}', 'StudentRankPagesController@search')
    ->name('publicStudentSearch'); // search
Route::get('/student/{id}', 'StudentRankPagesController@profile')
    ->name('publicStudentProfile'); // profile

// RANK SCHOOLS
Route::get('/rank/schools', 'SchoolRankPagesController@rankGlobalIndex')
    ->name('publicSchoolIndex'); // suruh redirect ke /rank/schools/page/1 !!
Route::get('/rank/schools/page/{page_number}', 'SchoolRankPagesController@rankGlobal')
    ->name('publicSchoolIndexPaginated'); // keseluruhan / global
Route::get('/rank/schools/level/{level_jenjang}/page/{page_number}', 'SchoolRankPagesController@rankLevel'); // di kota yang sama

Route::post('school/search', 'SupportController@searchSchoolPublic')
    ->name('publicSchoolSearchProcess');
Route::get('/school/search/{keyword}/page/{page_number}', 'SchoolRankPagesController@search')
    ->name('publicSchoolSearch'); // search
Route::get('/school/{id}', 'SchoolRankPagesController@profile')
    ->name('publicSchoolProfile'); // profile



// STUDENT MEMBER MASTER-APPRENTICE PROPOSAL
Route::get('/masapp', 'MemberProposalController@landing') // display 2 choice, discover master, discover apprentice, notif (dari proposals)
    ->name('memberMasappLanding')
    ->middleware('auth.is_student');
Route::get('/masapp/discover/{arg_master_or_apprentice}', 'MemberProposalController@discover') // display map and table
    ->name('memberMasappDiscover')
    ->middleware('auth.is_student');
Route::get('/masapp/discover/{arg_master_or_apprentice}/{subject_id}', 'MemberProposalController@discover') // display map and table
    ->name('memberMasappSubject')
    ->middleware('auth.is_student');
Route::post('/masapp/discover', 'MemberProposalController@searchRedirect') // display map and table
    ->name('memberMasappDiscoverSearchRedirect')
    ->middleware('auth.is_student');

Route::get('/masapp/apply/{arg_master_or_apprentice}/{target_student_id}/{subject_id}', 'MemberProposalController@applyForm')
    ->name('memberMasappApplyForm')
    ->middleware('auth.is_student');
Route::post('/masapp/apply', 'MemberProposalController@applyProcess')
    ->name('memberMasappApplyProcess')
    ->middleware('auth.is_student');

Route::get('/masapp/proposals', 'MemberProposalController@proposals')
    ->name('memberMasappProposals')
    ->middleware('auth.is_student');
Route::post('/masapp/proposals/{proposal_id}/{arg_accept_or_decline}', 'MemberProposalController@respond')
    ->name('memberMasappRespond')
    ->middleware('auth.is_student');



// LOGIN PAGE
//Route::get('/', 'PagesController@indexPublic')->name('#');
/*
Route::get('/login', 'PagesController@loginPublic');
Route::post('/login', 'PagesController@loginPublicProcess');
Route::get('/logout', 'PagesController@logoutPublic');
*/

// OLD, DEPRECATED, FOR HACKATHON DEMO
/*
Route::get('/', 'PagesController@loginPage');
Route::get('/login', 'PagesController@loginPage');

Route::get('/table_students', 'PagesController@listStudentsSimple');
Route::get('/students', 'PagesController@listStudents');
Route::get('/admin_students', 'PagesController@listStudentsAdmin');
Route::get('/student/{student_id}', 'PagesController@studentDetail');
Route::get('/discover', 'PagesController@googleMaps');
Route::get('/dashboard', 'PagesController@dashboard');
Route::get('/schools', 'PagesController@listSchools');
Route::get('/school/{school_id}', 'PagesController@schoolDetail');

Route::get('/battle', 'PagesController@battle');
Route::get('/battle/room/{battle_room_id}', 'PagesController@battleRoom');

*/








// common usage (both admin and normal user)
Auth::routes(); // berisi /login, /logout

Route::get('/home', 'HomeController@home')->name('home');
Route::get('/myprofile', 'HomeController@myProfile')->name('myProfile');

Route::get('/', 'PagesController@root')->name('root');
Route::get('/dev', 'PagesController@dev')->name('#');


Route::get('dev_kris', function()
{
    $kris = \App\Student::find(160416024);
    dd($kris->tests);
});