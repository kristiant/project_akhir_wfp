<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(SchoolSeeder::class);
        $this->call(StudentsSeeder::class);
        $this->call(AdminUserSeeder::class);
        //$this->call(StudentsFactorySeeder::class);
        Model::reguard();
    }
}
