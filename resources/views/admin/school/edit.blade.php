@extends('admin.school.template')
@section('content')

  <div class="content">
    <div class="container-fluid">

      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              @if( $school->id != -1 )
                Edit School
              @else
                Create New School
              @endif
            </h4>
            <p class="card-category">
              @if( $school->id != -1 )
                NPSN: {{ $school->id }}
              @endif
            </p>
          </div>
          <div class="card-body">
            @if( $school->id != -1 )
              <form action="{{ route('schools.update', ['id' => $school->id]) }}" method="POST" enctype="multipart/form-data">
            @else
              <form action="{{ route('schools.store') }}" method="POST" enctype="multipart/form-data">
            @endif

              @if( $school->id != -1 )
                @method('PUT')
              @else
                @method('POST')
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group label-floating
                    @if($errors->has('id'))
                      has-error
                    @endif">
                      <label class="bmd-label-floating">NPSN</label>
                      <input type="text" class="form-control form-input-number" name="id" value="">
                      @if($errors->has('id'))
                        <span class="label-error">{{ $errors->first('id') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
              @endif
              @csrf
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group label-floating
                    @if($errors->has('nama'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">Nama</label>
                    <input type="text" class="form-control" name="nama" value="{{ $school->nama }}">
                    @if($errors->has('nama'))
                      <span class="label-error">{{ $errors->first('nama') }}</span>
                    @endif
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group label-floating
                    @if($errors->has('alamat'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">Alamat</label>
                    <input type="text" class="form-control" name="alamat" value="{{ $school->alamat }}">
                    @if($errors->has('alamat'))
                      <span class="label-error">{{ $errors->first('alamat') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group label-floating
                    @if($errors->has('status'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">Status</label>
                    <input type="text" class="form-control" name="status" value="{{ $school->status }}">
                    @if($errors->has('status'))
                      <span class="label-error">{{ $errors->first('status') }}</span>
                    @endif
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group label-floating
                    @if($errors->has('jenjang'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">Jenjang</label>
                    <input type="text" class="form-control" name="jenjang" value="{{ $school->jenjang }}">
                    @if($errors->has('jenjang'))
                      <span class="label-error">{{ $errors->first('jenjang') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group label-floating
                    @if($errors->has('prestige'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">Prestige</label>
                    <input type="text" class="form-control form-input-number" name="prestige" value="{{ round($school->prestige) }}">
                    @if($errors->has('prestige'))
                      <span class="label-error">{{ $errors->first('prestige') }}</span>
                    @endif
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group label-floating
                    @if($errors->has('level'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">Level</label>
                    <input type="text" class="form-control form-input-number" name="level" value="{{ $school->level }}">
                    @if($errors->has('level'))
                      <span class="label-error">{{ $errors->first('level') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group label-floating
                    @if($errors->has('x'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">Posisi (X)</label>
                    <input type="text" class="form-control form-input-number" name="x" value="{{ $school->x }}">
                    @if($errors->has('x'))
                      <span class="label-error">{{ $errors->first('x') }}</span>
                    @endif
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group label-floating
                    @if($errors->has('y'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">Posisi (Y)</label>
                    <input type="text" class="form-control form-input-number" name="y" value="{{ $school->y }}">
                    @if($errors->has('y'))
                      <span class="label-error">{{ $errors->first('y') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <label class="bmd-label-floating">Upload Picture: </label>
                  <input type="file" name="picture" class="btn btn-primary" id="form_input_picture">
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <label class="bmd-label-floating">Foto Lama</label> <br>
                  <img src="{{ $school->getImageAsset() }}" id="form_picture_preview_old" style="width: 100%; height: auto" />
                </div>
                <div class="col-md-6">
                  <label class="bmd-label-floating">Foto Baru</label> <br>
                  <img id="form_picture_preview_new" src="{{ asset('img/no-image.png') }}" style="width: 100%; height: auto" />
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Save</button>
              <a href="{{ route('schools.index') }}"><button type="button" class="btn btn-secondary pull-right">Cancel</button></a>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>

    </div>
  </div>

@endsection

@section('scripts')
  <script type="text/javascript">
    $('#form_input_picture').change(function()
      {
        if (this.files && this.files[0])
        {
          var reader = new FileReader();
          reader.onload = function(e)
          {
              document.getElementById('form_picture_preview_new').src = e.target.result;
          }

          reader.readAsDataURL(this.files[0]);
        }
        else
        {
          document.getElementById('form_picture_preview_new').src='{{ asset('img/no-image.png') }}';
        }
      });
  </script>
@endsection