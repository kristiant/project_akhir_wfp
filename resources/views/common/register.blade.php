@extends('common.template_register')


@section('title')
  User Registration
@endsection

@section('searchBarVisibility')
  none
@endsection

@section('logoutTopNav')
@endsection

@section('text')
  Register to Hall of Academic Fame
@endsection

@section('formActionMethod')
  action="{{ route('register') }}" method="POST"
@endsection

@section('buttonText')
  Register
@endsection

@section('loginLink')
  <a href="{{ route('login') }}" class="btn btn-secondary">Log In</a>
@endsection