<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /*
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        $this->guard()->login($user);

        $registered = $this->registered($request, $user);
        if($registered)
        {
            return $registered;
        }
        else
        {
            return redirect($this->redirectPath());
        }
    }
    */

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'id' => 'required|integer|exists:students,id|unique:users',
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'nama' => 'required|string|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        /*
        return User::create([
            'id' => $data['id'],
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'nama' => $data['nama'],
            'is_admin' => false,
            'is_student' => true,
        ]);
        */

        $user = new User();
        $user->id = $data['id'];
        $user->username = $data['username'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->nama = $data['nama'];
        $user->is_admin = false;
        $user->is_student = true;
        $user->save();
        return $user;
    }

    public function showRegistrationForm()
    {
        $viewArgs["fields"] = [
            "id" => [
                "name" => "id",
                "type" => "text",
                "readable" => "NIK",
                "hint" => "NIK anda sesuai KTP/KK",
            ],
            "username" => [
                "name" => "username",
                "type" => "text",
                "readable" => "User Name",
                "hint" => "Any combination of alphanumeric character",
            ],
            "email" => [
                "name" => "email",
                "type" => "email",
                "readable" => "E-Mail",
            ],
            "password" => [
                "name" => "password",
                "type" => "password",
                "readable" => "Password",
                "hint" => "Minimum 6 characters",
            ],
            "password_confirmation" => [
                "name" => "password_confirmation",
                "type" => "password",
                "readable" => "Confirm Password",
                "hint" => "Repeat your password",
            ],
            "nama" => [
                "name" => "nama",
                "type" => "text",
                "readable" => "Nama Lengkap",
            ],
        ];
        return view('common.register', $viewArgs);
    }
}
