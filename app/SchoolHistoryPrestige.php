<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasCompositeKey;
use Exception;

class SchoolHistoryPrestige extends Model
{
    use HasCompositeKey;

    protected $table = 'school_prestigehistories';
    public $timestamps = false;
    public $incrementing = false;
    protected $primaryKey = ['school_id', 'tanggal'];
    //protected $keyType = ['string', 'dateTime'];
    protected $keyType = 'string';

    // relationships
    public function school()
    {
        return $this->belongsTo('App\School');
    }
}
