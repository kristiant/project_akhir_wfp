@extends('admin.test.template')
@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">
                Tests
                <a class="btn btn-success" style="float: right;" href="{{ route('tests.create') }}">
                  <i class="material-icons">add</i> Add New
                  <div class="ripple-container"></div>
                </a>
              </h4>
              <p class="card-category">Manage tests</p>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>ID</th>
                    <th>Sekolah</th>
                    <th>Subject</th>
                    <th>Summary</th>
                    <th>Jenis</th>
                    <th>Tanggal</th>
                    <th>V?</th>
                    <th>Action</th>
                  </thead>
                  <tbody>
                    @foreach($tests as $row)
                      <tr>
                        <td>{{ $row->id }}</td>
                        <td>{{ $row->school_id }} - {{ $row->school->nama }}</td>
                        <td>{{ $row->subject_id }} - {{ $row->subject->nama }}</td>
                        <td>{{ $row->summary }}</td>
                        <td>{{ $row->jenis }}</td>
                        <td>{{ $row->tanggal }}</td>
                        <td>
                          @if($row->is_validated == true)
                            V
                          @else
                            -
                          @endif
                        </td>
                        <td>
                          <a href="{{ route('tests.edit', ['id' => $row->id]) }}"><button class="btn-small btn-primary">Edit</button></a>
                          @if($row->isCanBeDeleted())
                            <button class="btn-small btn-danger" onclick="changeToDeleteId({{ $row->id }});" data-toggle="modal" data-target="#modal_delete">Delete</button>
                          @else
                            <button class="btn-small btn-secondary" disabled>Delete</button>
                          @endif
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              @include('common.addon_pagination')
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="modal_delete_title" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_delete_title">Delete</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Are you sure you want to delete this data?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger" onclick="deleteItem();">Delete</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript">
    var toDeleteId = -1;
    function deleteItem()
    {
      var urlFormat = "{{ route('tests.destroy', ['id' => 'VAR_ID']) }}";
      urlFormat = urlFormat.replace('VAR_ID', toDeleteId);
      //alert(urlFormat);
      $.ajax({
        type: "DELETE",
        url: urlFormat,
        headers: { "X-CSRF-TOKEN" : "{{ csrf_token() }}" },
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (data)
        {
          varResultObj = JSON.parse(data); // assume the return will always be {"status" : "success"} OR {"status" : "fail"}
          if(varResultObj.status == "success")
          {
            window.location = "{{ route('tests.index') }}";
          }
          else
          {
            alert('ERROR, response: ' + data);
          }
        },
        error: function (e) {
          alert('Cannot delete record, foreign key constraint failed!');
          console.log("ERROR : ", e);
        }
      });
    }
    function changeToDeleteId(newId)
    {
      toDeleteId = newId;
    }
  </script>
@endsection