@extends('common.template_app')

@section('searchBarVisibility')
  none
@endsection

@section('sidebarNavItems')
  @foreach($menus as $item)
    <li class="nav-item">
      <a class="nav-link"
      @if(isset($item['hrefArgs']))
        href="{{ route($item['href'], $item['hrefArgs']) }}"
      @else
        href="{{ route($item['href']) }}"
      @endif
      >
        <i class="material-icons">{{ $item["icon"] }}</i>
        <p>{{ $item["text"] }}</p>
      </a>
    </li>
  @endforeach
@endsection

@section('content')

  <div class="content">
    <div class="container-fluid">

      <div class="row">
        @foreach($menus as $item)
          <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-header card-header-{{ $item["color"] }} card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">{{ $item["icon"] }}</i>
                </div>
                <p class="card-category">{{ $item["text"] }}</p>
                <h3 class="card-title">
                  {{ $item["description"] }}
                </h3>
              </div>
              <div class="card-body">
                <a
                @if(isset($item['hrefArgs']))
                  href="{{ route($item['href'], $item['hrefArgs']) }}"
                @else
                  href="{{ route($item['href']) }}"
                @endif
                class="btn btn-{{ $item['color'] }}">Select</a></div>
            </div>
          </div>
        @endforeach
      </div>

    </div>
  </div>

@endsection