@extends('common.template_app')

@section('searchBarVisibility')
  block
@endsection

@section('searchBarValue')
@if( isset($keyword) )
{{ $keyword }}
@endif
@endsection

@section('sidebarNavItems')
  <li class="nav-item" id="nav_item_students">
    <a class="nav-link" href="{{ route('publicStudentIndex') }}">
      <i class="material-icons">people</i>
      <p>Students Ranking</p>
    </a>
  </li>
  <li class="nav-item" id="nav_item_schools">
    <a class="nav-link" href="{{ route('publicSchoolIndex') }}">
      <i class="material-icons">school</i>
      <p>Schools Ranking</p>
    </a>
  </li>
  <li class="nav-item" id="nav_item_mas-app">
    <a class="nav-link" href="{{ route('memberMasappLanding') }}">
      <i class="material-icons">group_work</i>
      <p>Mas-App</p>
    </a>
  </li>
  
@endsection