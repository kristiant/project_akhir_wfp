@extends('child_profile')
@section('content_profile')
  <h6>Stats</h6>
  <div style="width: 300px; height: 300px;">
    <canvas id="chart_radar_stats"></canvas>
  </div>

  <div style="width: 300px; height: 300px;">
    <canvas id="chart_line_spechistory"></canvas>
  </div>

  <hr>


  <div class="alert alert-info">
    <span>Glory: 13945721</span>
  </div>
  <div class="alert alert-info">
    <span>Apprentices: 5 people</span>
  </div>
  <div class="alert alert-success">
    <span>Apprentice Skill-Up: 3x</span>
  </div>
  <div class="alert alert-danger">
    <span>Battles: 2 win, 1 lose (67%)</span>
  </div>




  <hr>

  <div class="alert alert-info">
    <span>Skill Tree (3% unlocked)</span>
    <img style="width: 100%;height: auto;" src="{{ asset('img/skill-tree-crop.png') }}">
  </div>

  <hr>

  @if( isset($plainContent) )
    <h6>Location</h6>
    {!! $plainContent !!}
  @endif

@endsection

@section('scripts')
  <!-- script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.js"></script -->
  <script src="{{ asset('js/plugins/Chart.js') }}"></script>
  <script type="text/javascript">
    var ctx = document.getElementById('chart_radar_stats').getContext('2d');

    var radarChart = new Chart(ctx, {
      type: 'radar',
      data: @json($radar),
      options: {
        scale: {
            display: true
        }
      }
    });
  </script>
  <script type="text/javascript">
    var ctx1 = document.getElementById('chart_line_spechistory').getContext('2d');

    var radarChart1 = new Chart(ctx1, {
      type: 'line',
      data: @json($lineHistory),
      options: {
        title: {
          display: true,
          text: 'Specialization Score History'
        }
      }
    });
  </script>
@endsection