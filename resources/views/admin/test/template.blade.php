@extends('admin.template')

@section('searchBarAction')
  {{ route('adminTestSearchRedirect') }}
@endsection

@section('searchBarVisibility')
  inline-block
@endsection

@section('scriptNav')
  <script type="text/javascript">
    $("#nav_item_tests").addClass("active");
  </script>
@endsection