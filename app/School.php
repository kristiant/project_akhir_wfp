<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Config;
use Exception;

class School extends Model
{
    protected $table = 'schools';
    public $timestamps = true;
    protected $primaryKey = 'id';
    protected $keyType = 'string';

    // overriding general methods
    public function delete()
    {
        SchoolHistoryPrestige::where('school_id', $this->id)->delete();
        parent::delete();
    }

    // relationships
    public function students()
    {
        return $this->hasMany('App\Student');
    }

    public function schoolPrestigeHistories()
    {
        return $this->hasMany('App\SchoolHistoryPrestige');
    }

    public function tests()
    {
        return $this->hasMany('App\Test');
    }

    // other methods
    public function logPrestige()
    {
        /*
        //intinya: insert into history_prestige values ($this->id, date(), $this->prestige)
        DB::table('schools_history_prestige')->insert([
            'school_id' => $this->id,
            'tanggal' => date('Y-m-d'),
            'prestige' => $this->prestige,
        ]);
        */

        $history = new SchoolHistoryPrestige();
        $history->school_id = $this->id;
        $history->tanggal = date('Y-m-d');
        $history->prestige = $this->prestige;

        $history->save();
    }

    public function getImageAsset()
    {
        if (File::exists(base_path('public').'/img/schools/' . $this->id . '.png'))
        {
            return asset('/img/schools/' . $this->id . '.png');
        }
        else if (File::exists(base_path('public').'/img/schools/' . $this->id . '.jpg'))
        {
            return asset('/img/schools/' . $this->id . '.jpg');
        }
        else
        {
            return asset('/img/no-image.png');
        }
    }

    public function recountPrestige()
    {
        $query = Student::where('school_id', $this->id)->avg('glory');

        if($query == 0 || $query == null) { $query = 0; }

        $this->prestige = $query;
        //return $this->prestige;

        return $query;
    }

    public function getRank()
    {
        // ranking berapa School $this sekarang
        $higherSchools = School::where('prestige', '>', $this->prestige);
        return $higherSchools->count() + 1;
    }

    public function getChartLinePrestigeHistoryData()
    {
        $labels = [];
        $data = [];

        $histories = SchoolHistoryPrestige::where('school_id', $this->id)
            ->orderBy('tanggal', 'desc')
            ->take(Config::get('kristian.history_school_numberofitems'))
            ->get();

        foreach ($histories->reverse() as $key => $value)
        {
            $labels[] = $value->tanggal;
            $data[] = $value->prestige;
        }
        $labels[] = "Now";
        $data[] = $this->prestige;

        return [
            "labels" => $labels,
            "datasets" => [
                [
                    "label" => "Glory",
                    "backgroundColor" => "rgba(0, 0, 0, 0.5)",
                    "data" => $data,
                    "borderColor" => "rgb(255,255,255)",
                    "fill" => false,
                    "lineTension" => 0
                ],
            ],
        ];
    }

    public function isCanBeDeleted()
    {
        $jumlahStudent = Student::where('school_id', $this->id)->count();
        $jumlahTest = Test::where('school_id', $this->id)->count();
        if( $jumlahStudent == 0 && $jumlahTest == 0 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
