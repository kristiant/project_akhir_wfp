@extends('public.template')

@section('searchBarAction')
  {{ route('publicSchoolSearchProcess') }}
@endsection

@section('scriptNav')
  <script type="text/javascript">
    $("#nav_item_schools").addClass("active");
  </script>
@endsection