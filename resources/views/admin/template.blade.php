@extends('common.template_app')

@section('searchBarVisibility')
  none
@endsection

@section('searchBarValue')
@if( isset($keyword) )
{{ $keyword }}
@endif
@endsection

@section('sidebarNavItems')
  <li class="nav-item" id="nav_item_home_admin">
    <a class="nav-link" href="{{ route('adminIndex') }}">
      <i class="material-icons">settings</i>
      <p>Administration Panel</p>
    </a>
  </li>
  <li class="nav-item" id="nav_item_schools">
    <a class="nav-link" href="{{ route('schools.index') }}">
      <i class="material-icons">school</i>
      <p>Schools</p>
    </a>
  </li>
  <li class="nav-item" id="nav_item_students">
    <a class="nav-link" href="{{ route('students.index') }}">
      <i class="material-icons">people</i>
      <p>Students</p>
    </a>
  </li>
  <li class="nav-item" id="nav_item_subjects">
    <a class="nav-link" href="{{ route('subjects.index') }}">
      <i class="material-icons">book</i>
      <p>Subjects</p>
    </a>
  </li>
  <li class="nav-item" id="nav_item_tests">
    <a class="nav-link" href="{{ route('tests.index') }}">
      <i class="material-icons">library_books</i>
      <p>Tests</p>
    </a>
  </li>
  <li class="nav-item" id="nav_item_mas-app">
    <a class="nav-link" href="{{ route('masapp.index') }}">
      <i class="material-icons">group_work</i>
      <p>Mas-App</p>
    </a>
  </li>
@endsection

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">
                Please select an item to be managed
              </h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scriptNav')
  <script type="text/javascript">
    $("#nav_item_home_admin").addClass("active");
  </script>
@endsection