<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->string('id');
            $table->string('school_id');
            $table->string('nama');
            $table->integer('kelas'); // kelas 1-6
            $table->double('glory', 20, 10); // jumlah glory
            $table->string('specialization'); // nama subject spesialisasinya, bisa diisi bebas
            $table->integer('apprentice_skill_up'); // berapa kali apprenticenya skill up
            $table->double('x', 20, 10);
            $table->double('y', 20, 10);
            $table->timestamps();

            $table->primary('id');
            $table->foreign('school_id')->references('id')->on('schools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
