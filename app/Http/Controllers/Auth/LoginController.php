<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
//use App\Http\Controllers\Auth\Request;
use Illuminate\Http\Request;
use Auth;
use Config;
//use Illuminate\Foundation\Validation\ValidationException;
//use ValidationException;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';
    public function redirectTo()
    {
        return route('home');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /* protected function credentials(Request $request)
    {
        return $request->only('username', 'password');
    } */

    protected function validateLogin(Request $request)
    {
       $this->validate($request, [
            'username' => 'required|string',
            'password' => 'required|string'
        ]);
    }

    /* public function authenticate(Request $request)
    {
        dd($request); exit;

        if (Auth::attempt(['id'=> $request->username, 'password' => $request->password])) {
            return redirect()->intended('dashboard');
        }
        if (Auth::attempt(['username'=> $request->username, 'password' => $request->password])) {
            return redirect()->intended('dashboard');
        }
        if (Auth::attempt(['email'=> $request->username, 'password' => $request->password])) {
            return redirect()->intended('dashboard');
        }

        return redirect(route('subjects.index'));
    }*/

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if (Auth::attempt(['id'=> $request->username, 'password' => $request->password]))
        {
            //return redirect()->intended('dashboard');
            return $this->sendLoginResponse($request);
        }
        if (Auth::attempt(['username'=> $request->username, 'password' => $request->password]))
        {
            return $this->sendLoginResponse($request);
        }
        if (Auth::attempt(['email'=> $request->username, 'password' => $request->password]))
        {
            return $this->sendLoginResponse($request);
        }

        //return redirect(route('subjects.index'));
        /*
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
        */
        return redirect()->back()->withInput($request->all())->withErrors([
            'auth.failed' => 'Wrong username or password',
        ]);
    }

    public function showLoginForm()
    {
        return view('common.login');
    }
}
