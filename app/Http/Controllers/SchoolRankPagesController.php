<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;
use App\Student;
use Config;

class SchoolRankPagesController extends Controller
{
    public function rankGlobalIndex()
    {
        return redirect(route('publicSchoolIndexPaginated', ['page_number' => 1]));
    }

    public function rankGlobal($page_number)
    {
        $query = $query = School::orderBy('prestige', 'desc')->take(Config::get('kristian.max_rank_displayed'))->get();
        return $this->displayRank($query, null, $page_number);
    }

    /* public function rankLocal($page_number) { // return $this->displayRank($query); } */

    public function rankLevel($level_jenjang, $page_number)
    {
        // return $this->displayRank($query);
    }

    public function search($keyword, $page_number)
    {
        // return $this->displayRank($query);
        $query = School::where('nama', 'like', '%' . $keyword . '%')
            ->orWhere('id', 'like', $keyword . '%')
            ->get();
        return $this->displayRank($query, $keyword, $page_number);
    }

    private function displayRank($query, $keyword, $page_number)
    {
        $schools = $query->forPage($page_number, Config::get('kristian.pagination_max_item'));
        $viewArgs["schools"] = $schools;

        $viewArgs["page"]["current"] = $page_number;
        $viewArgs["page"]["max"] = ceil($query->count() / Config::get('kristian.pagination_max_item'));
        //dd($viewArgs); exit;
        if($viewArgs["page"]["max"] == 0) $viewArgs["page"]["max"] = 1;

        if($keyword != null) $viewArgs["keyword"] = $keyword;

        return view('public.school.list', $viewArgs);
    }

    public function profile($id)
    {
        $school = School::find($id);
        $topStudents = Student::where('school_id', $school->id)
            ->orderBy('glory', 'desc')
            ->take(5)
            ->get();

        $viewArgs["school"] = $school;
        $viewArgs["topStudents"] = $topStudents;

        return view('public.school.profile', $viewArgs);
    }
}
