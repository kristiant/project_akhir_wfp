@extends('admin.subject.template')
@section('content')

  <div class="content">
    <div class="container-fluid">

      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              @if( $subject->id != -1 )
                Edit Subject
              @else
                Create New Subject
              @endif
            </h4>
            <p class="card-category">
              @if( $subject->id != -1 )
                ID: {{ $subject->id }}
              @endif
            </p>
          </div>
          <div class="card-body">
            @if( $subject->id != -1 )
              <form action="{{ route('subjects.update', ['id' => $subject->id]) }}" method="POST" enctype="multipart/form-data">
            @else
              <form action="{{ route('subjects.store') }}" method="POST" enctype="multipart/form-data">
            @endif

              @if( $subject->id != -1 )
                @method('PUT')
              @else
                @method('POST')
              @endif
              @csrf
              <div class="row">
                <div class="col-md-9">
                  <div class="form-group label-floating
                    @if($errors->has('nama'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">Nama</label>
                    <input type="text" class="form-control" name="nama" value="{{ $subject->nama }}">
                    @if($errors->has('nama'))
                      <span class="label-error">{{ $errors->first('nama') }}</span>
                    @endif
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group label-floating
                    @if($errors->has('singkatan'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">Singkatan</label>
                    <input type="text" class="form-control" name="singkatan" value="{{ $subject->singkatan }}">
                    @if($errors->has('singkatan'))
                      <span class="label-error">{{ $errors->first('singkatan') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Save</button>
              <a href="{{ route('subjects.index') }}"><button type="button" class="btn btn-secondary pull-right">Cancel</button></a>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>

    </div>
  </div>

@endsection