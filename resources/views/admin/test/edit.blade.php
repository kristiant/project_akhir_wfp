@extends('admin.test.template')

@section('head')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.datetimepicker.min.css') }}">
@endsection

@section('content')

  <div class="content">
    <div class="container-fluid">

      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              @if( $test->id != -1 )
                Edit Test
              @else
                Create New Test
              @endif
            </h4>
            <p class="card-category">
              @if( $test->id != -1 )
                ID: {{ $test->id }}
              @endif
            </p>
          </div>
          <div class="card-body">
            @if( $test->id != -1 )
              <form action="{{ route('tests.update', ["id" => $test->id]) }}" method="POST" enctype="multipart/form-data">
            @else
              <form action="{{ route('tests.store') }}" method="POST" enctype="multipart/form-data">
            @endif

              @if( $test->id != -1 )
                @method('PUT')
              @else
                @method('POST')
              @endif
              @csrf
              <div class="row">
                <div class="col-md-9">
                  <div class="form-group label-floating
                    @if($errors->has('school_id'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">School</label>
                    <!-- input type="text" class="form-control" name="school_id" value="{{ $test->school_id }}" -->
                    <input type="text" class="form-control" name="school_name" id="school_name"
                    @if($test->school_id == -1)
                      value="Type school name or NPSN here ..."
                    @else
                      value="{{ $test->school->nama }} - NPSN {{ $test->school->id }}"
                    @endif
                    >
                    <input type="hidden" name="school_id" id="school_id" value="{{ $test->school_id }}">
                    <!-- input type="hidden" name="json_nilai" id="json_nilai" -->
                    @if($errors->has('school_id'))
                      <span class="label-error">{{ $errors->first('school_id') }}</span>
                    @endif
                  </div>
                </div>
                <div class="col-md-3">
                  @if($test->is_validated == true)
                    Validated at: {{ $test->validated_at }}
                  @endif
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group label-floating
                    @if($errors->has('tanggal'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">Tanggal</label>
                    <input type="text" class="form-control" id="input_datetimepicker_testtanggal" name="tanggal_picker" value="{{ $test->tanggal }}">
                    <input type="text" class="form-control disabled" disabled id="input_text_confirmtanggal" name="tanggal_confirm" value="{{ $test->tanggal }}">
                    <input type="hidden" id="input_text_tanggal" name="tanggal" value="{{ $test->tanggal }}">
                    <input type="text" class="form-control disabled" disabled id="input_text_readabletanggal" name="tanggal_readable" value="{{ (new DateTime($test->tanggal))->format('r') }}">
                    @if($errors->has('tanggal'))
                      <span class="label-error">{{ $errors->first('tanggal') }}</span>
                    @endif
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group label-floating
                    @if($errors->has('subject_id'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">Subject</label>
                    <!-- input type="text" class="form-control" name="subject_id" value="{{ $test->subject_id }}" -->
                    <select name="subject_id" class="form-control">
                      @foreach($subjects as $subject)
                        <option value="{{ $subject->id }}"
                          @if($test->subject_id == $subject->id)
                            selected
                          @endif
                        >
                          {{ $subject->nama }}
                        </option>
                      @endforeach
                    </select>
                    @if($errors->has('subject_id'))
                      <span class="label-error">{{ $errors->first('subject_id') }}</span>
                    @endif
                  </div>

                  <div class="form-group label-floating
                    @if($errors->has('summary'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">Summary</label>
                    <input type="text" class="form-control" name="summary" value="{{ $test->summary }}">
                    @if($errors->has('summary'))
                      <span class="label-error">{{ $errors->first('summary') }}</span>
                    @endif
                  </div>

                  <div class="form-group label-floating
                    @if($errors->has('jenis'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">Jenis</label>
                    <input type="text" class="form-control" name="jenis" value="{{ $test->jenis }}">
                    @if($errors->has('jenis'))
                      <span class="label-error">{{ $errors->first('jenis') }}</span>
                    @endif
                  </div>
                </div>
              </div>

              <button type="submit" class="btn btn-primary pull-right">Save</button>
              <a href="{{ route('tests.index') }}"><button type="button" class="btn btn-secondary pull-right">Cancel</button></a>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>

        @if($test->id == -1)
          <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">
                  Daftar Nilai
                </h4>
                <p class="card-category">
                  Input file CSV setelah Test disimpan
                  <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal_help" style="float: right;">
                    CSV Help
                  </button>
                </p>

              </div>
              <div class="card-body">
                Daftar nilai hanya boleh dimasukkan saat Test sudah disimpan, harap simpan Test dulu sebelum meng-upload nilai
              </div>
          </div>
        @else
          @if($test->is_validated == false)
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">
                  Daftar Nilai
                </h4>
                <p class="card-category">
                  Input file CSV
                  <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal_help" style="float: right;">
                    CSV Help
                  </button>
                </p>

              </div>
              <div class="card-body">
                <form> <!-- DUMMY FORM -->
                  <div class="row">
                    <div class="col-md-12">
                      <label class="bmd-label-floating">Upload Excel CSV: </label>
                      <input type="file" name="file_nilai_csv" class="btn btn-primary" id="file_nilai_csv">
                      <div class="table-nilai"></div>
                    </div>
                  </div>
                </form>

                <form action="{{ route('adminTestValidasiNilai', ["id" => $test->id]) }}" method="POST"> <!-- REAL FORM -->
                  @csrf
                  <input type="hidden" name="json_nilai" id="json_nilai">
                  <input type="submit" class="btn btn-danger" value="Validasi Nilai" id="input_submit_validasinilai">
                </form>
              </div>
            </div>
          @else
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">
                  Daftar Nilai Tervalidasi
                </h4>
                <p class="card-category">
                  Nilai sudah divalidasi, tidak dapat diubah lagi, validasi dilakukan pada: {{ $test->validated_at }}
                </p>

              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="table-nilai">
                      <table class="table">
                        <thead class=" text-primary">
                          <th>NIK</th>
                          <th>Nama</th>
                          <th>Nilai</th>
                        </thead>
                        <tbody>
                          {{-- foreach( App\TestStudent::where('test_id', Stest->id)->get() as Srow) --}}
                          @foreach( $test->students as $row)
                            <tr>
                              <td>{{ $row->id }}</td>
                              <td>{{ $row->nama }}</td>
                              <td>{{ $row->pivot->nilai }}</td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @endif
        @endif

      </div>

    </div>
  </div>

  <!-- MODALS -->
  <div class="modal fade" id="modal_help" tabindex="-1" role="dialog" aria-labelledby="modal_help_title" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_help_title">CSV File Help</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <h6>
            How to make CSV file from Excel
          </h6>
          <p>
            Excel 2003: click "File" > "Save As" > in file dialog, select ".CSV" or "Comma Separated File" > "Save"
          </p>
          <p>
            Excel 2007: click Office Button (top-left-most big circular button) > "Save As" > in file dialog, select ".CSV" or "Comma Separated File" > "Save"
          </p>
          <p>
            Excel 2016: click "File" > "Save As" > "Browse" > in file dialog, select ".CSV" or "Comma Separated File" > "Save"
          </p>
          <h6>
            Required CSV file format
          </h6>
          <p>
            First row must contain table header / label named "NIK" and "Nilai"
          </p>
          <p>
            Next rows must contain pair of NIK and Nilai value of your data. Example below.
          </p>
          <table class="table">
            <tr>
              <td>NIK</td><td>Nilai</td>
            </tr>
            <tr>
              <td>937234716572</td><td>93</td>
            </tr>
            <tr>
              <td>245712457833</td><td>70</td>
            </tr>
            <tr>
              <td>245767830244</td><td>40</td>
            </tr>
            <tr>
              <td>537357823053</td><td>100</td>
            </tr>
            <tr>
              <td>947520394956</td><td>60</td>
            </tr>
            <tr>
              <td>137466564726</td><td>81</td>
            </tr>
            <tr>
              <td>134442837422</td><td>91</td>
            </tr>
          </table>

          <h6>
            (Technical) Supported CSV format
          </h6>
          <p>
            Supported: standard csv (quote='"', separator=';', lineBreak='\n')
            <pre>"Data 1 1";"Data 1 2"\n"Data 2 1";"Data 2 2"</pre>
          </p>
          <p>
            Supported: ms excel default csv (quote=None, separator=',', lineBreak='\r\n')
            <pre>Data 1 1,Data 1 2,\r\nData 2 1,Data 2 2</pre>
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
  <script type="text/javascript">
    $( function() {
      var schools = @json($schoolsIdName) ;

      $( "#school_name" ).autocomplete({
        minLength: 3,
        source: schools,
        focus: function( event, ui ) {
          $( "#school_name" ).val( ui.item.label + " - NPSN " + ui.item.value );
          return false;
        },
        select: function( event, ui ) {
          $( "#school_name" ).val( ui.item.label + " - NPSN " + ui.item.value );
          $( "#school_id" ).val( ui.item.value );

          return false;
        }
      })
      .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
          .append( "<div>"  + item.label + " - NPSN " + item.value + "</div>" )
          .appendTo( ul );
      };
    } );
  </script>
  <script type="text/javascript" src="{{ asset('js/plugins/papaparse.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/plugins/table.js') }}"></script>
  <script type="text/javascript">
    $("#input_submit_validasinilai").css('display', 'none');
    $("#file_nilai_csv").change(function(){
      $(".table-nilai").html(''); //clear table-nilai preview
      var fileInput = document.getElementById("file_nilai_csv");
      Papa.parse(fileInput.files[0], {
        complete: function(results) {
          if(results.errors.length == 0)
          {
            eventHandler_csvParsingComplete(results);
          }
          else
          {
            // ACCOMODATE MICROSOFT EXCEL'S NON STANDARD CSV FORMAT (NOT HAVING QUOTE CHAR)
            Papa.parse(fileInput.files[0], {
              complete: function(results) {
                eventHandler_csvParsingComplete(results);
              },
              config: {
                //delimiter: ",",
                quoteChar: '',
              },
              error: function() {
                $("#input_submit_validasinilai").css('display', 'none');
              }
            });
          }
        },
        error: function() {
          //alert('error!!!!');
          $("#input_submit_validasinilai").css('display', 'none');
        }
      });
    });

    function eventHandler_csvParsingComplete(results)
    {

      var dataValid = true;

      // verify table header
      if(results.data[0][0] != "NIK" || results.data[0][1] != "Nilai")
      {
        dataValid = false;
        alert('ERROR: your table header is: "' + results.data[0][0] + '", "' + results.data[0][1] + '\r\n' + 'Please make it "NIK", "Nilai" ');
      }

      // verify number of column
      if(results.data[0].length != 2 || results.data[1].length != 2)
      {
        dataValid = false;
        alert('ERROR: the number of column of your file is ' + results.data[0].length + '\r\n' + 'Please make it 2');
      }

      // verify if first record of "Nilai" is numeric
      if( isNaN(results.data[1][1]) == true)
      {
        dataValid = false;
        alert('ERROR: your "Nilai" is not numeric, here it is: "' + results.data[1][1] + '"\r\n' + 'Plase make it numeric');
      }

      if(dataValid == true)
      {
        var data = {
          headerss: results.data[0],
          rowss: results.data.slice(1, results.data.length),
        };
        var table = new Table();
        table
          .setHeader(data.headerss)
          .setData(data.rowss)
          .setTableClass('table')
          .build('.table-nilai');
        $("#json_nilai").val(JSON.stringify(data.rowss));
        $("#input_submit_validasinilai").css('display', 'block');
      }
      else
      {
        $("#input_submit_validasinilai").css('display', 'none');
        $('#modal_help').modal();
      }
    }
  </script>
  <script type="text/javascript" src="{{ asset('js/plugins/jquery.datetimepicker.full.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/plugins/moment.js') }}"></script>
  <script type="text/javascript">
    jQuery('#input_datetimepicker_testtanggal').datetimepicker
    ({
      format: 'Y-m-d H:i:00',
      defaultTime: '09:00',
      inline: true,
      //theme: dark,
    });
    $("#input_datetimepicker_testtanggal").change(function(){
      var DATE_RFC2822 = "ddd, DD MMM YYYY HH:mm:ss ZZ";
      var datetimeValue = $("#input_datetimepicker_testtanggal").val();

      //alert($("#input_datetimepicker_testtanggal").val());
      $("#input_text_confirmtanggal").val(datetimeValue);
      $("#input_text_tanggal").val(datetimeValue);

      var momentKu = moment(datetimeValue);
      $("#input_text_readabletanggal").val(momentKu.format(DATE_RFC2822));

    });
  </script>
@endsection