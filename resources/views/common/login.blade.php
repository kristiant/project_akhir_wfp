@extends('common.template_login')


@section('title')
  User Login
@endsection

@section('searchBarVisibility')
  none
@endsection

@section('logoutTopNav')
@endsection

@section('text')
  Login to Hall of Academic Fame
@endsection

@section('formActionMethod')
  action="{{ route('login') }}" method="POST"
@endsection

@section('usernameHint')
  Enter your NIK here
@endsection

@section('buttonText')
  Login
@endsection

@section('registerLink')
  <a href="{{ route('register') }}" class="btn btn-secondary">Register</a>
@endsection