<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\School;
use App\Student;
use App\Subject;
use App\Test;
use App\MasterApprentice;
use Config;
use Auth;

class AdminPagesController extends Controller
{
    /*
    use AuthenticatesUsers;
    protected $redirectTo = '/'; //route('adminIndex');
    public function __construct()
    {
        $this->redirectTo = route('adminIndex');
        $this->middleware('guest')->except('logout');
    }
    public function authenticate(Request $request)
    {
        Auth::attempt(['email' => $request->username, 'password' => $request->password]);
        if ( Auth::check() )
        {
            //return redirect()->intended('dashboard');
            return redirect(route('adminIndex'));
        }
        Auth::attempt(['username' => $request->username, 'password' => $request->password]);
        if ( Auth::check() )
        {
            return redirect(route('adminIndex'));
        }
        Auth::attempt(['id' => $request->username, 'password' => $request->password]);
        if ( Auth::check() )
        {
            return redirect(route('adminIndex'));
        }

        return redirect(route('adminLogin'));
    }
    public function logout()
    {
        $this->middleware('auth');
        Auth::logout();
        return redirect(route('adminLogin'));
    }

    public function loginAdmin()
    {
        $viewArgs = Config::get('kristian.view_config_common'); // set common view arguments (page name, template, etc)
        $viewArgs["leftNavigations"] = [];
        $viewArgs["pageTitle"] = "Admin Login - Hall of Academic Fame"; // set page title displayed in browser's titlebar
        unset($viewArgs["searchArr"]); // do not enable search

        // login parameter
        $viewArgs["login"]["title"] = "Admin Login";
        $viewArgs["login"]["text"] = "Login to Hall of Academic Fame Administration Panel";
        $viewArgs["login"]["action"] = route('adminLoginProcess');
        $viewArgs["login"]["method"] = "POST";
        $viewArgs["login"]["usernameHint"] = "Enter your administrator username here";
        $viewArgs["login"]["buttonText"] = "Login";

        //dd($viewArgs); exit;

        return view('common_login', $viewArgs);
    }

    public function indexAdmin()
    {
        $viewArgs = Config::get('kristian.view_config_common'); // set common view arguments (page name, template, etc)
        $viewArgs["leftNavigations"] = Config::get('kristian.left_nav_admin'); // set sidebar
        $viewArgs["leftNavigations"]["schools"]["isActive"] = true; // set which sidebar is active
        $viewArgs["pageTitle"] = "Admin - Hall of Academic Fame"; // set page title displayed in browser's titlebar
        unset($viewArgs["searchArr"]); // do not enable search

        $viewArgs["topIconNavigations"][] = [
            "href" => route('adminLogout'),
            "text" => "Logout",
            "icon" => "exit_to_app",
            "isDropdown" => false,
        ];

        //additional info
        $viewArgs["leftNavigations"]["schools"]["numberOfRecords"] = School::all()->count();
        $viewArgs["leftNavigations"]["students"]["numberOfRecords"] = Student::all()->count();
        $viewArgs["leftNavigations"]["subjects"]["numberOfRecords"] = Subject::all()->count();
        $viewArgs["leftNavigations"]["tests"]["numberOfRecords"] = Test::all()->count();
        $viewArgs["leftNavigations"]["mas-app"]["numberOfRecords"] = MasterApprentice::all()->count();

        $viewArgs["leftNavigations"]["schools"]["lastUpdate"] = School::max('updated_at');//->updated_at;
        $viewArgs["leftNavigations"]["students"]["lastUpdate"] = Student::max('updated_at');//->updated_at;
        $viewArgs["leftNavigations"]["subjects"]["lastUpdate"] = Subject::max('updated_at');//->updated_at;
        $viewArgs["leftNavigations"]["tests"]["lastUpdate"] = Test::max('updated_at');//->updated_at;
        $viewArgs["leftNavigations"]["mas-app"]["lastUpdate"] = MasterApprentice::max('updated_at');//->updated_at;

        //dd($viewArgs); exit;

        return view('admin.index', $viewArgs);
    }
    */
}
