<?php

use Illuminate\Database\Seeder;
use App\Student;
use App\School;
use Faker\Factory as Faker;


class StudentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $arrFk1 = [];
        $fk1 = School::all();
        foreach ($fk1 as $key => $value)
        {
            $arrFk1[] = $value->id;
        }


        for ($i=1; $i < 301; $i++)
        {
            $item = new Student();
            $item->id = '99999999' . $i;
            $item->school_id = array_random($arrFk1);
            $item->nama = $faker->name; //str_random(10);
            $item->kelas = mt_rand(1,3);
            $item->glory = 0;
            $item->specialization = $faker->jobTitle; //"-";
            $item->x = (mt_rand(1110, 1130) / 10);
            $item->y = (mt_rand(-80, -60)) / 10;
            $item->apprentice_skill_up = 0;
            $item->save();
        }
    }
}
