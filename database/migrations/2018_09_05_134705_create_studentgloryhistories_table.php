<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentgloryhistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_gloryhistories', function (Blueprint $table) {
            $table->string('student_id');
            $table->date('tanggal');
            $table->string('glory');

            $table->foreign('student_id')->references('id')->on('students');
            $table->primary(['student_id', 'tanggal']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_gloryhistories');
    }
}
