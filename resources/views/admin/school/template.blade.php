@extends('admin.template')

@section('searchBarAction')
  {{ route('adminSchoolSearchRedirect') }}
@endsection

@section('searchBarVisibility')
  inline-block
@endsection

@section('scriptNav')
  <script type="text/javascript">
    $("#nav_item_schools").addClass("active");
  </script>
@endsection