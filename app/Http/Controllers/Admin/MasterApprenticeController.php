<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\MasterApprentice;
use App\Student;
use App\Subject;
use Config;
use App\Http\Requests\AdminMasAppFormRequest;

class MasterApprenticeController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth'); // tidak bisa mengakses controller ini jika belum login
        //sebaiknya jangan disini, di routes saja
        //atau mungkin lebih baik disini daripada di routes
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('adminMasAppIndexPaginated', ['page_number' => 1]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPaginated($page_number)
    {
        return $this->search(null, $page_number);
    }

    public function search($keyword, $page_number)
    {
        $query = null;
        if($keyword == null)
        {
            $query = MasterApprentice::all();
        }
        else
        {
            //$query = MasterApprentice::all();
            // /*
            $query = MasterApprentice::with('master', 'apprentice', 'subject')
                ->whereHas('master', function($query) use($keyword) {
                    //global $keyword;
                    $query->where('nama', 'like', '%' . $keyword . '%');
                })
                ->orWhereHas('apprentice', function($query) use($keyword) {
                    //global $keyword;
                    $query->where('nama', 'like', '%' . $keyword . '%');
                })
                ->orWhereHas('subject', function($query) use($keyword) {
                    //global $keyword;
                    $query->where('nama', 'like', '%' . $keyword . '%');
                })
                ->get();
            //*/
        }

        $masapps = $query->forPage($page_number, Config::get('kristian.pagination_max_item'));
        $viewArgs["masapps"] = $masapps;

        $viewArgs["page"]["current"] = $page_number;
        $viewArgs["page"]["max"] = ceil($query->count() / Config::get('kristian.pagination_max_item'));
        if($viewArgs["page"]["max"] == 0) $viewArgs["page"]["max"] = 1;

        if($keyword != null && isset($keyword)) $viewArgs["keyword"] = $keyword;


        return view('admin.masapp.list', $viewArgs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $masapp = new MasterApprentice();
        $masapp->master_student_id = -1;
        $masapp->apprentice_student_id = -1;
        $masapp->subject_id = -1;
        $masapp->started_at = date('Y-m-d H:i:s');
        $masapp->ended_at = null;

        $subjects = Subject::all();
        $studentsIdName = [];
        $students = Student::all();
        foreach ($students as $key => $value)
        {
            $arr = [];
            $arr["value"] = $value->id;
            $arr["label"] = $value->nama;

            $studentsIdName[] = $arr;
        }

        $viewArgs["masapp"] = $masapp;
        $viewArgs["subjects"] = $subjects;
        $viewArgs["studentsIdName"] = $studentsIdName;
        return view('admin.masapp.edit', $viewArgs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminMasAppFormRequest $request)
    {
        try
        {
            $masapp = new MasterApprentice();
            $masapp->master_student_id = $request->master_student_id;
            $masapp->apprentice_student_id = $request->apprentice_student_id;
            $masapp->subject_id = $request->subject_id;
            $masapp->started_at = $request->input_text_start;

            //handle checkbox is_ended
            if( isset($request->is_ended) && $request->is_ended == 1 )
            {
                $masapp->ended_at = $request->input_text_end;
            }
            else
            {
                $masapp->ended_at = null;
            }

            $masapp->save();
            //return json_encode(['status' => 'success']);
            return redirect(route('masapp.edit',
            [
                'master_student_id' => $masapp->master_student_id,
                'apprentice_student_id' => $masapp->apprentice_student_id,
                'subject_id' => $masapp->subject_id,
            ]));
        }
        catch (Exception $e)
        {
            return json_encode(['status' => 'failed', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($master_student_id, $apprentice_student_id, $subject_id)
    {
        $masapp = MasterApprentice::where('master_student_id', $master_student_id)
            ->where('apprentice_student_id', $apprentice_student_id)
            ->where('subject_id', $subject_id)
            ->get()
            ->first();
        $subjects = Subject::all();
        $studentsIdName = [];
        $students = Student::all();
        foreach ($students as $key => $value)
        {
            $arr = [];
            $arr["value"] = $value->id;
            $arr["label"] = $value->nama;

            $studentsIdName[] = $arr;
        }

        $viewArgs["masapp"] = $masapp;
        $viewArgs["subjects"] = $subjects;
        $viewArgs["studentsIdName"] = $studentsIdName;
        return view('admin.masapp.edit', $viewArgs);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $master_student_id, $apprentice_student_id, $subject_id)
    {
        try
        {
            $masapp = MasterApprentice::where('master_student_id', $master_student_id)
                ->where('apprentice_student_id', $apprentice_student_id)
                ->where('subject_id', $subject_id)
                ->get()
                ->first();
            $masapp->started_at = $request->input_text_start;

            //handle checkbox is_ended
            if( isset($request->is_ended) && $request->is_ended == 1 )
            {
                $masapp->ended_at = $request->input_text_end;
            }
            else
            {
                $masapp->ended_at = null;
            }

            $masapp->save();
            //return json_encode(['status' => 'success']);
            return redirect(route('masapp.edit', [
                'master_student_id' => $master_student_id,
                'apprentice_student_id' => $apprentice_student_id,
                'subject_id' => $subject_id,
            ]));
        }
        catch (Exception $e)
        {
            return json_encode(['status' => 'failed', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($master_student_id, $apprentice_student_id, $subject_id)
    {
        try
        {
            $masapp = MasterApprentice::where('master_student_id', $master_student_id)
                ->where('apprentice_student_id', $apprentice_student_id)
                ->where('subject_id', $subject_id)
                ->get()
                ->first();

            $masapp->delete();
            return json_encode(['status' => 'success']);
        }
        catch (Exception $e)
        {
            return json_encode(['status' => 'failed', 'message' => $e->getMessage()]);
        }
    }
}
