@extends('parent_template')
@section('content')

  <div class="content">
    <div class="container-fluid">

      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-danger">
            <h4 class="card-title">Battle Room</h4>
            <p class="card-category">ID: 293842</p>
          </div>
          <div class="card-body">
            <form>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Bet</label>
                    <input type="text" class="form-control" disabled value="1,000pt">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Winning Prize</label>
                    <input type="email" class="form-control" disabled value="200,000pt">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Mode</label>
                    <input type="text" class="form-control" disabled value="Battle Royale">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Arena</label>
                    <input type="text" class="form-control" disabled value="Math">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Room Password</label>
                    <input type="text" class="form-control" value="cjuvQghq4G63">
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Save Room Settings</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>

      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-warning">
            <h4 class="card-title">Players in Room</h4>
          </div>
          <div class="card-body table-responsive">
            <table class="table table-hover">
              <thead class="text-warning">
                <th>Global Rank</th>
                <th>Name</th>
                <th>Glory</th>
                <th>Class</th>
                <th>Action</th>
              </thead>
              <tbody>
                <tr>
                  <td>2</td>
                  <td>Kristian Tanuwijaya</td>
                  <td>144721</td>
                  <td>Data Scientist</td>
                  <td><a href="/student/1"><button class="btn btn-primary btn-round">View Profile</button></a></td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Simeon Yuda Prasetyo</td>
                  <td>124721</td>
                  <td>Tutor</td>
                  <td><a href="/student/1"><button class="btn btn-primary btn-round">View Profile</button></a></td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>Sundoro Iswara</td>
                  <td>124719</td>
                  <td>Gamer</td>
                  <td><a href="/student/1"><button class="btn btn-primary btn-round">View Profile</button></a></td>
                </tr>
                <tr>
                  <td>5</td>
                  <td>Justin Hadinata Chau</td>
                  <td>124718</td>
                  <td>Gamer</td>
                  <td><a href="/student/1"><button class="btn btn-primary btn-round">View Profile</button></a></td>
                </tr>
                <tr>
                  <td>6</td>
                  <td>Marcellino</td>
                  <td>124717</td>
                  <td>Student</td>
                  <td><a href="/student/1"><button class="btn btn-primary btn-round">View Profile</button></a></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
  </div>

@endsection