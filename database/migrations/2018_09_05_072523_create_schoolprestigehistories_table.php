<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolprestigehistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //if(!Schema::hasTable('school_prestigehistories'))
        //{
        Schema::create('school_prestigehistories', function (Blueprint $table) {
            $table->string('school_id');
            $table->date('tanggal');
            $table->string('prestige');

            $table->foreign('school_id')->references('id')->on('schools');
            $table->primary(['school_id', 'tanggal']);
        });
        //}

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_prestigehistories');
    }
}
