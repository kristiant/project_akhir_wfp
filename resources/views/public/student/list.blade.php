@extends('public.student.template')
@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">
                Student Global Ranking
              </h4>
              <p class="card-category">Students' glory are affected by test scores, master-apprentice mentoring system, and battles</p>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>#</th>
                    <th>Nama</th>
                    <th>Sekolah</th>
                    <th>Kelas</th>
                    <th>Glory</th>
                    <th>Specialization</th>
                    <th>Action</th>
                  </thead>
                  <tbody>
                    @foreach($students as $row)
                      <tr>
                        <td>{{ $row->getRank() }}</td>
                        <td>{{ $row->nama }}</td>
                        <td>{{ $row->school->nama }}</td>
                        <td>{{ $row->kelas }}</td>
                        <td>{{ round($row->glory) }}</td>
                        <td>{{ $row->specialization }}</td>
                        <td>
                          <a href="{{ route('publicStudentProfile', ['id' => $row->id]) }}"><button class="btn-small btn-primary">Profile</button></a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              @include('common.addon_pagination')
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection