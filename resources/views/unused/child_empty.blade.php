@extends('parent_template')
@section('content')
  <div class="content">
    <div class="container-fluid">
      {!! $plainContent !!}

      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-warning">
              <h4 class="card-title ">Potential Apprentice(s) Near You</h4>
              <p class="card-category">Discover new potential apprentice in your location</p>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-warning">
                    <th>
                      Name
                    </th>
                    <th>
                      Need Help In
                    </th>
                    <th>
                      Glory
                    </th>
                    <th>
                      Action
                    </th>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        Dakota Rice
                      </td>
                      <td>
                        Math
                      </td>
                      <td class="text-primary">
                        $36,738
                      </td>
                      <td>
                        <button class="btn btn-warning">Profile</button>
                        <button class="btn btn-warning">Map</button>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Minerva Hooper
                      </td>
                      <td>
                        Physics
                      </td>
                      <td class="text-primary">
                        $23,789
                      </td>
                      <td>
                        <button class="btn btn-warning">Profile</button>
                        <button class="btn btn-warning">Map</button>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Sage Rodriguez
                      </td>
                      <td>
                        Physics
                      </td>
                      <td class="text-primary">
                        $56,142
                      </td>
                      <td>
                        <button class="btn btn-warning">Profile</button>
                        <button class="btn btn-warning">Map</button>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Philip Chaney
                      </td>
                      <td>
                        Math
                      </td>
                      <td class="text-primary">
                        $38,735
                      </td>
                      <td>
                        <button class="btn btn-warning">Profile</button>
                        <button class="btn btn-warning">Map</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-success">
              <h4 class="card-title ">Recommended Master(s)</h4>
              <p class="card-category">Discover a master near your location to help you grow</p>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-warning">
                    <th>
                      Name
                    </th>
                    <th>
                      Specialization
                    </th>
                    <th>
                      Glory
                    </th>
                    <th>
                      Action
                    </th>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        Dakota Rice
                      </td>
                      <td>
                        Math
                      </td>
                      <td class="text-primary">
                        $36,738
                      </td>
                      <td>
                        <button class="btn btn-success">Profile</button>
                        <button class="btn btn-success">Map</button>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Minerva Hooper
                      </td>
                      <td>
                        Physics
                      </td>
                      <td class="text-primary">
                        $23,789
                      </td>
                      <td>
                        <button class="btn btn-success">Profile</button>
                        <button class="btn btn-success">Map</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>



    </div>
  </div>


@endsection