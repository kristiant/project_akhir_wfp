@extends('public.school.template')
@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">
                Schools Global Ranking
              </h4>
              <p class="card-category">Schools' prestige are affected by average of its students, and will be updated once in a week</p>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>#</th>
                    <!-- th>DEBUG</th -->
                    <th>Nama</th>
                    <th>Prestige</th>
                    <th>Level</th>
                    <th>Action</th>
                  </thead>
                  <tbody>
                    @foreach($schools as $row)
                      <tr>
                        <td>{{ $row->getRank() }}</td>
                        <!-- td>{{-- $row->recountPrestige() --}}</td -->
                        <td>{{ $row->nama }}</td>
                        <td>{{ round($row->prestige) }}</td>
                        <td>{{ $row->level }}</td>
                        <td>
                          <a href="{{ route('publicSchoolProfile', ['id' => $row->id]) }}"><button class="btn-small btn-primary">Profile</button></a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              @include('common.addon_pagination')
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection