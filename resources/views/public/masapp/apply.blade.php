@extends('public.masapp.template')

@section('content')
  <div class="content">
    <div class="container-fluid">

      {{--
      @if($errors->has('error'))
        {{ $errors->first('error') }}
      @endif
      --}}

      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              @if($mas_or_app == 'master')
                Apply to a Master
              @elseif($mas_or_app == 'apprentice')
                Recruit an Apprentice
              @endif
            </h4>
            <p class="card-category">
              {{ $mas_or_app }} appliance form
            </p>
          </div>
          <div class="card-body">
            <form action="{{ route('memberMasappApplyProcess') }}" method="POST" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="proposal_type" value="{{ $mas_or_app }}">

              <div class="row">
                <div class="col-md-12">
                  @if($errors->has('error'))
                    <span class="label-error">{{ $errors->first('error') }}</span>
                  @endif

                  @if($mas_or_app == 'master')
                    <p>Greetings, friend. </p>
                    <p>I have noticed your talent and dedication in specified subject. </p>
                    <p>With this proposal, I wish to seek your guidance and therefore apply as your apprentice. </p>
                  @elseif($mas_or_app == 'apprentice')
                    <p>Greetings, friend. </p>
                    <p>I have noticed that you have a good potential. </p>
                    <p>With this proposal, I wish to name you as my apprentice in specified subject. </p>
                    <p>Let's learn under my guidance and reach a new height. </p>
                  @endif
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">{{ $mas_or_app }}</label>
                    <input type="text" class="form-control" name="target_student_nama" id="target_student_nama"
                    @if($targetStudent != null)
                      value="{{ $targetStudent->nama }} - NIK {{ $targetStudent->id }}" disabled
                    @else
                      value="Type student name or NIK here ..."
                    @endif
                    >
                    <input type="hidden" name="target_student_id" id="target_student_id"
                    @if($targetStudent != null)
                      value="{{ $targetStudent->id }}"
                    @else
                      value="-1"
                    @endif
                    >
                    @if($errors->has('target_student_id'))
                      <span class="label-error">{{ $errors->first('target_student_id') }}</span>
                    @endif
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Subject</label>
                    <select name="subject_id" class="form-control">
                      @foreach($subjects as $item)
                        <option value="{{ $item->id }}"
                          @if($subject != null && $item->id == $subject->id)
                            selected
                          @endif
                        >
                          {{ $item->nama }}
                        </option>
                      @endforeach
                    </select>
                    @if($errors->has('subject_id'))
                      <span class="label-error">{{ $errors->first('subject_id') }}</span>
                    @endif

                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Length (month)</label>
                    <input type="text" class="form-control form-input-number" name="length_in_month" value="1">
                    @if($errors->has('length_in_month'))
                      <span class="label-error">{{ $errors->first('length_in_month') }}</span>
                    @endif

                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <p>Stamped, </p>
                  <p>
                  @if($mas_or_app == 'master')
                    Your future apprentice,
                  @elseif($mas_or_app == 'apprentice')
                    Your future master,
                  @endif
                    {{ Auth::user()->student->nama }}
                  </p>
                </div>
              </div>

              <button type="submit" class="btn btn-primary pull-right">Apply</button>
              <a href="{{ route('memberMasappLanding') }}"><button type="button" class="btn btn-secondary pull-right">Cancel</button></a>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>

    </div>
  </div>
@endsection


@section('scripts')
  <script type="text/javascript">
    var students = @json($studentsIdName) ;
    $( function() {

      $( "#target_student_nama" ).autocomplete({
        minLength: 3,
        source: students,
        focus: function( event, ui ) {
          $( "#target_student_nama" ).val( ui.item.label + " - NIK " + ui.item.value );
          return false;
        },
        select: function( event, ui ) {
          $( "#target_student_nama" ).val( ui.item.label + " - NIK " + ui.item.value );
          $( "#target_student_id" ).val( ui.item.value );

          return false;
        }
      })
      .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
          .append( "<div>"  + item.label + " - NIK " + item.value + "</div>" )
          .appendTo( ul );
      };
    } );
  </script>

@endsection