@extends('admin.student.template')
@section('content')

  <div class="content">
    <div class="container-fluid">

      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              @if( $student->id != -1 )
                Edit Student
              @else
                Create New Student
              @endif
            </h4>
            <p class="card-category">
              @if( $student->id != -1 )
                NIK: {{ $student->id }}
              @endif
            </p>
          </div>
          <div class="card-body">
            @if( $student->id != -1 )
              <form action="{{ route('students.update', ['id' => $student->id]) }}" method="POST" enctype="multipart/form-data">
            @else
              <form action="{{ route('students.store') }}" method="POST" enctype="multipart/form-data">
            @endif

              @if( $student->id != -1 )
                @method('PUT')
              @else
                @method('POST')
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group label-floating
                    @if($errors->has('id'))
                      has-error
                    @endif">
                      <label class="bmd-label-floating">NIK</label>
                      <input type="text" class="form-control form-input-number" name="id" value="">
                      @if($errors->has('id'))
                        <span class="label-error">{{ $errors->first('id') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
              @endif
              @csrf
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group label-floating
                    @if($errors->has('nama'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">Nama</label>
                    <input type="text" class="form-control" name="nama" value="{{ $student->nama }}">
                    @if($errors->has('nama'))
                      <span class="label-error">{{ $errors->first('nama') }}</span>
                    @endif
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group label-floating
                    @if($errors->has('kelas'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">Kelas</label>
                    <input type="text" class="form-control" name="kelas" value="{{ $student->kelas }}">
                    <!-- PERLU BUATKAN SUATU PICKER -->
                    @if($errors->has('kelas'))
                      <span class="label-error">{{ $errors->first('kelas') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group label-floating
                    @if($errors->has('glory'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">Glory</label>
                    <input type="text" class="form-control form-input-number" name="glory" value="{{ round($student->glory) }}">
                    @if($errors->has('glory'))
                      <span class="label-error">{{ $errors->first('glory') }}</span>
                    @endif
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group label-floating
                    @if($errors->has('specialization'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">Specialization</label>
                    <input type="text" class="form-control" name="specialization" value="{{ $student->specialization }}">
                    @if($errors->has('specialization'))
                      <span class="label-error">{{ $errors->first('specialization') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group label-floating
                    @if($errors->has('school_id'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">School</label>
                    <input type="text" class="form-control" name="school_name" id="school_name"
                    @if($student->school_id == -1)
                      value="Type school name or NPSN here ..."
                    @else
                      value="{{ $student->school->nama }} - NPSN {{ $student->school_id }}"
                    @endif
                    >
                    <input type="hidden" name="school_id" id="school_id" value="{{ $student->school_id }}">
                    @if($errors->has('school_id'))
                      <span class="label-error">{{ $errors->first('school_id') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group label-floating
                    @if($errors->has('x'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">Posisi (X)</label>
                    <input type="text" class="form-control form-input-number" name="x" value="{{ $student->x }}">
                    @if($errors->has('x'))
                      <span class="label-error">{{ $errors->first('x') }}</span>
                    @endif
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group label-floating
                    @if($errors->has('y'))
                      has-error
                    @endif">
                    <label class="bmd-label-floating">Posisi (Y)</label>
                    <input type="text" class="form-control form-input-number" name="y" value="{{ $student->y }}">
                    @if($errors->has('y'))
                      <span class="label-error">{{ $errors->first('y') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <label class="bmd-label-floating">Upload Picture: </label>
                  <input type="file" name="picture" class="btn btn-primary" id="form_input_picture">
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <label class="bmd-label-floating">Foto Lama</label> <br>
                  <img src="{{ $student->getImageAsset() }}" id="form_picture_preview_old" style="width: 100%; height: auto" />
                </div>
                <div class="col-md-6">
                  <label class="bmd-label-floating">Foto Baru</label> <br>
                  <img id="form_picture_preview_new" src="{{ asset('img/no-image.png') }}" style="width: 100%; height: auto" />
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Save</button>
              <a href="{{ route('students.index') }}"><button type="button" class="btn btn-secondary pull-right">Cancel</button></a>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>

    </div>
  </div>

@endsection

@section('scripts')
  <script type="text/javascript">
    $('#form_input_picture').change(function()
      {
        if (this.files && this.files[0])
        {
          var reader = new FileReader();
          reader.onload = function(e)
          {
              document.getElementById('form_picture_preview_new').src = e.target.result;
          }

          reader.readAsDataURL(this.files[0]);
        }
        else
        {
          document.getElementById('form_picture_preview_new').src='{{ asset('img/no-image.png') }}';
        }
      });
  </script>
  <script type="text/javascript">
    $( function() {
      var projects = [
        {
          value: "jquery",
          label: "jQuery"
        },
        {
          value: "jquery-ui",
          label: "jQuery UI"
        },
        {
          value: "sizzlejs",
          label: "Sizzle JS"
        }
      ];
      var schools = @json($schoolsIdName) ;

      $( "#school_name" ).autocomplete({
        minLength: 3,
        source: schools,
        focus: function( event, ui ) {
          $( "#school_name" ).val( ui.item.label + " - NPSN " + ui.item.value );
          //$( "#school_id" ).val( ui.item.value );
          return false;
        },
        select: function( event, ui ) {
          $( "#school_name" ).val( ui.item.label + " - NPSN " + ui.item.value );
          $( "#school_id" ).val( ui.item.value );

          return false;
        }
      })
      .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
          .append( "<div>"  + item.label + " - NPSN " + item.value + "</div>" )
          .appendTo( ul );
      };
    } );
  </script>
@endsection