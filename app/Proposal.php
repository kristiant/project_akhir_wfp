<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Exception;
use DateTime;

class Proposal extends Model
{
    protected $table = 'proposals';
    public $timestamps = true;
    public $incrementing = true;

    // relationships
    public function sender()
    {
        return $this->belongsTo('App\Student', 'sender_student_id');
    }

    public function receiver()
    {
        return $this->belongsTo('App\Student', 'receiver_student_id');
    }

    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }

    // other methods
    public function isCanBeDeleted()
    {
        return true;
    }

    public function isValidForApplication()
    {
        $masterId = -1;
        $apprenticeId = -1;
        if($this->proposal_type == 'master')
        {
            $masterId = $this->receiver->id;
            $apprenticeId = $this->sender->id;
        }
        else if($this->proposal_type == 'apprentice')
        {
            $masterId = $this->sender->id;
            $apprenticeId = $this->receiver->id;
        }
        $masapp = MasterApprentice::where('master_student_id', $masterId)
            ->where('apprentice_student_id', $apprenticeId)
            ->where('subject_id', $this->subject->id)
            ->get()
            ->first();

        if($masapp != null)
        {
            if($masapp->isActive())
            {
                throw new Exception("Cannot make new proposal of an already established master-apprentice relationship", 1);
            }
            else
            {
                $masapp->delete();
            }
        }

        $masapp = MasterApprentice::where('master_student_id', $apprenticeId)
            ->where('apprentice_student_id', $masterId)
            ->where('subject_id', $this->subject->id)
            ->get()
            ->first();

        if($masapp != null)
        {
            throw new Exception("Cannot make reversed master-apprentice relationship", 1);
        }

        if($masterId == $apprenticeId)
        {
            throw new Exception("Cannot apply to yourself", 1);
        }

        return true;

    }
}
