<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterApprenticeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_apprentice', function (Blueprint $table) {
            $table->string('master_student_id');
            $table->string('apprentice_student_id');
            $table->integer('subject_id')->unsigned();
            $table->dateTime('started_at');
            $table->dateTime('ended_at')->nullable();
            $table->timestamps();

            $table->foreign('master_student_id')->references('id')->on('students');
            $table->foreign('apprentice_student_id')->references('id')->on('students');
            $table->foreign('subject_id')->references('id')->on('subjects');
            $table->primary(['master_student_id', 'apprentice_student_id', 'subject_id'], 'master_apprentice_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_apprentice');
    }
}
