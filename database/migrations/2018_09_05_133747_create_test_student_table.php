<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_student', function (Blueprint $table) {
            $table->integer('test_id')->unsigned();
            $table->string('student_id');
            $table->integer('nilai');

            $table->foreign('test_id')->references('id')->on('tests');
            $table->foreign('student_id')->references('id')->on('students');
            $table->primary(['test_id', 'student_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_student');
    }
}
