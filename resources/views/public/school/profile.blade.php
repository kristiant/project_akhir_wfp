@extends('public.school.template')

@section('head')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/ol.css') }}">
  <style type="text/css">
    .map, #map {
      height: 200px;
      width: 100%;
    }
    #map_marker
    {
      background-color: rgba(230, 10, 10, 0.5);
      width: 30px;
      height: 30px;
      border-radius: 15px;
    }
    img.olTileImage {
      max-width: none;
    }
  </style>
@endsection

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-8">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">
                  @if($school->jenjang == "SMA")
                    High
                  @elseif($school->jenjang == "SMP")
                    Middle
                  @elseif($school->jenjang == "SD")
                    Primary
                  @else
                    Other
                  @endif
                  School Profile ({{ $school->jenjang }})
                </h4>
              </div>

              <!-- div style="width: 100%; height: auto;">
                <canvas id="chart_line_prestigehistory"></canvas>
              </div -->

            </div>
          </div>
          <!-- div class="col-md-12">
            <div class="card card-chart">
              <div class="card-header card-header-success">
                <div class="ct-chart" id="dailySalesChart"></div>
              </div>
              <div class="card-body">
                <h4 class="card-title">Prestige</h4>
                <p class="card-category">
                  <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in this month's prestige</p>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons">access_time</i> updated 3 days ago
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="card card-chart">
              <div class="card-header card-header-warning">
                <div class="ct-chart" id="websiteViewsChart"></div>
              </div>
              <div class="card-body">
                <h4 class="card-title">Battles Won By Students</h4>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons">access_time</i> last challenge sent 2 days ago
                </div>
              </div>
            </div>
          </div -->


          <!-- div class="col-md-12">
            <div style="width: 100%; height: auto;">
              <canvas id="chart_line_prestigehistory"></canvas>
            </div>
          </div -->

          <div class="col-md-12">
            <div class="card card-chart">
              <div class="card-header card-header-success">
                <!-- div class="ct-chart" id="websiteViewsChart"></div -->
                <canvas id="chart_line_prestigehistory"></canvas>
              </div>
              <div class="card-body">
                <h4 class="card-title">School Prestige History</h4>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons">access_time</i> last log at
                  {{ App\SchoolHistoryPrestige::where('school_id', $school->id)->max('tanggal') }}
                </div>
              </div>
            </div>
          </div>



          <div class="col-lg-12 col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">Top Students Stats</h4>
                <p class="card-category">Students with highest glory on {{ date('r') }}</p>
              </div>
              <div class="card-body table-responsive">
                <table class="table table-hover">
                  <thead class="text-warning">
                    <th>Global #</th>
                    <th>Name</th>
                    <th>Glory</th>
                    <th>Specialization</th>
                    <th>Action</th>
                  </thead>
                  <tbody>
                    @foreach($topStudents as $row)
                      <tr>
                        <td>{{ $row->getRank() }}</td>
                        <td>{{ $row->nama }}</td>
                        <td>{{ round($row->glory) }}</td>
                        <td>{{ $row->specialization }}</td>
                        <td><a href="{{ route('publicStudentProfile', ['id' => $row->id]) }}"><button class="btn-primary btn-small">Profile</button></a></td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>



        </div>

        <div class="col-md-4">
          <div class="card card-profile">
            <div class="card-avatar">
              <a href="#">
                <img class="img" src="{{ $school->getImageAsset() }}" style="width: 90%; height: auto;" /> {{-- asset('img/schools/ubaya.png') --}}
              </a>
            </div>
            <div class="card-body">
              <h4 class="card-title">{{ $school->nama }}</h4>
              <h6 class="card-category">Prestige: {{ round($school->prestige) }} ({{ $school->getRank() }} of {{ App\School::count() }})</h6>
              <h4 class="card-title">Top {{ ceil(($school->getRank())/(App\School::count())*100)/100 }}%</h4>
              <h4 class="card-title">
                @for($i = 0; $i < 10; $i++)
                  @if($i < $school->level)
                    <i class="material-icons">star</i>
                  @else
                    <i class="material-icons">star_border</i>
                  @endif
                @endfor
              </h4>
              <p class="card-description">{{ $school->alamat }}</p>
            </div>
          </div>
        </div>
      </div>



      <div class="row">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">Location</h4>
            <p class="card-category">Lat: {{ $school->y }}, Lon: {{ $school->x }}</p>
          </div>
          <div class="card-body">
            <div class="col-md-12">
              <div id="map_marker"></div>
              <div id="map" class="map"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="{{ asset('js/plugins/chartist.min.js') }}"></script>
  <script src="{{ asset('js/plugins/Chart.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/plugins/ol.js') }}"></script>
  <script type="text/javascript">
    //contoh koordinat:
    //-7.3251668,112.7762085 rumahku //-7.3201795,112.7676362 ubaya
    var lat = {{ $school->y }};
    var lng = {{ $school->x }};
    var center = ol.proj.transform([lng, lat], 'EPSG:4326', 'EPSG:3857'); //set coordinate

    //set overlay (map marker)
    var overlay = new ol.Overlay({
      position: center,
      element: document.getElementById('map_marker'),
      positioning: 'center-center',
    });


    //set map
    var map = new ol.Map({
      target: 'map',
      layers: [new ol.layer.Tile({
        source: new ol.source.OSM()
      })],
      overlays: [overlay],
      view: new ol.View({
        center: center,
        zoom: 16 // 9 kelihatan pulau jawa, 16 kelihatan perkampungan sekitar, 20 kelihatan rumah2 tetangga
      })
    });
  </script>
  <script type="text/javascript">
    Chart.defaults.global.defaultFontColor = "#000000";
  </script>
  <script type="text/javascript">
    var ctx1 = document.getElementById('chart_line_prestigehistory').getContext('2d');

    var radarChart1 = new Chart(ctx1, {
      type: 'line',
      data: @json( $school->getChartLinePrestigeHistoryData() ),
      options: {
        scales: {
          yAxes: [{
            ticks: {
              //beginAtZero: true
            }
          }]
        },
        bezierCurve : false,
        scaleFontColor: "#000000"
      }
    });
  </script>

@endsection