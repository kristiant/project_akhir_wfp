@extends('common.template_parent')

@section('pageTitle')
  Hall of Academic Fame
@endsection

@section('sidebar')
  <div class="sidebar" data-color="azure" data-background-color="black" data-image="{{ asset('img/sidebar-2.jpg') }}">
    <div class="logo">
      <a href="{{ route('home') }}" class="simple-text logo-normal">
        Hall of Academic Fame
      </a>
    </div>
    <div class="sidebar-wrapper">
      <ul class="nav">
        @yield('sidebarNavItems')
      </ul>
    </div>
  </div>
@endsection

@section('pageHeader')
  <a class="navbar-brand" href="{{ route('home') }}">Surabaya System</a>
@endsection

@section('footer')
  <a href="{{ route('home') }}">
    Let your potential be unleashed!
  </a>
@endsection

@section('copyright')
  Beta Software Production Inc.
  &copy;
  <script>
    document.write(new Date().getFullYear())
  </script>
@endsection

@section('logoutTopNav')
  @if(Auth::user() != null)
    <li class="nav-item">
      <a class="nav-link" href="#" onclick="document.getElementById('logout-form').submit();">
        <i class="material-icons">power_settings_new</i>
        <p class="d-lg-none d-md-block">
          Logout
        </p>
      </a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;"> {{ csrf_field() }} </form>
    </li>
  @else
    <li class="nav-item">
      <a class="nav-link" href="{{ route('login') }}">
        <i class="material-icons">input</i>
        <p class="d-lg-none d-md-block">
          Login
        </p>
      </a>
    </li>
  @endif
@endsection



{{-- 
// YANG PERLU DI OVERRIDE DI CHILD-CHILD NYA PAGE INI:

section('searchBarVisibility')
  block
endsection
 // can be "block" or "none"

section('searchBarAction')
endsection
 // fill it with url from route() method

section('searchBarValue')
endsection
 // fill it with string (old search query)

section('topIconNavItems') // (BISA DIISI MULTIPLE)

 // contoh isi:
 // 1. contoh jika biasa
  <li class="nav-item">
    <a class="nav-link" href="#">
      <i class="material-icons">people</i> // ini google material icon
      <p class="d-lg-none d-md-block">
        Friends
      </p>
    </a>
  </li>

  // 2. contoh jika ada dropdown-nya
  <li class="nav-item">
    <a class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="material-icons">people</i> // ini google material icon
      <span class="notification">3</span> // ini number (sebaiknya diisi jumlah item dropdownnya, misal jumlah notifikasi yang dimiliki)
      <p class="d-lg-none d-md-block">
        Friend Requests
      </p>
    </a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
      <a class="dropdown-item" href="/confirm/1">Kristian has added you as friend</a>
      <a class="dropdown-item" href="/confirm/2">Chris has added you as friend</a>
      <a class="dropdown-item" href="/confirm/302">Other has added you as friend</a>
    </div>
  </li>

endsection

section('sidebarNavItems')
  <li class="nav-item"> // jika active bisa ditambahkan di classnya (jadi: class="nav-item active")
    <a class="nav-link" href="3">
      <i class="material-icons">people</i>
      <p>Friends</p>
    </a>
  </li>
endsection

section('scriptNav')
  <script type="text/javascript">
    @if( isset($active) )
      $("#nav_item_schools").addClass("active");
    @endif
  </script>
endsection

--}}