@extends('admin.template')

@section('searchBarAction')
  {{ route('adminStudentSearchRedirect') }}
@endsection

@section('searchBarVisibility')
  inline-block
@endsection

@section('scriptNav')
  <script type="text/javascript">
    $("#nav_item_students").addClass("active");
  </script>
@endsection