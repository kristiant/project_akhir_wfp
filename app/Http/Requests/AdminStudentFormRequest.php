<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminStudentFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "school_id" => "required|not_in:-1",
            "nama" => "required",
            "kelas" => "required|numeric",
            "glory" => "required|numeric",
            "specialization" => "required",
            "x" => "required|numeric",
            "y" => "required|numeric",
        ];
    }
}
