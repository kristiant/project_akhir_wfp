@extends('admin.masapp.template')

@section('head')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.datetimepicker.min.css') }}">
@endsection

@section('content')

  <div class="content">
    <div class="container-fluid">

      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              @if( $masapp->master_student_id != -1 || $masapp->master != null )
                Edit Master Apprentice Relationship
              @else
                Create Master Apprentice Relationship
              @endif
            </h4>
            <p class="card-category">
              Master-Apprentice relationship is a student peer mentoring relationship. Student who rank higher may get more points by mentoring other students and get bonus points when the mentored student's average score increased.
            </p>
          </div>
          <div class="card-body">
            @if( $masapp->master_student_id != -1 || $masapp->master != null )
              <form action="{{ route('masapp.update', [
                'master_student_id' => $masapp->master->id,
                'apprentice_student_id' => $masapp->apprentice->id,
                'subject_id' => $masapp->subject->id,
              ]) }}" method="POST" enctype="multipart/form-data">
            @else
              <form action="{{ route('masapp.store') }}" method="POST" enctype="multipart/form-data">
            @endif

              @if( $masapp->master_student_id != -1 || $masapp->master != null )
                @method('PUT')
              @else
                @method('POST')
              @endif
              @csrf
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group label-floating
                  @if($errors->has('master_student_id'))
                    has-error
                  @endif
                  ">
                    <label class="bmd-label-floating">Master</label>
                    <input type="text" class="form-control" name="master_student_nama" id="master_student_nama"
                    @if($masapp->master_student_id != -1 || $masapp->master != null)
                      value="{{ $masapp->master->nama }} - NIK {{ $masapp->master->id }}" disabled
                    @else
                      value="Type student name or NIK here ..."
                    @endif
                    >
                    <input type="hidden" name="master_student_id" id="master_student_id"
                    @if($masapp->master_student_id != -1 || $masapp->master != null)
                      value="{{ $masapp->master->id }}"
                    @else
                      value="-1"
                    @endif
                    >
                    <!--span class="label-error">Error!!!</span-->
                    @if($errors->has('master_student_id'))
                      <span class="label-error">{{ $errors->first('master_student_id') }}</span>
                    @endif
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group label-floating
                  @if($errors->has('apprentice_student_id'))
                    has-error
                  @endif
                  ">
                    <label class="bmd-label-floating">Apprentice</label>
                    <input type="text" class="form-control" name="apprentice_student_nama" id="apprentice_student_nama"
                    @if($masapp->master_student_id != -1 || $masapp->master != null)
                      value="{{ $masapp->apprentice->nama }} - NIK {{ $masapp->apprentice->id }}" disabled
                    @else
                      value="Type student name or NIK here ..."
                    @endif
                    >
                    <input type="hidden" name="apprentice_student_id" id="apprentice_student_id"
                    @if($masapp->master_student_id != -1 || $masapp->master != null)
                      value="{{ $masapp->apprentice->id }}"
                    @else
                      value="-1"
                    @endif
                    >
                    @if($errors->has('apprentice_student_id'))
                      <span class="label-error">{{ $errors->first('apprentice_student_id') }}</span>
                    @endif
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group label-floating
                  @if($errors->has('subject_id'))
                    has-error
                  @endif
                  ">
                    <label class="bmd-label-floating">Subject</label>
                    <select name="subject_id" class="form-control"
                    @if( $masapp->master_student_id != -1 || $masapp->master != null )
                      disabled
                    @endif
                    >
                      @foreach($subjects as $subject)
                        <option value="{{ $subject->id }}"
                          @if($masapp->subject_id == $subject->id)
                            selected
                          @endif
                        >
                          {{ $subject->nama }}
                        </option>
                      @endforeach
                    </select> <br>

                    @if($errors->has('subject_id'))
                      <span class="label-error">{{ $errors->first('subject_id') }}</span>
                    @endif
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-check">
                    <label class="form-check-label">
                      <input class="form-check-input" id="is_ended" type="checkbox" value="1" name="is_ended"
                      @if($masapp->ended_at != null)
                        checked
                      @endif
                      >
                      sudah selesai
                      <span class="form-check-sign">
                        <span class="check"></span>
                      </span>
                    </label>
                  </div>
                </div>
              </div>

              {{-- mulai tanggal --}}

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group label-floating
                  @if($errors->has('input_text_start'))
                    has-error
                  @endif
                  ">
                    <label class="bmd-label-floating">Mulai</label>
                    <input type="text" class="form-control" id="input_datetimepicker_start" name="input_datetimepicker_start" value="{{ $masapp->started_at }}">
                    <input type="text" class="form-control disabled" disabled id="input_text_confirm_start" name="input_text_confirm_start" value="{{ $masapp->started_at }}">
                    <input type="hidden" id="input_text_start" name="input_text_start" value="{{ $masapp->started_at }}">
                    <input type="text" class="form-control disabled" disabled id="input_text_start_readable" name="input_text_start_readable" value="{{ (new DateTime($masapp->started_at))->format('r') }}">
                    @if($errors->has('input_text_start'))
                      <span class="label-error">{{ $errors->first('input_text_start') }}</span>
                    @endif
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group" id="container_datetimepicker_end">
                    <label class="bmd-label-floating">Akhir</label>
                    <input type="text" class="form-control" id="input_datetimepicker_end" name="input_datetimepicker_end"
                    value="@if($masapp->ended_at == null){{ date('Y:m:d H:i:s') }}@else{{ $masapp->ended_at }}@endif
                    ">
                    <input type="text" class="form-control disabled" disabled id="input_text_confirm_end" name="input_text_confirm_end"
                    value="@if($masapp->ended_at == null){{ date('Y:m:d H:i:s') }}@else{{ $masapp->ended_at }}@endif
                    ">
                    <input type="hidden" id="input_text_end" name="input_text_end" value="{{ $masapp->ended_at }}">
                    <input type="text" class="form-control disabled" disabled id="input_text_end_readable" name="input_text_end_readable"
                    value="@if($masapp->ended_at == null){{ date('r') }}@else{{ (new DateTime($masapp->ended_at))->format('r') }}@endif
                    ">
                  </div>
                </div>
              </div>

              {{-- akhir tanggal --}}


              <button type="submit" class="btn btn-primary pull-right">Save</button>
              <a href="{{ route('masapp.index') }}"><button type="button" class="btn btn-secondary pull-right">Cancel</button></a>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>

      </div>

    </div>
  </div>


@endsection

@section('scripts')
  <script type="text/javascript">
    var students = @json($studentsIdName) ;
    $( function() {

      $( "#master_student_nama" ).autocomplete({
        minLength: 3,
        source: students,
        focus: function( event, ui ) {
          $( "#master_student_nama" ).val( ui.item.label + " - NIK " + ui.item.value );
          return false;
        },
        select: function( event, ui ) {
          $( "#master_student_nama" ).val( ui.item.label + " - NIK " + ui.item.value );
          $( "#master_student_id" ).val( ui.item.value );

          return false;
        }
      })
      .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
          .append( "<div>"  + item.label + " - NIK " + item.value + "</div>" )
          .appendTo( ul );
      };
    } );
    $( function() {

      $( "#apprentice_student_nama" ).autocomplete({
        minLength: 3,
        source: students,
        focus: function( event, ui ) {
          $( "#apprentice_student_nama" ).val( ui.item.label + " - NIK " + ui.item.value );
          return false;
        },
        select: function( event, ui ) {
          $( "#apprentice_student_nama" ).val( ui.item.label + " - NIK " + ui.item.value );
          $( "#apprentice_student_id" ).val( ui.item.value );

          return false;
        }
      })
      .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
          .append( "<div>"  + item.label + " - NIK " + item.value + "</div>" )
          .appendTo( ul );
      };
    } );
  </script>
  <script type="text/javascript" src="{{ asset('js/plugins/jquery.datetimepicker.full.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/plugins/moment.js') }}"></script>
  <script type="text/javascript">
    jQuery('#input_datetimepicker_start').datetimepicker
    ({
      format: 'Y-m-d H:i:00',
      defaultTime: '09:00',
      inline: true,
      //theme: dark,
    });
    $("#input_datetimepicker_start").change(function(){
      var DATE_RFC2822 = "ddd, DD MMM YYYY HH:mm:ss ZZ";
      var datetimeValue = $("#input_datetimepicker_start").val();

      $("#input_text_confirm_start").val(datetimeValue);
      $("#input_text_start").val(datetimeValue);

      var momentKu = moment(datetimeValue);
      $("#input_text_start_readable").val(momentKu.format(DATE_RFC2822));

    });
  </script>
  <script type="text/javascript">
    jQuery('#input_datetimepicker_end').datetimepicker
    ({
      format: 'Y-m-d H:i:00',
      defaultTime: '09:00',
      inline: true,
      //theme: dark,
    });
    $("#input_datetimepicker_end").change(function(){
      var DATE_RFC2822 = "ddd, DD MMM YYYY HH:mm:ss ZZ";
      var datetimeValue = $("#input_datetimepicker_end").val();

      $("#input_text_confirm_end").val(datetimeValue);
      $("#input_text_end").val(datetimeValue);

      var momentKu = moment(datetimeValue);
      $("#input_text_end_readable").val(momentKu.format(DATE_RFC2822));

    });
  </script>
  <script type="text/javascript">
    toggleEndDateTimePickerAsNeeded();
    $("#is_ended").change(function(){
      toggleEndDateTimePickerAsNeeded();
    });
    function toggleEndDateTimePickerAsNeeded()
    {
      if($("#is_ended").is(":checked"))
      {
        $("#container_datetimepicker_end").show();
      }
      else
      {
        $("#container_datetimepicker_end").hide();
      }
    }
  </script>
@endsection