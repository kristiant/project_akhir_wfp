<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminMasAppFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "master_student_id" => "required|not_in:-1",
            "apprentice_student_id" => "required|not_in:-1",
            "subject_id" => "required|integer",
            "input_text_start" => "required|date",
        ];
    }
}
