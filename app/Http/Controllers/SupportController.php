<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;

class SupportController extends Controller
{
    //admin
    public function searchSchoolAdmin(Request $request)
    {
        return redirect(route('adminSchoolSearch', ['keyword' => $request->keyword, 'page_number' => 1]));
    }
    public function searchStudentAdmin(Request $request)
    {
        return redirect(route('adminStudentSearch', ['keyword' => $request->keyword, 'page_number' => 1]));
    }
    public function searchSubjectAdmin(Request $request)
    {
        return redirect(route('adminSubjectSearch', ['keyword' => $request->keyword, 'page_number' => 1]));
    }
    public function searchTestAdmin(Request $request)
    {
        return redirect(route('adminTestSearch', ['keyword' => $request->keyword, 'page_number' => 1]));
    }
    public function searchMasterApprenticeAdmin(Request $request)
    {
        return redirect(route('adminMasAppSearch', ['keyword' => $request->keyword, 'page_number' => 1]));
    }

    //public
    public function searchSchoolPublic(Request $request)
    {
        return redirect(route('publicSchoolSearch', ['keyword' => $request->keyword, 'page_number' => 1]));
    }
    public function searchStudentPublic(Request $request)
    {
        return redirect(route('publicStudentSearch', ['keyword' => $request->keyword, 'page_number' => 1]));
    }

}
