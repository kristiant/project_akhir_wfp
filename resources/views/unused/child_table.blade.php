@extends('parent_template')
@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">{{ $table["title"] }}</h4>
              <p class="card-category">{{ $table["subtitle"] }}</p>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    @foreach($table["columns"] as $col)
                      <th>{{ $col }}</th>
                    @endforeach
                  </thead>
                  <tbody>
                    @foreach($table["data"] as $row)
                    <tr>
                      @foreach($row["data"] as $cell)
                        <td
                        @if( in_array($loop->index, $table["primaryIndex"]) )
                          class="text-primary"
                        @endif
                        >{{ $cell }}</td>
                      @endforeach
                      <!-- td>
                        <button type="submit" class="btn btn-primary">Edit</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                      </td -->
                      @if( isset($row["buttons"]) && count($row["buttons"]) > 0 )
                        <td>
                        @foreach($row["buttons"] as $cell)
                          <a href="{{ $cell["href"] }}"><button href="{{ $cell["href"] }}" type="submit" class="btn btn-{{ $cell["color"] }}">{{ $cell["text"] }}</button></a>
                        @endforeach
                        </td>
                      @endif
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection