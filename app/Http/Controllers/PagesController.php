<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;
use App\Student;
use App\Subject;
use App\Test;
//use App\TestStudent;
use Config;
use Auth;

class PagesController extends Controller
{
    public function dev()
    {
        /*
        //
        //$s = new School();
        $s = School::find('999');
        $s->id = '999';
        $s->nama = 'hello';
        $s->alamat = 'hlo';
        $s->status = 'hlo1';
        $s->jenjang = 'hlo2';
        $s->prestige = 200;
        $s->level = 3;
        $s->x = 2.1;
        $s->y = 4.3;
        //$s->save();
        $s->delete();

        $ss = School::where('nama', 'hello')->get();

        dd($ss);
        */

        //dd(Auth::user());

        //dd(Student::find(160416024)->masters);
        //dd(Student::find(160416024)->apprentices);
        //dd(Student::find(160416024)->getAverageScoreOnSubject(Subject::find(7)));

        //$s = new TestStudent();
        //$s->test_id=1;
        //$s->student_id=160416024;
        //$s->nilai=9999;
        //dd($s->student); // ADA ISINYA
        //dd($s);

        //dd(Student::find(160416024)->recountSpecialization());
        //dd(Student::find(160416024)->getExtremeSubject('MIN'));

        /*
        $me = Student::find(160416024);
        $math = Subject::find(2);

        dd($me->getRataRataNilaiSubject($math));
        */

        //dd(Student::find(160416024)->getArrRecommendedMasters());
        dd(Student::find(160416024)->getArrRecommendedMasApp('apprentice'));
    }

    public function root()
    {
        if(Auth::user() == null)
        {
            return redirect(route('publicStudentIndex'));
        }
        else
        {
            return redirect(route('home'));
        }

    }





























    /*
    public function loginPublic()
    {
        $viewArgs = Config::get('kristian.view_config_common'); // set common view arguments (page name, template, etc)
        $viewArgs["leftNavigations"] = [];
        $viewArgs["pageTitle"] = "Member Login - Hall of Academic Fame"; // set page title displayed in browser's titlebar
        unset($viewArgs["searchArr"]); // do not enable search

        // login parameter
        $viewArgs["login"]["title"] = "Member Login";
        $viewArgs["login"]["text"] = "Login to Hall of Academic Fame Member Area";
        $viewArgs["login"]["action"] = "/login";
        $viewArgs["login"]["method"] = "POST";
        $viewArgs["login"]["usernameHint"] = "Enter your username here";
        $viewArgs["login"]["buttonText"] = "Login";

        return view('common_login', $viewArgs);
    }

    public function loginPublicProcess(Request $request)
    {
        //STUBBED (BELUM DIAJARI CARA LOGIN)
        $username = $request->username;
        $password = $request->password;

        return redirect('/');
    }

    public function indexPublic()
    {
        // STUBBED (BELUM DIAJARI CARA LOGIN)
        $isLoggedIn = true;
        if($isLoggedIn == false)
        {
            return redirect('/login');
        }
        else
        {
            return 'Logged in!';
        }
    }

    public function logoutPublic()
    {
        //STUBBED (BELUM DIAJARI CARA LOGIN)
        return redirect('/');
    }

    public function listStudentsSimple()
    {
        $arrArgs = Config::get('kristian.view_config_common');
        $arrArgs["table"] = array(
            "title" => "Tabelku",
            "subtitle" => "ada tabel disini",
            "columns" => array("Rank", "Name", "Power", "Money"),
            "data" => array(
                array(
                    "data" => array("1", "Kristian0", "100", "204721")
                ),
                array(
                    "data" => array("1", "Kristian0", "100", "204721")
                ),
                array(
                    "data" => array("1", "Kristian0", "100", "204721")
                )
            ),
            "primaryIndex" => array(0)
        );
        $arrArgs["leftNavigations"][1]["isActive"] = true;
        return view('child_table', $arrArgs);
    }

    public function listStudents()
    {
        //$arrArgs = $this->defaultContent; // php copy arrays by value, not by reference, so its safe
        $arrArgs = Config::get('kristian.view_config_common');
        $arrArgs["table"] = array(
            "title" => "Global Ranking",
            "subtitle" => "Ranking keseluruhan siswa di Surabaya",
            "columns" => array("Rank", "Name", "Class", "Glory", "School", "Apprentice(s)", "Specialization", "View Profile"),
            "data" => array(
                array(
                    "data" => array("1", "Chris Rivesky", "Logician", "204721", "Hogwarts High", "5", "Math"),
                    "buttons" => array(
                        array("text" => "view", "color" => "primary", "href" => "/student/1")
                    )
                ),
                array(
                    "data" => array("2", "Kristian Tanuwijaya", "Data Scientist", "144721", "Oubayaa High", "3", "Computer Science"),
                    "buttons" => array(
                        array("text" => "view", "color" => "primary", "href" => "/student/1")
                    )
                ),
                array(
                    "data" => array("3", "Simeon Yuda Prasetyo", "Tutor", "124721", "Oubayaa High", "24", "Artificial Intelligence"),
                    "buttons" => array(
                        array("text" => "view", "color" => "primary", "href" => "/student/1")
                    )
                )
            ),
            "primaryIndex" => array(0,3)
        );
        for ($i=4; $i <= 20; $i++)
        {
            $arrArgs["table"]["data"][] = array(
                "data" => array($i, str_random(15), str_random(10), mt_rand(70000, 100000), str_random(15), mt_rand(0, 4), str_random(5)),
                "buttons" => array(
                    array("text" => "view", "color" => "primary", "href" => "/student/1")
                )
            );
        }



        $arrArgs["leftNavigations"][1]["isActive"] = true;
        return view('child_table', $arrArgs);
    }

    public function listStudentsAdmin()
    {
        //$arrArgs = $this->defaultContent; // php copy arrays by value, not by reference, so its safe
        $arrArgs = Config::get('kristian.view_config_common');

        $arrArgs["table"] = array(
            "title" => "Tabelku",
            "subtitle" => "ada tabel disini",
            "columns" => array("Rank", "Name", "Power", "Money", "Action"),
            "data" => array(
                array(
                    "data" => array("1", "Kristian0", "100", "204721"),
                    "buttons" => array(
                        array("text" => "edit", "color" => "primary", "href" => "/student/1/edit"),
                        array("text" => "delete", "color" => "danger", "href" => "/student/1/delete")
                    )
                ),
                array(
                    "data" => array("1", "Kristian0", "100", "204721"),
                    "buttons" => array(
                        array("text" => "edit", "color" => "primary", "href" => "/student/1/edit"),
                        array("text" => "delete", "color" => "danger", "href" => "/student/1/delete")
                    )
                ),
                array(
                    "data" => array("1", "Kristian0", "100", "204721"),
                    "buttons" => array(
                        array("text" => "edit", "color" => "primary", "href" => "/student/1/edit"),
                        array("text" => "delete", "color" => "danger", "href" => "/student/1/delete")
                    )
                )
            ),
            "primaryIndex" => array(0)
        );
        $arrArgs["leftNavigations"][1]["isActive"] = true;
        return view('child_table', $arrArgs);
    }

    public function studentDetail($student_id)
    {
        //$arrArgs = $this->defaultContent; // php copy arrays by value, not by reference, so its safe
        $arrArgs = Config::get('kristian.view_config_common');

        $arrArgs["page"] = array(
            "title" => "Student of Oubayaa High School",
            "subtitle" => "Science student of Oubayaa High, designed as Division A 3rd Class (XII-A IPA)"
        );

        if($student_id == 1)
        {
            $arrArgs ["profile"] = array(
                "pictureHref" => "#",
                "pictureSrc" => "faces/marc.jpg",
                "title" => "Logician, 1st Rank",
                "subtitle" => "Specialization: Math, Physics",
                "name" => "Chris Riversky",
                "description" => "Medals: State-Licensed Magician, First B100d, Invincible General",
                "linkHref" => "/school/1",
                "linkText" => "View School"
            );
        }
        else
        {
            $arrArgs ["profile"] = array(
                "pictureHref" => "#",
                "pictureSrc" => "faces/card-profile1-square.jpg",
                "title" => "Beginner, 6-Digit-Rank",
                "subtitle" => "Specialization: Art",
                "name" => "John Doe",
                "description" => "Medals: -",
                "linkHref" => "/school/1",
                "linkText" => "View School"
            );
        }
        $arrArgs["leftNavigations"][0]["isActive"] = true;


        $arrArgs["radar"] = array(
            "labels" => array("Math", "Physics", "Chemistry", "Biology", "Geography", "History", "Sociology", "Art"),
            "datasets" => array(
                array(
                    "label" => "Academic",
                    "backgroundColor" => "rgba(0, 0, 100, 0.5)",
                    "data" => array(100, 95, 97, 80, 60, 40, 50, 70)
                ),
                array(
                    "label" => "Practical Skills",
                    "backgroundColor" => "rgba(100, 0, 0, 0.5)",
                    "data" => array(70, 80, 87, 60, 90, 30, 60, 50)
                )
            )
        );

        $arrArgs["lineHistory"] = array(
            "labels" => array("January", "February", "March", "April", "May"),
            "datasets" => array(
                array(
                    "label" => "Math",
                    "backgroundColor" => "rgba(0, 0, 0, 0.5)",
                    "data" => array(90, 95, 80, 70, 100),
                    "borderColor" => "rgb(0,0,0)",
                    "fill" => false
                )
            )
        );

        $arrArgs["profile"]["logoLabel"] = "School";
        $arrArgs["profile"]["logoImgSrc"] = "ubaya.png";
        $arrArgs["profile"]["logoText"] = "Oubayaa High";

        $arrArgs["profile"]["points"] = "Glory: 3486921 (Top 0.1%)";


        $html = '<div class="mapouter"><div class="gmap_canvas"><iframe width="600" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q=universitas%20surabaya&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.pureblack.de">webseite erstellen lassen</a></div><style>.mapouter{text-align:right;height:400px;width:600px;}.gmap_canvas {overflow:hidden;background:none!important;height:400px;width:600px;}</style></div>';

        $arrArgs["plainContent"] = $html;

        return view('child_profilestudent', $arrArgs);
    }

    public function listSchools()
    {
        //$arrArgs = $this->defaultContent; // php copy arrays by value, not by reference, so its safe
        $arrArgs = Config::get('kristian.view_config_common');

        $arrArgs["table"] = array(
            "title" => "Schools and Training Facility",
            "subtitle" => "Daftar dan perbandingan seluruh sekolah dan lembaga pendidikan",
            "columns" => array("Rank", "Name", "Prestige", "Level", "#Rankers", "Specialization", "#Specialist", "View Profile"),
            "data" => array(
                array(
                    "data" => array("1", "Oubayaa High", "27453672", "****", "6", "Computer", "31"),
                    "buttons" => array(
                        array("text" => "view", "color" => "primary", "href" => "/school/1")
                    )
                ),
                array(
                    "data" => array("2", "Hogwarts High", "21453672", "*****", "3", "Magic", "53"),
                    "buttons" => array(
                        array("text" => "view", "color" => "primary", "href" => "/school/1")
                    )
                ),
                array(
                    "data" => array("3", "Lanseal High", "16453672", "***", "2", "Automotive and Weaponry", "5"),
                    "buttons" => array(
                        array("text" => "view", "color" => "primary", "href" => "/school/1")
                    )
                )
            ),
            "primaryIndex" => array(0,3,4)
        );
        for ($i=4; $i <= 20; $i++)
        {
            $arrArgs["table"]["data"][] = array(
                "data" => array($i, str_random(15), mt_rand(70000, 100000), "***", mt_rand(0, 4), str_random(5), mt_rand(3,10)),
                "buttons" => array(
                    array("text" => "view", "color" => "primary", "href" => "/student/1")
                )
            );
        }

        $arrArgs["leftNavigations"][2]["isActive"] = true;
        return view('child_table', $arrArgs);
    }

    public function schoolDetail($school_id)
    {
        //$arrArgs = $this->defaultContent; // php copy arrays by value, not by reference, so its safe
        $arrArgs = Config::get('kristian.view_config_common');


        $arrArgs["leftNavigations"][2]["isActive"] = true;

        return view('child_school', $arrArgs);
    }

    public function googleMaps()
    {
        $html = '<div class="mapouter"><div class="gmap_canvas"><iframe width="600" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q=universitas%20surabaya&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><style>.mapouter{text-align:right;height:400px;width:600px;}.gmap_canvas {overflow:hidden;background:none!important;height:400px;width:600px;}</style></div>';

        //$arrArgs = $this->defaultContent; // php copy arrays by value, not by reference, so its safe
        $arrArgs = Config::get('kristian.view_config_common');

        $arrArgs["plainContent"] = $html;

        $arrArgs["leftNavigations"][3]["isActive"] = true;
        return view('child_empty', $arrArgs);
    }

    public function dashboard()
    {
        //$arrArgs = $this->defaultContent; // php copy arrays by value, not by reference, so its safe
        $arrArgs = Config::get('kristian.view_config_common');

        return view('child_dashboard', $arrArgs);
    }

    public function loginPage()
    {
        //$arrArgs = $this->defaultContent; // php copy arrays by value, not by reference, so its safe
        $arrArgs = Config::get('kristian.view_config_common');

        unset($arrArgs["leftNavigations"][4]);
        unset($arrArgs["leftNavigations"][3]);
        unset($arrArgs["leftNavigations"][0]);
        unset($arrArgs["topIconNavigations"]);
        unset($arrArgs["searchArr"]);
        return view('child_login', $arrArgs);
    }

    public function battle()
    {
        //$arrArgs = $this->defaultContent; // php copy arrays by value, not by reference, so its safe
        $arrArgs = Config::get('kristian.view_config_common');

        $arrArgs["leftNavigations"][4]["isActive"] = true;
        return view('child_battle', $arrArgs);
    }

    public function battleRoom($battle_room_id)
    {
        //$arrArgs = $this->defaultContent; // php copy arrays by value, not by reference, so its safe
        $arrArgs = Config::get('kristian.view_config_common');

        $arrArgs["leftNavigations"][4]["isActive"] = true;
        return view('child_battleroom', $arrArgs);
    }
    */
}
