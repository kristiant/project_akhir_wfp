<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;
use App\Student;
use App\Subject;
use App\Test;
use App\MasterApprentice;
use Auth;
use Config;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function home()
    {
        $viewArgs["menus"] = [];

        if(Auth::user()->is_admin)
        {
            $viewArgs["menus"]["admin"] =
            [
                "isActive" => false,
                "href" => "adminIndex",
                "text" => "Administration Panel",
                "icon" => "settings",
                "color" => "danger",
                "description" => "",
            ];
        }

        $viewArgs["menus"]["public"] =
        [
            "isActive" => false,
            "href" => "publicStudentIndex",
            "text" => "Rankings",
            "icon" => "public",
            "color" => "success",
            "description" => "",
        ];

        if(Auth::user()->is_student)
        {
            $viewArgs["menus"]["myprofile"] =
            [
                "isActive" => false,
                "href" => "myProfile",
                "text" => "My Profile",
                "icon" => "person",
                "color" => "primary",
                "description" => "",
            ];
        }

        return view('common.home_menu', $viewArgs);
    }

    public function myProfile()
    {
        if(Auth::user()->is_student)
        {
            return redirect(route('publicStudentProfile', ['id' => Auth::user()->id]));
        }
        else
        {
            return redirect(route('home'));
        }
    }


    public function indexAdmin()
    {
        $viewArgs["leftNavigations"]["home_admin"]["isActive"] = true; // set which sidebar is active
        $viewArgs["pageTitle"] = "Admin - Hall of Academic Fame"; // set page title displayed in browser's titlebar
        
        $viewArgs["leftNavigations"]["home_admin"]["description"] = "";
        $viewArgs["leftNavigations"]["schools"]["description"] = School::all()->count() . " item(s)";
        $viewArgs["leftNavigations"]["students"]["description"] = Student::all()->count() . " item(s)";
        $viewArgs["leftNavigations"]["subjects"]["description"] = Subject::all()->count() . " item(s)";
        $viewArgs["leftNavigations"]["tests"]["description"] = Test::all()->count() . " item(s)";
        $viewArgs["leftNavigations"]["mas-app"]["description"] = MasterApprentice::all()->count() . " item(s)";

        $viewArgs["menus"] = $viewArgs["leftNavigations"];
        unset($viewArgs["menus"]["home_admin"]); // supaya tidak di-daftar di bagian content halaman

        //dd($viewArgs); exit;

        return view('admin.template', $viewArgs);
    }
}
