<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Variables / Configs that are needed to be shown on common view
    |--------------------------------------------------------------------------
    |
    | Just pass common setting config that will be passed into common/parent view
    | Can be customized as needed in controls
    |
    |
    */

    'view_config_common' => [
        //"pageTitle" => "Hall of Academic Fame",
        //"sidebarColor" => "azure",
        //"sidebarImage" => "sidebar-2.jpg",
        //"logoHref" => "home",
        //"appName" => "Hall of Academic Fame",
        "leftNavigations" => [],
        //"pageHeader" => "Surabaya System",
        //"pageHeaderHref" => "#",
        //"footerHref" => "#",
        //"footerText" => "Let your potential be unleashed!",
        //"footerCopyright" => "Beta Software Production Inc.",
        "topIconNavigations" => [],
        /*
        "topIconNavigations" => [
            [
                "href" => "/student/1",
                "text" => "My Profile",
                "icon" => "person",
                "isDropdown" => false,
            ],
            [
                "href" => "#",
                "text" => "Notifications",
                "icon" => "notifications",
                "isDropdown" => true,
                "number" => "2",
                "arrDropdown" => [
                    [
                        "href" => "#",
                        "text" => "Select your specialization",
                    ],
                    [
                        "href" => "#",
                        "text" => "1 apprentice applied for you",
                    ],
                ],
            ],
            [
                "href" => "/login",
                "text" => "Logout",
                "icon" => "exit_to_app",
                "isDropdown" => false,
            ],
        ],
        */
        "searchArr" => [
            "text" => "",
            "placeholder" => "Search...",
            "formAction" => "#",
        ],
    ],


    'left_nav_admin' => [
    /*
        "home_admin" =>
        [
            "isActive" => false,
            "href" => "adminIndex",
            "text" => "Administration Panel",
            "icon" => "settings",
            "color" => "secondary",
        ],
        "schools" =>
        [
            "isActive" => false,
            "href" => "schools.index",
            "text" => "Schools",
            "icon" => "school",
            "color" => "warning",
        ],
        "students" =>
        [
            "isActive" => false,
            "href" => "students.index",
            "text" => "Students",
            "icon" => "people",
            "color" => "primary",
        ],
        "subjects" =>
        [
            "isActive" => false,
            "href" => "subjects.index",
            "text" => "Subjects",
            "icon" => "book",
            "color" => "danger",
        ],
        "tests" =>
        [
            "isActive" => false,
            "href" => "tests.index",
            "text" => "Tests",
            "icon" => "library_books",
            "color" => "success",
        ],
        "mas-app" =>
        [
            "isActive" => false,
            "href" => "masapp.index",
            "text" => "Mas-App",
            "icon" => "group_work",
            "color" => "success",
        ],
    */
    ],

    'left_nav_public' => [
    /*
        "students" =>
        [
            "isActive" => false,
            "href" => "publicStudentIndex",
            "text" => "Students Ranking",
            "icon" => "people",
            "color" => "success",
        ],
        "schools" =>
        [
            "isActive" => false,
            "href" => "publicSchoolIndex",
            "text" => "Schools Ranking",
            "icon" => "school",
            "color" => "success",
        ],
        "mas-app" =>
        [
            "isActive" => false,
            "href" => "memberMasappLanding",
            "text" => "Mas-App",
            "icon" => "group_work",
            "color" => "success",
        ],
        /*
        "battles" =>
        [
            "isActive" => false,
            "href" => "#",
            "text" => "Battle",
            "icon" => "games",
            "color" => "success",
        ],
        */
    ],

    'pagination_max_item' => 10, // dalam 1 halaman (page) bisa diisi berapa item tabelnya
    'max_rank_displayed' => 999, // dianjurkan angkanya 9, atau 99, atau 999, atau 9999, atau 99999, dst...
    // hanya akan menampilkan 999 Student dengan glory tertinggi
    // juga untuk mereka yang rank nya lebih buruk dari angka itu nantinya akan ditampilkan sebagai X-digit rank
    // (tujuan: agar yang rank bawah tidak minder)


    'history_student_numberofitems' => 10,
    'history_school_numberofitems' => 10,

];
