<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Student::class, function (Faker $faker) {
    return [
        'id' => '0',
        'school_id' => '1',
        'nama' => $faker->name,
        'kelas' => mt_rand(1,3),
        'glory' => '0',
        'specialization' => str_random(10),
        'x' => mt_rand(50, 150),
        'y' => mt_rand(50, 150),
        'created_at' => date("Y-m-d H:i:s",mt_rand(1, time())),
        'updated_at' => date("Y-m-d H:i:s",mt_rand(1, time())),
    ];
});
