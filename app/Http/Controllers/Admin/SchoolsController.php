<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\School;
use Config;
use App\Http\Requests\AdminSchoolFormRequest;

class SchoolsController extends Controller
{
    // need to be logged in as admin to continue
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('adminSchoolIndexPaginated', ['page_number' => 1]));
    }

    /**
     * Display a listing of the resource with pagination.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPaginated($page_number)
    {
        return $this->search(null, $page_number);
    }

    public function search($keyword, $page_number)
    {
        $query = null;
        if($keyword == null)
        {
            $query = School::all();
        }
        else
        {
            $query = School::where('nama', 'like', '%' . $keyword . '%')
                ->orWhere('id', 'like', $keyword . '%')
                ->get();
        }

        $schools = $query->forPage($page_number, Config::get('kristian.pagination_max_item'));
        $viewArgs["schools"] = $schools;

        $viewArgs["page"]["current"] = $page_number;
        $viewArgs["page"]["max"] = ceil($query->count() / Config::get('kristian.pagination_max_item'));
        if($viewArgs["page"]["max"] == 0) $viewArgs["page"]["max"] = 1;

        if($keyword != null && isset($keyword)) $viewArgs["keyword"] = $keyword;
        
        return view('admin.school.list', $viewArgs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school = new School();
        $school->id = -1;

        $viewArgs["school"] = $school;
        return view('admin.school.edit', $viewArgs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminSchoolFormRequest $request)
    {
        try
        {
            $school = new School();
            $school->id = $request->id;
            $school->nama = $request->nama;
            $school->alamat = $request->alamat;
            $school->status = $request->status;
            $school->jenjang = $request->jenjang;
            $school->prestige = $request->prestige;
            $school->level = $request->level;
            $school->x = $request->x;
            $school->y = $request->y;

            //handle picture upload
            if ($request->hasFile('picture'))
            {
                $picture = $request->file('picture');
                $destinationPath = base_path('public').'/img/schools/';
                $name = $school->id . '.' . $picture->getClientOriginalExtension();
                if (File::exists($destinationPath . $school->id . ".jpg"))
                {
                    File::delete($destinationPath . $school->id . ".jpg");
                }
                if (File::exists($destinationPath . $school->id . ".png"))
                {
                    File::delete($destinationPath . $school->id . ".png");
                }
                $picture->move($destinationPath, $name);
            }

            $school->save();
            //return json_encode(['status' => 'success']);
            return redirect(route('schools.edit', ['id' => $request->id]));
        }
        catch (Exception $e)
        {
            return json_encode(['status' => 'failed', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $schools = School::find($id); // dd($schools);
        $viewArgs["school"] = $schools;
        return view('admin.school.detail', $viewArgs);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schools = School::find($id);
        $viewArgs["school"] = $schools;
        return view('admin.school.edit', $viewArgs);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminSchoolFormRequest $request, $id)
    {
        try
        {
            $school = School::find($id);
            $school->nama = $request->nama;
            $school->alamat = $request->alamat;
            $school->status = $request->status;
            $school->jenjang = $request->jenjang;
            $school->prestige = $request->prestige;
            $school->level = $request->level;
            $school->x = $request->x;
            $school->y = $request->y;

            if ($request->hasFile('picture'))
            {
                $picture = $request->file('picture');
                $destinationPath = base_path('public').'/img/schools/';
                $name = $school->id . '.' . $picture->getClientOriginalExtension();
                if (File::exists($destinationPath . $school->id . ".jpg"))
                {
                    File::delete($destinationPath . $school->id . ".jpg");
                }
                if (File::exists($destinationPath . $school->id . ".png"))
                {
                    File::delete($destinationPath . $school->id . ".png");
                }
                $picture->move($destinationPath, $name);
            }

            $school->save();
            //return json_encode(['status' => 'success']);
            return redirect(route('schools.edit', ['id' => $id]));
        }
        catch (Exception $e)
        {
            return json_encode(['status' => 'failed', 'message' => $e->getMessage()]);
        }




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $school = School::find($id);

            $school->delete();
            return json_encode(['status' => 'success']);
        }
        catch (Exception $e)
        {
            return json_encode(['status' => 'failed', 'message' => $e->getMessage()]);
        }
    }





    public function logPrestige()
    {
        try
        {
            $schools = School::all();
            foreach ($schools as $key => $value)
            {
                $value->logPrestige();
            }
            return json_encode(['status' => 'success']);
        }
        catch (Exception $e)
        {
            return json_encode(['status' => 'failed', 'message' => $e->getMessage()]);
        }
    }
}
