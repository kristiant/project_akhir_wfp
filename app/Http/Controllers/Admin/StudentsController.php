<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Student;
use App\School;
use Config;
use App\Http\Requests\AdminStudentFormRequest;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('adminStudentIndexPaginated', ['page_number' => 1]));
    }

    /**
     * Display a listing of the resource with pagination.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPaginated($page_number)
    {
        return $this->search(null, $page_number);
    }

    public function search($keyword, $page_number)
    {
        $query = null;
        if($keyword == null)
        {
            $query = Student::all();
        }
        else
        {
            $query = Student::where('nama', 'like', '%' . $keyword . '%')
                ->orWhere('id', 'like', $keyword . '%')
                ->get();
        }

        $students = $query->forPage($page_number, Config::get('kristian.pagination_max_item'));
        $viewArgs["students"] = $students;

        $viewArgs["page"]["current"] = $page_number;
        $viewArgs["page"]["max"] = ceil($query->count() / Config::get('kristian.pagination_max_item'));
        if($viewArgs["page"]["max"] == 0) $viewArgs["page"]["max"] = 1;

        if($keyword != null && isset($keyword)) $viewArgs["keyword"] = $keyword;

        return view('admin.student.list', $viewArgs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $student = new Student();
        $student->id = -1;
        $student->school_id = -1;

        $schoolsIdName = [];

        $schools = School::all();
        foreach ($schools as $key => $value)
        {
            $item = [];
            $item["value"] = $value->id;
            $item["label"] = $value->nama; // . " - NPNS " . $value->id;
            $schoolsIdName[] = $item;
        }

        $viewArgs["student"] = $student;
        $viewArgs["schoolsIdName"] = $schoolsIdName;
        return view('admin.student.edit', $viewArgs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminStudentFormRequest $request)
    {
        try
        {
            $student = new Student();
            $student->id = $request->id;
            $student->school_id = $request->school_id;
            $student->nama = $request->nama;
            $student->kelas = $request->kelas;
            $student->glory = $request->glory;
            $student->specialization = $request->specialization;
            $student->x = $request->x;
            $student->y = $request->y;
            $student->apprentice_skill_up = 0;

            //handle picture upload
            if ($request->hasFile('picture'))
            {
                $picture = $request->file('picture');
                $destinationPath = base_path('public').'/img/students/';
                $name = $student->id . '.' . $picture->getClientOriginalExtension();
                if (File::exists($destinationPath . $student->id . ".jpg"))
                {
                    File::delete($destinationPath . $student->id . ".jpg");
                }
                if (File::exists($destinationPath . $student->id . ".png"))
                {
                    File::delete($destinationPath . $student->id . ".png");
                }
                $picture->move($destinationPath, $name);
            }

            $student->save();
            //return json_encode(['status' => 'success']);
            return redirect(route('students.edit', ['id' => $request->id]));
        }
        catch (Exception $e)
        {
            return json_encode(['status' => 'failed', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::find($id);

        $schoolsIdName = [];

        $schools = School::all();
        foreach ($schools as $key => $value)
        {
            $item = [];
            $item["value"] = $value->id;
            $item["label"] = $value->nama; // . " - NPNS " . $value->id;
            $schoolsIdName[] = $item;
        }

        $viewArgs["student"] = $student;
        $viewArgs["schoolsIdName"] = $schoolsIdName;
        return view('admin.student.edit', $viewArgs);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminStudentFormRequest $request, $id)
    {
        try
        {
            $student = Student::find($id);
            $student->school_id = $request->school_id;
            $student->nama = $request->nama;
            $student->kelas = $request->kelas;
            $student->glory = $request->glory;
            $student->specialization = $request->specialization;
            $student->x = $request->x;
            $student->y = $request->y;

            //handle picture upload
            if ($request->hasFile('picture'))
            {
                $picture = $request->file('picture');
                $destinationPath = base_path('public').'/img/students/';
                $name = $student->id . '.' . $picture->getClientOriginalExtension();
                if (File::exists($destinationPath . $student->id . ".jpg"))
                {
                    File::delete($destinationPath . $student->id . ".jpg");
                }
                if (File::exists($destinationPath . $student->id . ".png"))
                {
                    File::delete($destinationPath . $student->id . ".png");
                }
                $picture->move($destinationPath, $name);
            }

            $student->save();
            //return json_encode(['status' => 'success']);
            return redirect(route('students.edit', ['id' => $id]));
        }
        catch (Exception $e)
        {
            return json_encode(['status' => 'failed', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $student = Student::find($id);

            $student->delete();
            return json_encode(['status' => 'success']);
        }
        catch (Exception $e)
        {
            return json_encode(['status' => 'failed', 'message' => $e->getMessage()]);
        }
    }

}
