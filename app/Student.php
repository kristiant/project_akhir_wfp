<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Config;
use Exception;

class Student extends Model
{
    protected $table = 'students';
    public $timestamps = true;
    protected $primaryKey = 'id';
    protected $keyType = 'string';

    // overriding general methods
    public function delete()
    {
        StudentHistoryGlory::where('student_id', $this->id)->delete();
        parent::delete();
    }

    // relationships
    public function school()
    {
        return $this->belongsTo('App\School');
    }

    /*
    public function testStudentDetails()
    {
        return $this->hasMany('App\TestStudent');
    }
    */

    public function tests()
    {
        return $this->belongsToMany('App\Test', 'test_student', 'student_id', 'test_id')
            ->withPivot('nilai')
        ;
    }

    public function masters()
    {
        return $this->hasMany('App\MasterApprentice', 'apprentice_student_id');
    }

    public function apprentices()
    {
        return $this->hasMany('App\MasterApprentice', 'master_student_id');
    }

    public function proposalsSent()
    {
        return $this->hasMany('App\Proposal', 'sender_student_id');
    }

    public function proposalsReceived()
    {
        return $this->hasMany('App\Proposal', 'receiver_student_id');
    }

    // other methods
    public function studentGloryHistories()
    {
        return $this->hasMany('App\StudentHistoryGlory');
    }

    public function logGlory()
    {
        $history = new StudentHistoryGlory();
        $history->student_id = $this->id;
        $history->tanggal = date('Y-m-d');
        $history->glory = $this->glory;

        $history->save();
    }

    public function getImageAsset()
    {
        if (File::exists(base_path('public').'/img/students/' . $this->id . '.png'))
        {
            return asset('/img/students/' . $this->id . '.png');
        }
        else if (File::exists(base_path('public').'/img/students/' . $this->id . '.jpg'))
        {
            return asset('/img/students/' . $this->id . '.jpg');
        }
        else
        {
            return asset('/img/no-image.png');
        }
    }

    public function recountGlory($argNilaiUlanganBaru) // $argUlanganBaru = object dari model Test
    {
        $gloryLama = $this->glory;

        $jumlahUlanganSebelumnya = $this->tests()->count(); //TestStudent::where('student_id', $this->id)->count();

        if($jumlahUlanganSebelumnya == 0 || $jumlahUlanganSebelumnya == null)
        {
            $gloryBaru = $argNilaiUlanganBaru * 1000;
        }
        else
        {
            $gloryBaru = (($gloryLama * $jumlahUlanganSebelumnya) + ($argNilaiUlanganBaru * 1000)) / ($jumlahUlanganSebelumnya + 1);
        }

        $this->glory = $gloryBaru;
        return $gloryBaru;
    }

    public function getRank()
    {
        // ranking berapa Student $this sekarang
        $higherStudents = Student::where('glory', '>', $this->glory);
        return $higherStudents->count() + 1;
    }

    public function getExtremeSubject($argExtremeType = 'MAX') // bisa 'MAX' bisa 'MIN', jika 'MAX' akan cari subject yang paling dikuasai student $this, jika 'MIN' akan cari subject yang student $this paling lemah / tidak bisa
    {
        $orderByMethod = '';
        if($argExtremeType == 'MAX')
        {
            $orderByMethod = 'desc';
        }
        else if($argExtremeType == 'MIN')
        {
            $orderByMethod = 'asc';
        }
        else
        {
            throw new Exception("method getExtremeSubject(argExtremeType) receive invalid argument: " . $argExtremeType, 1);
        }

        $subject = Subject::where('subjects.id', '<>', '-1')
            ->join('tests', 'tests.subject_id', '=', 'subjects.id')
            //->join('schools', 'schools.id', '=', 'tests.school_id')
            ->join('test_student', 'tests.id', '=', 'test_student.test_id')
            ->join('students', 'students.id', '=', 'test_student.student_id')
            ->where('students.id', $this->id)
            ->groupBy('subjects.id', 'subjects.nama', 'subjects.singkatan')
            ->orderBy('ratarata', $orderByMethod)
            ->select('subjects.id', 'subjects.nama', 'subjects.singkatan', DB::raw('AVG(test_student.nilai) AS ratarata'))
            ->first();

        return $subject;
    }

    public function getRataRataNilaiSubject($argSubject) // return double, jika null berarti belum pernah ambil ujian dengan subject itu
    {
        //
        $subject = Subject::where('subjects.id', $argSubject->id)
            ->join('tests', 'tests.subject_id', '=', 'subjects.id')
            //->join('schools', 'schools.id', '=', 'tests.school_id')
            ->join('test_student', 'tests.id', '=', 'test_student.test_id')
            ->join('students', 'students.id', '=', 'test_student.student_id')
            ->where('students.id', $this->id)
            ->groupBy('subjects.id', 'subjects.nama', 'subjects.singkatan')
            //->orderBy('ratarata', $orderByMethod)
            ->select('subjects.id', 'subjects.nama', 'subjects.singkatan', DB::raw('AVG(test_student.nilai) AS ratarata'))
            ->first();

        if($subject == null)
        {
            return null;
        }
        else
        {
            return $subject->ratarata;
        }
    }

    public function recountSpecialization()
    {
        $subject = $this->getExtremeSubject('MAX');
        if($subject == null)
        {
            return null;
        }
        else
        {
            return $subject->nama;
        }

        /*
        // ini hanya ambil nilai ulangan yang tertinggi milik Student $this itu pada mata pelajaran apa
        $test_student = Subject::where('subjects.id', '<>', '-1')
            ->join('students', 'students.id', '=', 'test_student.student_id')
            ->join('schools', 'schools.id', '=', 'students.school_id')
            ->join('tests', 'tests.school_id', '=', 'school.id')
            ->join('test_student', 'tests.id', '=', 'test_student.test_id')
            //->join('subjects', 'subjects.id', '=', 'tests.subject_id')
            ->where('students.id', $this->id)
            //->where('subjects.id', $value->id)
            ->orderBy('test_student.nilai', 'desc')
            ->select('subjects.*')->first();
        */

        /*
        // ini ambil rata-rata nilai ulangan yang tertinggi milik Student $this itu pada mata pelajaran apa
        $subject = Subject::where('subjects.id', '<>', '-1')
            ->join('tests', 'tests.subject_id', '=', 'subjects.id')
            ->join('schools', 'schools.id', '=', 'tests.school_id')
            ->join('test_student', 'tests.id', '=', 'test_student.test_id')
            ->join('students', 'students.id', '=', 'test_student.student_id')
            ->where('students.id', $this->id)
            ->groupBy('subjects.id', 'subjects.nama')
            ->orderBy('ratarata', 'desc')
            ->select('subjects.id', 'subjects.nama', DB::raw('AVG(test_student.nilai) AS ratarata'))->first();

        if($subject == null)
        {
            return null;
        }
        else
        {
            return $subject->nama;
        }
        */

    }

    public function getGloryDescription()
    {
        // ex: "Rank 327 (Top 0.1% Globally)", "6-digit rank"
        //return "XX-digit rank";
        $rank = $this->getRank();
        $numberOfStudents = Student::get()->count();
        if( $rank < Config::get('kristian.max_rank_displayed') )
        {
            $percent = ceil($rank / $numberOfStudents  * 100 * 10) / 10;
            // dikali 10 dan dibagi 10 supaya ada ketelitian 1 angka dibelakang koma
            return 'Rank ' . $rank . ' (Top '. $percent .'% Student)';
        }
        else
        {
            return strlen($rank) . '-digit rank';
        }
    }

    // arguments: $argSubject type: Subject (Model)
    public function getAverageScoreOnSubject($argSubject)
    {
        /*
        // ambil seluruh test_id yang mana test tersebut mata pelajaran $argSubject
        $testIdOfSubject = [];
        foreach ($argSubject->tests as $key => $value)
        {
            $testIdOfSubject[] = $value->id;
        }

        // hitung average score
        $testDetailSaya = $this->testStudentDetails->whereIn('test_id', $testIdOfSubject);
        $averageScore = $testDetailSaya->avg('nilai');
        return $averageScore;
        */

        $tests = $this->tests()->where('subject_id','=',$argSubject->id)->get();
        $count = 0; $sum = 0.0;
        foreach ($tests as $key => $value)
        {
            $sum += $value->pivot->nilai;
            $count += 1;
        }
        if($count == 0) return 0;
        return ($sum / $count);
    }

    public function getChartRadarData()
    {
        $result = [
            "labels" => [],
            "datasets" => [],
        ];
        $datasets = [
            "label" => "Academic",
            "backgroundColor" => "rgba(50, 50, 200, 0.65)",
            "data" => [],
        ];

        $subjects = [];
        foreach ($this->school->tests as $key => $value)
        {
            $subjects[] = $value->subject;
        }

        $subjects = collect($subjects)->unique(); // remove duplicates

        foreach ($subjects as $key => $value)
        {
            $result["labels"][] = $value->singkatan;
            $datasets["data"][] = $this->getAverageScoreOnSubject($value);
        }

        $result["datasets"][] = $datasets;
        return $result;
    }

    public function getChartLineGloryHistoryData()
    {
        $labels = [];
        $data = [];

        $histories = StudentHistoryGlory::where('student_id', $this->id)
            ->orderBy('tanggal', 'desc')
            ->take(Config::get('kristian.history_student_numberofitems'))
            ->get();

        foreach ($histories->reverse() as $key => $value)
        {
            $labels[] = $value->tanggal;
            $data[] = $value->glory;
        }
        $labels[] = "Now";
        $data[] = $this->glory;

        return [
            "labels" => $labels,
            "datasets" => [
                [
                    "label" => "Glory",
                    "backgroundColor" => "rgba(0, 0, 0, 0.5)",
                    "data" => $data,
                    "borderColor" => "rgb(255,255,255)",
                    "fill" => false,
                    "lineTension" => 0
                ],
            ],
        ];
    }

    public function distributeGloryToMasters($argTest, $argNilai) // argument type: Test model, int
    {
        $subject = $argTest->subject;
        $nilai = $argNilai;

        $mastersApprenticeArr = $this->masters;

        if($mastersApprenticeArr != null)
        {
            foreach ($mastersApprenticeArr as $key => $value) // for all masters
            {
                if ($value->subject->id == $subject->id && $value->isActive()) // if subject is being mentored by current master
                {
                    $nilaiRataRataSubject = $this->getAverageScoreOnSubject($subject);
                    // if student $this is getting higher score on this subject
                    if($nilai > $nilaiRataRataSubject && $nilaiRataRataSubject != null)
                    {
                        // count master glory bonus, and increment master's number of apprentice skill up
                        $skillUpPoint = ($nilai - $nilaiRataRataSubject) - 5;
                        if($skillUpPoint < 0)
                        {
                            $skillUpPoint = 0; // pastikan skill up point tidak boleh negatif
                        }

                        $value->master->glory = $value->master->glory + pow($skillUpPoint, 3);
                        // rumus: glory baru (milik master) = glory lama (milik master) + (skill up point milik apprentice)^3
                        $value->master->apprentice_skill_up = $value->master->apprentice_skill_up + 1;

                        /*
                        echo "\r\n" . "value" . "\r\n";
                        var_dump($value);
                        echo "\r\n" . "skill up point" . "\r\n";
                        var_dump($skillUpPoint);
                        echo "\r\n" . "nilaiRataRataSubject" . $nilaiRataRataSubject . "\r\n";
                        echo  "\r\n". "glory" . "\r\n";
                        var_dump($value->master->glory);
                        exit;
                        */

                        $value->master->save();
                    }
                }
            }
        }
    }

    public function isCanBeDeleted()
    {
        $jumlahTest = $this->tests()->count();
        if($jumlahTest == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /*
    public function getArrRecommendedMasters()
    {
        $result = [];

        // 1
        $keySubject = $this->getExtremeSubject('MIN');

        // 2
        $query = DB::table($this->table)
            ->select('students.id', DB::raw('ABS( POWER((students.x - '.$this->x.'), 2) + POWER((students.y - '.$this->y.'), 2)  ) AS distance'))
            //->where('students.id', '<>', $this->id)
            ->orderBy('distance', 'asc')
            ->get();

        //dd($query); exit;

        // 3
        $arrPrioritasNilai = [90, 85, 80];
        $arrStudentIdPotensial = []; // array of string (id dari student)
        $my_ratarata = $this->getRataRataNilaiSubject($keySubject);
        foreach ($arrPrioritasNilai as $key => $batasNilai)
        {
            foreach ($query as $key => $student_id)
            {
                $student = Student::find($student_id->id);
                $student_ratarata = $student->getRataRataNilaiSubject($keySubject);

                $isNilaiTidakNull = $student_ratarata != null;
                $isNilaiBagus = $student_ratarata > $batasNilai;
                $isLebihBagusDariAku = $student_ratarata > $my_ratarata;

                if
                (
                    $isNilaiTidakNull // nilainya tidak null
                    && $isNilaiBagus // nilainya dia pada subject itu lebih besar dari 85
                    && $isLebihBagusDariAku // nilainya dia pada subject itu lebih besar dari aku
                    && !in_array($student->id, $arrStudentIdPotensial) // belum masuk array
                    && MasterApprentice::where('master_student_id', $this->id) // bukan masterku
                        ->where('apprentice_student_id', $student->id)
                        ->where('subject_id', $keySubject->id)
                        ->get()
                        ->first() == null
                    && MasterApprentice::where('master_student_id', $student->id) // bukan apprenticeku
                        ->where('apprentice_student_id', $this->id)
                        ->where('subject_id', $keySubject->id)
                        ->get()
                        ->first() == null
                )
                {
                    $arrStudentIdPotensial[] = $student->id;
                }

                if( count($result) == 5 )
                {
                    break 2; // break dari semua loop
                }
            }
        }

        //dd($arrStudentIdPotensial); exit;
        foreach ($arrStudentIdPotensial as $key => $value)
        {
            $result[] = Student::find($value);
        }

        return $result;
    }
    */

    public function getArrRecommendedMasApp($argMasterOrApprentice, $argSubject = null)
    {
        if($argMasterOrApprentice != 'master' && $argMasterOrApprentice != 'apprentice')
        {
            throw new Exception("Argument error on Student.getArrRecommendedMasApp : '" . $argMasterOrApprentice . "', please only use 'master' or 'apprentice'!", 1);
        }

        $result = [];

        // 1) cari key subject
        $keySubject = null;

        if($argSubject == null)
        {
            if($argMasterOrApprentice == 'master')
            {
                $keySubject = $this->getExtremeSubject('MIN');
            }
            else if($argMasterOrApprentice == 'apprentice')
            {
                $keySubject = $this->getExtremeSubject('MAX');
            }
        }
        else
        {
            $keySubject = $argSubject;
        }

        if($keySubject == null)
        {
            return null;
        }

        // 2) list semua student dari yang terdekat
        $query = DB::table($this->table)
            ->select('students.id', DB::raw('ABS( POWER((students.x - '.$this->x.'), 2) + POWER((students.y - '.$this->y.'), 2)  ) AS distance'))
            //->where('students.id', '<>', $this->id)
            ->orderBy('distance', 'asc')
            ->get();

        //dd($query); exit;

        // 3) cari yang memenuhi persyaratan
        $arrPrioritasNilai = null;
        if($argMasterOrApprentice == 'master')
        {
            $arrPrioritasNilai = [90, 85, 80, 70, 50, 30, 0];
        }
        else if($argMasterOrApprentice == 'apprentice')
        {
            $arrPrioritasNilai = [30, 50, 70, 80, 90, 95, 100];
            //$arrPrioritasNilai = [9000, 9100, 9200];
            //$arrPrioritasNilai = [93, 94, 95, 96, 97, 98, 99, 100, 101, 102];
        }

        $arrStudentIdPotensial = []; // array of string (id dari student)
        $my_ratarata = $this->getRataRataNilaiSubject($keySubject);
        foreach ($arrPrioritasNilai as $key => $batasNilai)
        {
            foreach ($query as $key => $student_id)
            {
                $student = Student::find($student_id->id);
                $student_ratarata = $student->getRataRataNilaiSubject($keySubject);

                $isNilaiBagus = false;
                if($argMasterOrApprentice == 'master')
                {
                    $isNilaiBagus = $student_ratarata >= $batasNilai;
                }
                else if($argMasterOrApprentice == 'apprentice')
                {
                    $isNilaiBagus = $student_ratarata <= $batasNilai;
                }

                $isLebihBagusDariAku = false;
                if($argMasterOrApprentice == 'master')
                {
                    $isLebihBagusDariAku = $student_ratarata > $my_ratarata;
                }
                else if($argMasterOrApprentice == 'apprentice')
                {
                    $isLebihBagusDariAku = $student_ratarata < $my_ratarata;
                }

                if
                (
                    $student->id != $this->id // bukan aku
                    && $student_ratarata != null // nilainya tidak null
                    && $isNilaiBagus // nilainya dia pada subject itu lebih besar dari 85
                    && $isLebihBagusDariAku // nilainya dia pada subject itu lebih besar dari aku
                    && !in_array($student->id, $arrStudentIdPotensial) // belum masuk array
                    && MasterApprentice::where('master_student_id', $this->id) // bukan masterku
                        ->where('apprentice_student_id', $student->id)
                        ->where('subject_id', $keySubject->id)
                        ->get()
                        ->first() == null
                    && MasterApprentice::where('master_student_id', $student->id) // bukan apprenticeku
                        ->where('apprentice_student_id', $this->id)
                        ->where('subject_id', $keySubject->id)
                        ->get()
                        ->first() == null
                )
                {
                    $arrStudentIdPotensial[] = $student->id;
                }

                if( count($result) == 5 )
                {
                    break 2; // break dari semua loop
                }
            }
        }

        //dd($arrStudentIdPotensial); exit;
        foreach ($arrStudentIdPotensial as $key => $value)
        {
            $result[] = Student::find($value);
        }

        return $result;
    }


}
