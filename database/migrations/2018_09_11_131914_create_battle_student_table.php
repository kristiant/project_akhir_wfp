<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBattleStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('battle_student', function (Blueprint $table) {
            $table->integer('battle_id')->unsigned();
            $table->string('student_id');
            $table->boolean('is_finished');
            $table->integer('score');

            $table->foreign('battle_id')->references('id')->on('tests');
            $table->foreign('student_id')->references('id')->on('students');
            $table->primary(['battle_id', 'student_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('battle_student');
    }
}
