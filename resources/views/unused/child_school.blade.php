@extends('parent_template')
@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-8">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">High School Profile</h4>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="card card-chart">
              <div class="card-header card-header-success">
                <div class="ct-chart" id="dailySalesChart"></div>
              </div>
              <div class="card-body">
                <h4 class="card-title">Prestige</h4>
                <p class="card-category">
                  <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in this month's prestige</p>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons">access_time</i> updated 3 days ago
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="card card-chart">
              <div class="card-header card-header-warning">
                <div class="ct-chart" id="websiteViewsChart"></div>
              </div>
              <div class="card-body">
                <h4 class="card-title">Battles Won By Students</h4>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons">access_time</i> last challenge sent 2 days ago
                </div>
              </div>
            </div>
          </div>
        <div class="col-lg-12 col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">Top Students Stats</h4>
              <p class="card-category">Students with highest glory on 31st August, 2018</p>
            </div>
            <div class="card-body table-responsive">
              <table class="table table-hover">
                <thead class="text-warning">
                  <th>Global Rank</th>
                  <th>Name</th>
                  <th>Glory</th>
                  <th>Class</th>
                  <th>Action</th>
                </thead>
                <tbody>
                  <tr>
                    <td>2</td>
                    <td>Kristian Tanuwijaya</td>
                    <td>144721</td>
                    <td>Data Scientist</td>
                    <td><a href="/student/1"><button class="btn btn-primary btn-round">View Profile</button></a></td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>Simeon Yuda Prasetyo</td>
                    <td>124721</td>
                    <td>Tutor</td>
                    <td><a href="/student/1"><button class="btn btn-primary btn-round">View Profile</button></a></td>
                  </tr>
                  <tr>
                    <td>4</td>
                    <td>Sundoro Iswara</td>
                    <td>124719</td>
                    <td>Gamer</td>
                    <td><a href="/student/1"><button class="btn btn-primary btn-round">View Profile</button></a></td>
                  </tr>
                  <tr>
                    <td>5</td>
                    <td>Justin Hadinata Chau</td>
                    <td>124718</td>
                    <td>Gamer</td>
                    <td><a href="/student/1"><button class="btn btn-primary btn-round">View Profile</button></a></td>
                  </tr>
                  <tr>
                    <td>6</td>
                    <td>Marcellino</td>
                    <td>124717</td>
                    <td>Student</td>
                    <td><a href="/student/1"><button class="btn btn-primary btn-round">View Profile</button></a></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card card-profile">
          <div class="card">
            <a href="#">
              <img class="img" src="{{ asset('img/schools/ubaya.png') }}" />
            </a>
          </div>
          <div class="card-body">
            <h4 class="card-title">Ubaya High</h4>
            <h6 class="card-category">1st Ranked Presige</h6>
            <p class="card-description">An outstanding high school founded in 1734 after the end of Galactic War IV by master scholar Ivan Riversky</p>
            <a href="http://kristian.zz.mu/ubaya/desweb" class="btn btn-primary btn-round">Website</a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="mapouter"><div class="gmap_canvas"><iframe style="width: 100%; height: auto;" id="gmap_canvas" src="https://maps.google.com/maps?q=universitas%20surabaya&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><style>.mapouter{text-align:right;height:auto;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:auto;width:100%;}</style></div>
      </div>
    </div>
  </div>
@endsection
@section('scripts')
  <script src="{{ asset('js/plugins/chartist.min.js') }}"></script>

@endsection