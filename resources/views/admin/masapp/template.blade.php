@extends('admin.template')

@section('searchBarAction')
  {{ route('adminMasAppSearchRedirect') }}
@endsection

@section('searchBarVisibility')
  inline-block
@endsection

@section('scriptNav')
  <script type="text/javascript">
    $("#nav_item_mas-app").addClass("active");
  </script>
@endsection