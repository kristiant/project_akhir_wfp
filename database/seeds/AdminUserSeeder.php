<?php

use Illuminate\Database\Seeder;
use App\User;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$admin = User::find(1);
    	if($admin != null)
    	{
    		return;
    	}

        $admin = new User();
        $admin->id = 1;
        $admin->username = "admin";
        $admin->email = "admin@localhost";
        $admin->password = bcrypt("admin");
        $admin->nama = "Administrator";
        $admin->is_student = false;
        $admin->is_admin = true;

        $admin->save();
    }
}
