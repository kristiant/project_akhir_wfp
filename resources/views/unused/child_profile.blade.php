@extends('parent_template')
@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-8">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">{{ $page["title"] }}</h4>
              <p class="card-category">{{ $page["subtitle"] }}</p>
            </div>
            <div class="card-body">
              @yield('content_profile')
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card card-profile">
            <div class="card-avatar">
              <a href="{{ $profile["pictureHref"] }}">
                <img class="img" src="{{ asset('img/'.$profile["pictureSrc"]) }}" />
              </a>
            </div>
            <div class="card-body">
              <h6 class="card-category">{{ $profile["title"] }}</h6>
              <h4 class="card-title">{{ $profile["name"] }}</h4>
              <h6 class="card-category">{{ $profile["subtitle"] }}</h6>
              <h4 class="card-title">{{ $profile["points"] }}</h4>
              <p class="card-description">{{ $profile["description"] }}</p>
              <h4 class="card-title">{{ $profile["logoLabel"] }} : <img style="height: 20px;width: auto;" src="{{ asset('img/schools/'.$profile["logoImgSrc"]) }}"> {{ $profile["logoText"] }}</h4>
              <a href="{{ $profile["linkHref"] }}" class="btn btn-primary btn-round">{{ $profile["linkText"] }}</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection