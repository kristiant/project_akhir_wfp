@extends('public.template')

@section('searchBarAction')
  {{ route('publicStudentSearchProcess') }}
@endsection

@section('scriptNav')
  <script type="text/javascript">
    $("#nav_item_students").addClass("active");
  </script>
@endsection