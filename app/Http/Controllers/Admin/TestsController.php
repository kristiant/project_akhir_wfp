<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Subject;
use App\School;
use App\Test;
use App\Student;
use Config;
use Exception;
use App\Http\Requests\AdminTestFormRequest;

class TestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('adminTestIndexPaginated', ['page_number' => 1]));
    }

    public function indexPaginated($page_number)
    {
        return $this->search(null, $page_number);
    }

    public function search($keyword, $page_number)
    {
        $query = null;
        if($keyword == null)
        {
            $query = Test::all();
        }
        else
        {
            $query = Test::where('summary', 'like', '%' . $keyword . '%')
                ->orWhere('id', 'like', $keyword . '%')
                ->get();
        }

        $tests = $query->forPage($page_number, Config::get('kristian.pagination_max_item'));
        $viewArgs["tests"] = $tests;

        $viewArgs["page"]["current"] = $page_number;
        $viewArgs["page"]["max"] = ceil($query->count() / Config::get('kristian.pagination_max_item'));
        if($viewArgs["page"]["max"] == 0) $viewArgs["page"]["max"] = 1;

        if($keyword != null && isset($keyword)) $viewArgs["keyword"] = $keyword;

        return view('admin.test.list', $viewArgs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $test = new Test();
        $test->id = -1;
        $test->school_id = -1;
        $test->tanggal = date('Y-m-d H:i:s');
        $test->is_validated = false;

        $subjects = Subject::all();

        $schoolsIdName = [];

        $schoolsAll = School::all();
        foreach ($schoolsAll as $key => $value)
        {
            $item = [];
            $item["value"] = $value->id;
            $item["label"] = $value->nama; // . " - NPNS " . $value->id;
            $schoolsIdName[] = $item;
        }

        $viewArgs["test"] = $test;
        $viewArgs["subjects"] = $subjects;
        $viewArgs["schoolsIdName"] = $schoolsIdName;
        return view('admin.test.edit', $viewArgs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminTestFormRequest $request)
    {
        try
        {
            $test = new Test();
            $test->school_id = $request->school_id;
            $test->subject_id = $request->subject_id;
            $test->summary = $request->summary;
            $test->jenis = $request->jenis;
            $test->tanggal = $request->tanggal;

            //handle checkbox is_validated
            if( isset($request->is_validated) && $request->is_validated == 1 )
            {
                $test->is_validated = true;
            }
            else
            {
                $test->is_validated = false;
            }

            //dd($test);
            //dd($request->tanggal);

            $test->save();
            //return json_encode(['status' => 'success']);
            return redirect(route('tests.edit', ['id' => $test->id]));
        }
        catch (Exception $e)
        {
            return json_encode(['status' => 'failed', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $test = Test::find($id);

        $subjects = Subject::all();

        $schoolsIdName = [];

        $schoolsAll = School::all();
        foreach ($schoolsAll as $key => $value)
        {
            $item = [];
            $item["value"] = $value->id;
            $item["label"] = $value->nama; // . " - NPNS " . $value->id;
            $schoolsIdName[] = $item;
        }

        $viewArgs["test"] = $test;
        $viewArgs["subjects"] = $subjects;
        $viewArgs["schoolsIdName"] = $schoolsIdName;
        return view('admin.test.edit', $viewArgs);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminTestFormRequest $request, $id)
    {
        try
        {
            $test = Test::find($id);
            $test->school_id = $request->school_id;
            $test->subject_id = $request->subject_id;
            $test->summary = $request->summary;
            $test->jenis = $request->jenis;
            $test->tanggal = $request->tanggal;

            $test->save();
            //return json_encode(['status' => 'success']);
            return redirect(route('tests.edit', ['id' => $id]));
        }
        catch (Exception $e)
        {
            return json_encode(['status' => 'failed', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $test = Test::find($id);

            $test->delete();
            return json_encode(['status' => 'success']);
        }
        catch (Exception $e)
        {
            return json_encode(['status' => 'failed', 'message' => $e->getMessage()]);
        }
    }

    public function validasiNilai(Request $request, $id)
    {
        try
        {
            $test = Test::find($id);
            $arrStudentNilai = [];

            $arrNilai = json_decode($request->json_nilai);
            foreach ($arrNilai as $key => $value)
            {
                if( !is_array($value) || count($value) < 2 )
                {
                    break;
                }

                $student_id = $value[0];
                $nilai = $value[1];
                $student = Student::find($student_id);

                $arrStudentNilai[] = ['student' => $student, 'nilai' => $nilai];
            }

            $test->validate($arrStudentNilai);

            //return json_encode(['status' => 'success']);
            return redirect(route('tests.edit', ['id' => $id]));
        }
        catch (Exception $e)
        {
            return json_encode(['status' => 'failed', 'message' => $e->getMessage()]);
        }
    }
}
