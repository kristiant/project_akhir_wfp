@extends('parent_template')
@section('content')
  <div class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="card card-nav-tabs col-md-12">
          <h4 class="card-header card-header-info">Battle</h4>
          <div class="card-body">
            <p class="card-text">Enter Room</p>
            <form action="/battle/room/1">
              <div class="form-group">
               <label for="form_login_username" class="bmd-label-floating">Room ID</label>
               <input type="text" class="form-control" id="form_login_username">
               <span class="bmd-help">Enter room ID given by challenger</span>
              </div>
              <div>
                <input type="password" class="form-control" id="form_login_password" placeholder="Password">
              </div>
              <div class="form-group">
                <button class="btn btn-primary">Start Battle</button>
                <button class="btn btn-secondary" data-toggle="modal" data-target="#modal_help" type="button">Help</button>
                <!-- type="button" prevent form submission when clicked -->
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="card card-nav-tabs col-md-12">
          <form action="/battle/room/1"><button class="btn btn-success" style="width: 100%;">Create New Battle Room</button></form>
        </div>
      </div>


      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-warning">
              <h4 class="card-title ">Your Battles</h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-danger">
                    <th>
                      Bet
                    </th>
                    <th>
                      Winning Prize
                    </th>
                    <th>
                      Battle Mode
                    </th>
                    <th>
                      Arena
                    </th>
                    <th>
                      Action
                    </th>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        235pt
                      </td>
                      <td>
                        293pt
                      </td>
                      <td class="text-primary">
                        1on1
                      </td>
                      <td>
                        Math
                      </td>
                      <td>
                        <a href="/battle/room/1"><button class="btn btn-primary">Enter Room</button></a>
                        <button class="btn btn-success disabled">You Won</button>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        50pt
                      </td>
                      <td>
                        100pt
                      </td>
                      <td class="text-primary">
                        Team Battle
                      </td>
                      <td>
                        Physics
                      </td>
                      <td>
                        <a href="/battle/room/1"><button class="btn btn-primary">Enter Room</button></a>
                        <button class="btn btn-secondary disabled">You Lost</button>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        1,000pt
                      </td>
                      <td>
                        200,000pt
                      </td>
                      <td class="text-primary">
                        Battle Royale
                      </td>
                      <td>
                        Math
                      </td>
                      <td>
                        <a href="/battle/room/1"><button class="btn btn-primary">Enter Room</button></a>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#modal_forfeit">Forfeit</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="modal_forfeit" tabindex="-1" role="dialog" aria-labelledby="modal_forfeit_title" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_forfeit_title">Forfeiting Battle</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Are you sure you want to forfeit battle? Your bet points will be lost!
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger">Forfeit</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_help" tabindex="-1" role="dialog" aria-labelledby="modal_help_title" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_help_title">Battle Help</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <h6>
            Battle Explanation
          </h6>
          <p>
            In battle mode, you can bet your glory on your next test against other student. After creating room, you can give your room ID and password to your rival(s) to challenge them to battle.
          </p>
          <h6>
            Battle Modes
          </h6>
          <p>
            1on1 mode is for 2 player seeking a duel-like showdown.
          </p>
          <p>
            Team match is for players to form 2 teams and the team with highest average score win.
          </p>
          <p>
            Battle royale is a free for all battle mode with 'winner-take-all' rule.
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

@endsection