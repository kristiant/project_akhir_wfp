<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Config;
use App\Subject;
use App\Http\Requests\AdminSubjectFormRequest;

class SubjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('adminSubjectIndexPaginated', ['page_number' => 1]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPaginated($page_number)
    {
        return $this->search(null, $page_number);
    }

    public function search($keyword, $page_number)
    {
        $query = null;
        if($keyword == null)
        {
            $query = Subject::all();
        }
        else
        {
            $query = Subject::where('nama', 'like', '%' . $keyword . '%')
                ->orWhere('id', 'like', $keyword . '%')
                ->get();
        }

        $subjects = $query->forPage($page_number, Config::get('kristian.pagination_max_item'));
        $viewArgs["subjects"] = $subjects;

        $viewArgs["page"]["current"] = $page_number;
        $viewArgs["page"]["max"] = ceil($query->count() / Config::get('kristian.pagination_max_item'));
        if($viewArgs["page"]["max"] == 0) $viewArgs["page"]["max"] = 1;

        if($keyword != null && isset($keyword)) $viewArgs["keyword"] = $keyword;
        
        return view('admin.subject.list', $viewArgs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subject = new Subject();
        $subject->id = -1;

        $viewArgs["subject"] = $subject;
        return view('admin.subject.edit', $viewArgs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminSubjectFormRequest $request)
    {
        try
        {
            $subject = new Subject();
            $subject->nama = $request->nama;
            $subject->singkatan = $request->singkatan;

            $subject->save();
            //return json_encode(['status' => 'success']);
            return redirect(route('subjects.edit', ['id' => $subject->id]));
        }
        catch (Exception $e)
        {
            return json_encode(['status' => 'failed', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subject = Subject::find($id);

        $viewArgs["subject"] = $subject;
        return view('admin.subject.edit', $viewArgs);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminSubjectFormRequest $request, $id)
    {
        try
        {
            $subject = Subject::find($id);
            $subject->nama = $request->nama;
            $subject->singkatan = $request->singkatan;

            $subject->save();
            //return json_encode(['status' => 'success']);
            return redirect(route('subjects.edit', ['id' => $id]));
        }
        catch (Exception $e)
        {
            return json_encode(['status' => 'failed', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $subject = Subject::find($id);

            $subject->delete();
            return json_encode(['status' => 'success']);
        }
        catch (Exception $e)
        {
            return json_encode(['status' => 'failed', 'message' => $e->getMessage()]);
        }
    }
}
