@extends('public.masapp.template')
@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-warning">
              <h4 class="card-title">
                Master Apprentice Proposals
                <a class="btn btn-success" style="float: right;" href="{{
                  route('memberMasappApplyForm', [
                  'arg_master_or_apprentice' => 'master',
                  'target_student_id' => -1,
                  'subject_id' => -1
                  ])
                }}">
                  <i class="material-icons">add</i> Ask Someone to Become Your Master
                  <div class="ripple-container"></div>
                </a>
                <a class="btn btn-success" style="float: right;" href="{{
                  route('memberMasappApplyForm', [
                  'arg_master_or_apprentice' => 'apprentice',
                  'target_student_id' => -1,
                  'subject_id' => -1
                  ])
                }}">
                  <i class="material-icons">add</i> Ask Someone to Become Your Apprentice
                  <div class="ripple-container"></div>
                </a>
              </h4>
              <p class="card-category">Received proposals</p>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>Name</th>
                    <th>Message</th>
                    <th>Length</th>
                    <th>Action</th>
                  </thead>
                  <tbody>
                    @foreach($student->proposalsReceived as $row)
                      @if($row->status == 'new')
                        <tr>
                          <td>{{ $row->sender->nama }}</td>
                          <td>
                            @if($row->proposal_type == 'master')
                              Please be my master in
                            @else
                              I'm willing to make you my apprentice in
                            @endif
                            {{ $row->subject->nama }}
                          </td>
                          <td>{{ $row->length_in_month }} Month(s)</td>
                          <td>
                            <button class="btn-small btn-success" onclick="changeAction('{{ $row->id }}', 'accept');" data-toggle="modal" data-target="#modal_delete">Accept</button>
                            <button class="btn-small btn-danger" onclick="changeAction('{{ $row->id }}', 'decline');" data-toggle="modal" data-target="#modal_delete">Decline</button>
                            <a href="{{ route('publicStudentProfile', ['id' => $row->sender->id]) }}"><button class="btn-small btn-primary">Profile</button></a>
                          </td>
                        </tr>
                      @endif
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>


          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">
                Sent Proposals
              </h4>
              <p class="card-category">Proposals that you sent</p>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>Name</th>
                    <th>Message</th>
                    <th>Length</th>
                    <th>Status</th>
                    <th>Action</th>
                  </thead>
                  <tbody>
                    @foreach($student->proposalsSent()->orderBy('status', 'DESC')->get() as $row)
                      <tr>
                        <td>{{ $row->receiver->nama }}</td>
                        <td>
                          @if($row->proposal_type == 'master')
                            Please be my master in
                          @else
                            I'm willing to make you my apprentice in
                          @endif
                          {{ $row->subject->nama }}
                        </td>
                        <td>{{ $row->length_in_month }} Month(s)</td>
                        <td>{{ $row->status }}</td>
                        <td>
                          <a href="{{ route('publicStudentProfile', ['id' => $row->receiver->id]) }}"><button class="btn-small btn-primary">Profile</button></a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>


          <div class="card">
            <div class="card-header card-header-success">
              <h4 class="card-title">
                Your Masters
              </h4>
              <p class="card-category">You are being mentored by:</p>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>Mentored In</th>
                    <th>By Rank</th>
                    <th>Name</th>
                    <th>Glory</th>
                    <th>Specialization</th>
                    <th>School</th>
                  </thead>
                  <tbody>
                    @foreach($student->masters as $row)
                      @if($row->isActive())
                        <tr>
                          <td>{{ $row->subject->nama }}</td>
                          <td>{{ $row->master->getRank() }}</td>
                          <td>{{ $row->master->nama }}</td>
                          <td>{{ round($row->master->glory) }}</td>
                          <td>{{ $row->master->specialization }}</td>
                          <td>{{ $row->master->school->nama }}</td>
                        </tr>
                      @endif
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>


          <div class="card">
            <div class="card-header card-header-success">
              <h4 class="card-title">
                Your Apprentices
              </h4>
              <p class="card-category">You are mentoring:</p>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>Teaching</th>
                    <th>To Rank</th>
                    <th>Name</th>
                    <th>Glory</th>
                    <th>Specialization</th>
                    <th>School</th>
                  </thead>
                  <tbody>
                    @foreach($student->apprentices as $row)
                      @if($row->isActive())
                        <tr>
                          <td>{{ $row->subject->nama }}</td>
                          <td>{{ $row->apprentice->getRank() }}</td>
                          <td>{{ $row->apprentice->nama }}</td>
                          <td>{{ round($row->apprentice->glory) }}</td>
                          <td>{{ $row->apprentice->specialization }}</td>
                          <td>{{ $row->apprentice->school->nama }}</td>
                        </tr>
                      @endif
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- Modal -->
  <div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="modal_delete_title" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_delete_title">Confirmation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="modal_body">
          Are you sure you want to accept/decline this proposal?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger" onclick="respondToProposal();" id="modal_button_confirm">Sure</button>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
  <script type="text/javascript">
    var toActionProposalId = -1;
    var actionName = 'accept'; // 'accept' or 'decline'
    function respondToProposal()
    {
      //alert('you say you want to: ' + actionName + ' to proposal with id: ' + toActionProposalId);
      //var urlTarget = "/masapp/proposals/" + toActionProposalId + "/" + actionName;
      var urlTarget = "{!! route('memberMasappRespond', ['proposal_id' => '::..toActionProposalId..::', 'arg_accept_or_decline' => '::..actionName..::' ]) !!}";
      urlTarget = urlTarget.replace("::..toActionProposalId..::", toActionProposalId)
      urlTarget = urlTarget.replace("::..actionName..::", actionName)
      console.log(urlTarget);
      //return;
      $.ajax({
        type: "POST",
        url: urlTarget,
        headers: { "X-CSRF-TOKEN" : "{{ csrf_token() }}" },
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (data)
        {
          varResultObj = JSON.parse(data); // assume the return will always be {"status" : "success"} OR {"status" : "fail"}
          if(varResultObj.status == "success")
          {
            window.location = "{{ route('memberMasappProposals') }}";
          }
          else
          {
            alert('ERROR, response: ' + data);
          }
        },
        error: function (e) {
          alert('Cannot do that action, see console');
          console.log("ERROR : ", e);
        }
      });
    }
    function changeAction(newProposalId, newAction)
    {
      toActionProposalId = newProposalId;
      actionName = newAction;
      $("#modal_body").html("Are you sure you want to " + actionName + " this proposal?");
      $("#modal_button_confirm").html(actionName);
    }
  </script>
@endsection