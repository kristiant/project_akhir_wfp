<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Exception;

class Test extends Model
{
    protected $table = 'tests';
    public $timestamps = true;
    protected $primaryKey = 'id';
    //protected $keyType = 'string';

    // relationships
    public function school()
    {
        return $this->belongsTo('App\School');
    }

    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }

    /*
    public function testStudentDetails()
    {
        return $this->hasMany('App\TestStudent');
    }
    */

    public function students()
    {
        return $this->belongsToMany('App\Student', 'test_student', 'test_id', 'student_id')
            ->withPivot('nilai')
        ;
    }

    // other methods
    public function validate($argArrStudentNilai) // isi array: [ ['student'=>$student1,'nilai'=>100], [...] ]
    {
        if($this->is_validated == true)
        {
            //raise error
            throw new Exception("This test has already been validated!", 1);
        }

        foreach ($argArrStudentNilai as $key => $value)
        {
            $valid1 = ($value['student']->school_id == $this->school_id);
            $valid2 = ($this->students()->where('id','=',$value['student']->id)->count() == 0);

            if((!$valid1) || (!$valid2))
            {
                throw new Exception("Invalid data: test_id=" . $this . ", student_id=" . $value["student"] . ", nilai=" . $value["nilai"], 1);
            }

            $value["student"]->distributeGloryToMasters($this, $value["nilai"]);

            $value["student"]->recountGlory($value["nilai"]);
            $this->students()->attach($value["student"], ["nilai" => $value["nilai"]]);
            $value["student"]->save();
        }

        $this->is_validated = true;
        $this->validated_at = date('Y-m-d H:i:s');
        $this->save();

        $this->school->recountPrestige();
        $this->school->save();

    }

    public function isCanBeDeleted()
    {
        if($this->is_validated)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
